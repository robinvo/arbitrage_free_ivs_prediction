import numpy as np
from tensorflow import keras
import tensorflow as tf
from sklearn.preprocessing import MinMaxScaler

class NSA_IVS_DNN:
    
    beta_default = np.array([1,1,1,1])
    dnn_params_default = np.array([3,30,30,30])

    #strike_grid_cond34 = np.arange(0.7, 1.3, 0.05)
    #ttm_grid_cond34 = np.arange(1, 10, 1)
    strike_grid_cond34 = np.arange(0.65, 1.55, 0.04)
    ttm_grid_cond34 = np.arange(15, 415, 15)

    #strike_grid_cond5 = np.arange(0.7, 1.3, 0.05)
    #ttm_grid_cond5 = np.arange(1, 10, 1)
    strike_grid_cond5 = np.arange(0.65, 1.55, 0.04)
    ttm_grid_cond5 = np.arange(15, 415, 15)

    # for calculating approximate derivatives
    tol = 1e-6 

    def __init__(self, data: np.ndarray, grid: np.ndarray, dnn_params: np.array = None, beta: np.array = None) -> None:
        # data = { {IV_t(K_i,\tau_j}_i,j }_t
        
        self.strike_grid = grid[0]
        self.ttm_grid = grid[1]

        self.num_strikes = self.strike_grid.shape[0]
        self.num_ttms = self.ttm_grid.shape[0]

        self.scalerX = MinMaxScaler()
        self.scalerY = MinMaxScaler()

        # set batch size to number of IVs in a grid
        self.batch_size = self.num_strikes * self.num_ttms

        self.num_data_grids = data.shape[0]

        self.current_iv_grid = np.zeros(self.num_strikes*self.num_ttms)

        assert self.num_strikes * self.num_ttms == data.shape[1], print(data.shape[1], self.num_strikes * self.num_ttms)

        self.dataX = np.zeros((int(self.num_data_grids)*self.num_strikes*self.num_ttms, self.num_strikes*self.num_ttms+2))
        self.dataY = np.zeros((int(self.num_data_grids)*self.num_strikes*self.num_ttms, 1))

        for i in range(int(self.num_data_grids)):
            for j in range(self.num_strikes*self.num_ttms):
                self.dataX[i*self.num_strikes*self.num_ttms+j][:-2] = data[i][:]
                self.dataX[i*self.num_strikes*self.num_ttms+j][-2] = self.strike_grid[j%self.num_strikes]
                self.dataX[i*self.num_strikes*self.num_ttms+j][-1]= self.ttm_grid[int(j/self.num_strikes)]
                self.dataY[i*self.num_strikes*self.num_ttms+j][0] =  data[i][j]

        self.dataX = self.scalerX.fit_transform(self.dataX)
        self.dataY = self.scalerY.fit_transform(self.dataY)

        if beta is None:
            self.beta = self.beta_default
        else:
            self.beta = beta

        if dnn_params is None:
            self.dnn_params = self.dnn_params_default
        else:
            self.dnn_params = dnn_params

        # neural network setup
        self.dnn = keras.Sequential()

        self.dnn.add(keras.layers.Dense(self.dnn_params[1], input_dim=self.num_strikes*self.num_ttms+2, activation='elu'))

        for i in range(self.dnn_params[0]-1):
            #self.dnn.add(keras.layers.BatchNormalization())
            self.dnn.add(keras.layers.Dense(self.dnn_params[i+2], activation='elu'))

        #self.dnn.add(keras.layers.BatchNormalization())
        self.dnn.add(keras.layers.Dense(1, activation='softplus'))
        
        self.nsa_dnn = NSA_DNN(dnn=self.dnn, beta=beta)

        self.nsa_dnn.compile(optimizer='adam')


    def train(self, epochs: int = 30):
        """
        self.current_iv_grid = self.dataX[0][:-2]
        ind = self.num_strikes * self.num_ttms

        self.dnn.fit(self.dataX[:ind], self.dataY[0:ind], epochs=self.epochs, batch_size=self.batch_size)

        # save model
        model_json = self.dnn.to_json()
        with open("dnn_model.json", "w") as json_file:
            json_file.write(model_json)
        self.dnn.save_weights("dnn_model.h5")

        for i in range(1, self.num_data_grids):
            print("Iteration: ", i, "of", self.num_data_grids)

            # load model
            json_file = open('dnn_model.json', 'r')
            loaded_model_json = json_file.read()
            json_file.close()
            self.dnn = keras.models.model_from_json(loaded_model_json)
            self.dnn.load_weights("dnn_model.h5")
            self.dnn.compile(loss=self.total_loss, optimizer='adam', metrics=['accuracy'])

            ind1 = i * self.num_strikes * self.num_ttms
            ind2 = ind1 + self.num_strikes * self.num_ttms

            # set the current implied volatility grid used for calculating the NSA loss 
            self.current_iv_grid = self.dataX[ind1][:-2]

            self.dnn.fit(self.dataX[ind1:ind2], self.dataY[ind1:ind2], epochs=self.epochs, batch_size=self.batch_size)

            # save model
            model_json = self.dnn.to_json()
            with open("dnn_model.json", "w") as json_file:
                json_file.write(model_json)
            self.dnn.save_weights("dnn_model.h5")
        """

        self.history = self.nsa_dnn.fit(self.dataX, self.dataY, self.batch_size, epochs, shuffle=False)

        return self.history


    def predict(self, x: np.ndarray) -> np.ndarray:
        x = self.scalerX.transform(x)

        self.current_iv_grid = x[:-2]
        res = self.nsa_dnn.dnn.predict(tf.expand_dims(tf.convert_to_tensor(x, dtype=tf.float32), axis=0))

        return self.scalerY.inverse_transform(res)


    def predict_mult(self, x: np.ndarray) -> np.ndarray:
        x = self.scalerX.transform(x)

        self.current_iv_grid = x[0,:-2]

        res = self.nsa_dnn.dnn.predict(tf.convert_to_tensor(x, dtype=tf.float32))

        return self.scalerY.inverse_transform(res)


    def save_model(self, file_name: str) -> None:

        model_json = self.dnn.to_json()
        with open(file_name + ".json", "w") as json_file:
            json_file.write(model_json)
        self.nsa_dnn.dnn.save_weights(file_name + ".h5")

        return 


    def load_model(self, file_name_and_path: str) -> None:

        json_file = open(file_name_and_path + ".json", 'r')
        loaded_model_json = json_file.read()
        json_file.close()

        self.dnn = keras.models.model_from_json(loaded_model_json)
        self.dnn.load_weights(file_name_and_path + ".h5")

        self.nsa_dnn = NSA_DNN(self.dnn)
        self.nsa_dnn.compile(optimizer='adam')

        return


    def predict_surface(self, iv_grid: np.array, strikes_test: np.array, ttms_test: np.array) -> np.array:

        x = np.zeros((ttms_test.shape[0]*strikes_test.shape[0], self.num_strikes*self.num_ttms+2))

        for i in range(ttms_test.shape[0]):
            for j in range(strikes_test.shape[0]):
                x[i*strikes_test.shape[0]+j] = np.append(iv_grid,[strikes_test[j],ttms_test[i]]).flatten()

        Z = self.predict_mult(x)
        Z = Z.reshape([ttms_test.size,strikes_test.size])

        return Z


    def predict_all_surfaces(self, iv_grids: np.array, strikes_test: np.array, ttms_test: np.array) -> np.array:
        num_grids = iv_grids.shape[0]

        Z = np.zeros((num_grids, ttms_test.shape[0], strikes_test.shape[0]))

        for i in range(num_grids):
            Z[i,:,:] = self.predict_surface(iv_grids[i], strikes_test, ttms_test)

        return Z

    """
    def test(self, x_test: np.ndarray, y_test: np.ndarray) -> float:

        y_pred = self.dnn.predict([x_test])

        return self.total_loss(y_pred, y_test)


    def mse_loss(self, y_pred: np.ndarray, y_true: np.ndarray) -> float:

        return keras.backend.mean(keras.backend.square(y_true - y_pred), axis = 0) 


    def nsa_loss(self, y_pred: np.ndarray, y_true: np.ndarray) -> float:
        # The neural network: f(K,\tau) = f(S exp(x), \tau)
        # x = log(K/S)
        # Does not depend on y_pred, but needed for custome keras loss function

        # Condition 1/2: 
        # Use activation that is twice differentiable and non-negative in last layer

        # Condition 3: 
        # max( -(1 - x * f_x(K,\tau) / f(K,\tau))^2 + 0.25 * f_x(K,\tau)^2 * f(K,\tau)^2 - f(K,\tau) * f_xx(K,\tau), 0.0 ) 
        L_cond3 = 0.0
        for strike in self.strike_grid_cond34:
            for ttm in self.ttm_grid_cond34:
                tmp1 = - ( 1.0 - np.log(strike) * self.f_x(strike,ttm) / (self.f(strike,ttm)**2) )
                tmp2 = 0.25 * (self.f(strike,ttm)**2) * (self.f_x(strike,ttm)**2)
                tmp3 = - self.f(strike,ttm) * self.f_xx(strike,ttm)
                L_cond3 += np.maximum(tmp1 + tmp2 - tmp3, 0.0)

        # Condition 4:
        # max( -\partial_\tau f(K, \tau), 0 ) -> 0
        L_cond4 = 0.0
        for strike in self.strike_grid_cond34:
            for ttm in self.ttm_grid_cond34:
                L_cond4 += np.maximum(-self.f_tau(strike,ttm), 0.0)

        # Condition 5:
        # abs( f(K,\tau) \partial_{xx}f(K,\tau) + (\partial_x f(K,\tau))^2 ) -> 0
        L_cond5 = 0.0
        for strike in self.strike_grid_cond5:
            for ttm in self.ttm_grid_cond5:
                L_cond5 += np.abs(self.f(strike,ttm) * self.f_xx(strike,ttm) + self.f_x(strike,ttm)**2)

        # Condition 6:
        # f(K,0) -> 0
        L_cond6 = 0.0
        for strike in self.strike_grid_cond5:
            L_cond6 += self.f(strike, 0.0)

        return self.beta[0] * L_cond3 + self.beta[1] * L_cond4 + self.beta[2] * L_cond5 + self.beta[3] * L_cond6


    def total_loss(self, y_pred: np.ndarray, y_true: np.ndarray) -> float:

        return  self.mse_loss(y_pred, y_true) + self.nsa_loss(y_pred, y_true) / 1000000.0

    
    ### HELPER FUNCTIONOS ###

    def f(self, strike: float, ttm: float) -> float:
        x = [np.append(self.current_iv_grid, [strike, ttm])]
        return self.dnn.predict([x])


    def f_x(self, strike: float, ttm: float) -> float:
        # f_x(K,\tau) =  K * f_K(K,\tau)
        return strike * (self.f(strike - self.tol/2, ttm) - self.f(strike + self.tol/2, ttm)) / self.tol


    def f_xx(self, strike: float, ttm: float) -> float:
        # f_xx(K,\tau) = K * f_K(K,\tau) + K^2 * f_KK(K,\tau) = f_x(K,\tau) + K^2 * f_KK(K,\tau)
        tmp1 = (self.f(strike - self.tol, ttm) - self.f(strike, ttm) + self.f(strike + self.tol, ttm))
        tmp2 = self.tol * self.tol / strike / strike 

        return self.f_x(strike,ttm) + tmp1 / tmp2


    def f_tau(self, strike: float, ttm: float) -> float:

        return (self.f(strike, ttm - self.tol/2) - self.f(strike, ttm + self.tol/2)) / self.tol
    """


class NSA_DNN(keras.Model):

    #strike_grid_cond34 = np.arange(0.7, 1.3, 0.05)
    #ttm_grid_cond34 = np.arange(1, 10, 1)
    strike_grid_cond34 = np.arange(0.6, 1.6, 0.3)
    ttm_grid_cond34 = np.arange(15, 415, 90)

    #strike_grid_cond5 = np.arange(0.7, 1.3, 0.05)
    #ttm_grid_cond5 = np.arange(1, 10, 1)
    strike_grid_cond5 = np.arange(0.5, 1.7, 0.4)
    ttm_grid_cond5 = np.arange(15, 415, 90)

    beta_default = np.array([1e-3,1e-3,1e-3,1e-3])


    def __init__(self, dnn: keras.Model, beta: np.array = [], **kwargs):
        super(NSA_DNN, self).__init__(**kwargs)
        
        self.dnn = dnn
        self.dnn.summary()

        self.tol = 1e-9 #3e-2
        self.tol_tau = 1e-9 #3

        self.beta = beta if beta != [] else self.beta_default

        self.total_loss_tracker = keras.metrics.Mean(name="total_loss")
        self.reconstruction_loss_tracker = keras.metrics.Mean(
            name="reconstruction_loss"
        )
        self.nsa_C3_loss_tracker = keras.metrics.Mean(name="nsa_C3")
        self.nsa_C4_loss_tracker = keras.metrics.Mean(name="nsa_C4")
        self.nsa_C5_loss_tracker = keras.metrics.Mean(name="nsa_C5")
        self.nsa_C6_loss_tracker = keras.metrics.Mean(name="nsa_C6")


    def call(self, x):

        return self.dnn.predict(x)


    @property
    def metrics(self):
        return [
            self.total_loss_tracker,
            self.reconstruction_loss_tracker,
            self.nsa_C3_loss_tracker,
            self.nsa_C4_loss_tracker,
            self.nsa_C5_loss_tracker,
            self.nsa_C6_loss_tracker
        ]


    def train_step(self, data):

        trainX, y_true = data

        with tf.GradientTape() as tape:
            self.current_iv_grid = trainX[0][:-2]
            y_pred = self.dnn(trainX)
            #print(trainX)
            #print(float(trainX[0][0]))
            #print(float(trainX[1][0]))
            #print(float(trainX[2][0]))


            reconstruction_loss = tf.reduce_mean(
                tf.reduce_sum(
                    keras.losses.mean_squared_error(y_true=y_true, y_pred=y_pred)
                )
            )

            # Condition 1/2: 
            # Use activation that is twice differentiable and non-negative in last layer

            # Condition 3: 
            # max( -(1 - x * f_x(K,\tau) / f(K,\tau))^2 + 0.25 * f_x(K,\tau)^2 * f(K,\tau)^2 - f(K,\tau) * f_xx(K,\tau), 0.0 )
            nsa_C3_loss = 0.0
            #tmp_C3 = 0.0
            for strike in self.strike_grid_cond34:
                for ttm in self.ttm_grid_cond34:
                    tmp1 = - ( 1.0 - tf.divide(tf.multiply(tf.cast(tf.math.log(strike), dtype=tf.float32), tf.cast(self.f_x(strike,ttm), dtype=tf.float32)), (tf.pow(self.f(strike,ttm),2))))
                    tmp2 = 0.25 * tf.multiply((tf.pow(self.f(strike,ttm),2)), (tf.pow(self.f_x(strike,ttm),2)))
                    tmp3 = - tf.multiply(self.f(strike,ttm), self.f_xx(strike,ttm))
                    nsa_C3_loss += tf.math.maximum(tmp1 + tmp2 - tmp3, 0.0)
                    #tmp_C3 += tmp1 + tmp2 - tmp3
            #if tmp_C3 > 0:
                #nsa_C3_loss += tf.math.maximum(tmp1 + tmp2 - tmp3, 0.0)

            #nsa_C3_loss *= self.beta[0]
            """
            y_C3_C4 = np.zeros((self.strike_grid_cond34.shape[0], self.ttm_grid_cond34.shape[0]))
            grad_y_C3_C4 = np.zeros((self.strike_grid_cond34.shape[0], self.ttm_grid_cond34.shape[0]))
            grad_of_grad_y_C3_C4 = np.zeros((self.strike_grid_cond34.shape[0], self.ttm_grid_cond34.shape[0]))
            i1 = 0
            for strike in self.strike_grid_cond34:
                i2 = 0
                for ttm in self.ttm_grid_cond34:
                    x = tf.concat([self.current_iv_grid, [strike], [ttm]], axis=0)
                    print(self.dnn(tf.expand_dims(x, axis=0)))
                    y_C3_C4[i1, i2] = self.dnn(tf.expand_dims(x, axis=0))
                    grad_y_C3_C4[i1, i2] = tape.gradient(y_C3_C4[i1, i2], x)
                    grad_of_grad_y_C3_C4[i1, i2] = tape.gradient(grad_y_C3_C4[i1, i2], x)
                    i2 += 1
                i1 += 1

            nsa_C3_loss = self.beta_default[0] * tf.reduce_sum(grad_of_grad_y_C3_C4 + grad_y_C3_C4)
            """
           
            # Condition 4:
            # max( -\partial_\tau f(K, \tau), 0 ) -> 0

            nsa_C4_loss = 0.0
            for strike in self.strike_grid_cond34:
                for ttm in self.ttm_grid_cond34:
                    if self.f_tau(strike,ttm) < 0.0:
                        nsa_C4_loss += tf.math.maximum(-self.f_tau(strike,ttm), 0.0)
            #nsa_C4_loss *= self.beta[1]
            

            # Condition 5:
            # abs( f(K,\tau) \partial_{xx}f(K,\tau) + (\partial_x f(K,\tau))^2 ) -> 0
            nsa_C5_loss = 0.0
            for strike in self.strike_grid_cond5:
                for ttm in self.ttm_grid_cond5:
                    nsa_C5_loss += tf.math.abs(self.f(strike,ttm) * self.f_xx(strike,ttm) + self.f_x(strike,ttm)**2)
            #nsa_C5_loss *= self.beta[2]

            # Condition 6:
            # f(K,0) -> 0
            nsa_C6_loss = 0.0
            
            for strike in self.strike_grid_cond5:
                nsa_C6_loss += self.f(strike, 0.0)
            #nsa_C6_loss *= self.beta[3]
            
            total_loss = reconstruction_loss + nsa_C3_loss*self.beta[0] + nsa_C4_loss*self.beta[1] + nsa_C5_loss*self.beta[2] + nsa_C6_loss*self.beta[3]

        # optimizer step
        grads = tape.gradient(total_loss, self.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))

        # update metrics
        self.total_loss_tracker.update_state(reconstruction_loss + nsa_C3_loss + nsa_C4_loss + nsa_C5_loss + nsa_C6_loss)
        self.reconstruction_loss_tracker.update_state(reconstruction_loss)
        self.nsa_C3_loss_tracker.update_state(nsa_C3_loss)
        self.nsa_C4_loss_tracker.update_state(nsa_C4_loss)
        self.nsa_C5_loss_tracker.update_state(nsa_C5_loss)
        self.nsa_C6_loss_tracker.update_state(nsa_C6_loss)

        return {
            "total_loss": self.total_loss_tracker.result(),
            "reconstruction_loss": self.reconstruction_loss_tracker.result(),
            "nsa_C3": self.nsa_C3_loss_tracker.result(),
            "nsa_C4": self.nsa_C4_loss_tracker.result(),
            "nsa_C5": self.nsa_C5_loss_tracker.result(),
            "nsa_C6": self.nsa_C6_loss_tracker.result()
        }


    def test_for_conditions(self, iv_grid: np.array, strikes: np.array, ttms: np.array) -> list:
        C3_violation, C4_violation, C5_violation, C6_violation = 0.0, 0.0, 0.0, 0.0

        self.current_iv_grid = iv_grid
    
        for strike in strikes:
            for ttm in ttms:
                tmp1 = - ( 1.0 - tf.divide(tf.multiply(tf.cast(tf.math.log(strike), dtype=tf.float32), tf.cast(self.f_x(strike,ttm), dtype=tf.float32)), (tf.pow(self.f(strike,ttm),2))))
                tmp2 = 0.25 * tf.multiply((tf.pow(self.f(strike,ttm),2)), (tf.pow(self.f_x(strike,ttm),2)))
                tmp3 = - tf.multiply(self.f(strike,ttm), self.f_xx(strike,ttm))
                C3_violation += tf.math.maximum(tmp1 + tmp2 - tmp3, 0.0)

                C4_violation += tf.math.maximum(-self.f_tau(strike,ttm), 0.0)

                C5_violation += tf.math.abs(self.f(strike,ttm) * self.f_xx(strike,ttm) + self.f_x(strike,ttm)**2)

                C6_violation += self.f(strike, 0.0)
        C3_violation /= (strikes.shape[0]*ttms.shape[0])
        C4_violation /= (strikes.shape[0]*ttms.shape[0])
        C5_violation /= (strikes.shape[0]*ttms.shape[0])
        C6_violation /= (strikes.shape[0]*ttms.shape[0])

        return [C3_violation,C4_violation,C5_violation,C6_violation]


    ### HELPER FUNCTIONOS ###
    def f(self, strike: float, ttm: float) -> float:
        x = tf.concat([self.current_iv_grid, [strike], [ttm]], axis=0)
        return self.dnn(tf.expand_dims(x, axis=0))[0]


    def f_x(self, strike: float, ttm: float) -> float:
        """ f_x(K,\tau) =  K * f_K(K,\tau) """
        #with tf.GradientTape() as tape:
        #    x = tf.concat([self.current_iv_grid, [strike], [ttm]], axis=0)
        #    f = self.dnn(tf.expand_dims(x, axis=0))
        #    f_x = tape.gradient(f, x)
        #return f_x
        return strike * (self.f(strike - self.tol/2, ttm) - self.f(strike + self.tol/2, ttm)) / self.tol

        # 5 point method
        #return strike * (-self.f(strike + 2*self.tol, ttm) + 8*self.f(strike + self.tol, ttm) - self.f(strike - self.tol, ttm) + 8*self.f(strike - 2*self.tol, ttm)) / (12*self.tol) 


    def f_xx(self, strike: float, ttm: float) -> float:
        """ f_xx(K,\tau) = K * f_K(K,\tau) + K^2 * f_KK(K,\tau) = f_x(K,\tau) + K^2 * f_KK(K,\tau) """
        tmp1 = (self.f(strike - self.tol, ttm) - 2*self.f(strike, ttm) + self.f(strike + self.tol, ttm))
        tmp2 = self.tol * self.tol / strike / strike 
        #with tf.GradientTape() as tape:
        #    x = tf.concat([self.current_iv_grid, [strike], [ttm]], axis=0)
        #    f = self.dnn(tf.expand_dims(x, axis=0))
        #    f_x = tape.gradient(f, x)
        #    f_xx = tape.gradient(f_x, x)
        #return f_xx

        # 5 point method 
        # f_K(K+2h,\tau)
        #tmp1 = (-self.f(strike + 4*self.tol, ttm) + 8*self.f(strike + 3*self.tol, ttm) - self.f(strike + self.tol, ttm) + 8*self.f(strike, ttm))
        # f_K(K+h,\tau)
        #tmp2 = (-self.f(strike + 3*self.tol, ttm) + 8*self.f(strike + 2*self.tol, ttm) - self.f(strike, ttm) + 8*self.f(strike - self.tol, ttm))
        # f_K(K-h,\tau)
        #tmp3 = (-self.f(strike + self.tol, ttm) + 8*self.f(strike, ttm) - self.f(strike - 2*self.tol, ttm) + 8*self.f(strike - 3*self.tol, ttm))
        # f_K(K-2h,\tau)
        #tmp4 = (-self.f(strike, ttm) + 8*self.f(strike - self.tol, ttm) - self.f(strike - 3*self.tol, ttm) + 8*self.f(strike - 4*self.tol, ttm))

        #tmp = (-tmp1 + 8*tmp2 - 8*tmp3 + tmp4) * strike**2 / ((12*self.tol)**2)

        #tmp_1 = self.f(strike + 4*self.tol, ttm) - 16*self.f(strike + 3*self.tol, ttm) + 64*self.f(strike + 2*self.tol, ttm) + 16*self.f(strike + 1*self.tol, ttm)
        #tmp_2 = self.f(strike - 4*self.tol, ttm) - 16*self.f(strike - 3*self.tol, ttm) + 64*self.f(strike - 2*self.tol, ttm) + 16*self.f(strike - 1*self.tol, ttm)
        return self.f_x(strike,ttm) + tmp1 / tmp2
        #return self.f_x(strike,ttm) + (tmp_1 - 130*self.f(strike, ttm) + tmp_2) / ((12*self.tol)**2)


    def f_tau(self, strike: float, ttm: float) -> float:
        #with tf.GradientTape() as tape:
        #    x = tf.concat([self.current_iv_grid, [strike], [ttm]], axis=0)
        #    f = self.dnn(tf.expand_dims(x, axis=0))
        #    f_tau = tape.gradient(f, x)
        #return f_tau
        return (self.f(strike, ttm - self.tol_tau/2) - self.f(strike, ttm + self.tol_tau/2)) / self.tol_tau



