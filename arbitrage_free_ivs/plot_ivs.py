import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


class IV_Plotting_Helper:

    def __init__(self, data: dict = {}) -> None:
        
        self.iv_surface_1 = None if "iv_surface_1" not in data else data["iv_surface_1"]
        self.iv_surface_2 = None if "iv_surface_2" not in data else data["iv_surface_2"]

        self.iv_grid_1 = None if "iv_grid_1" not in data else data["iv_grid_1"]
        self.iv_grid_2 = None if "iv_grid_2" not in data else data["iv_grid_2"]

        self.legend_ = [] if "legend" not in data else data["legend"]
        self.num_plots_req = 1
        if self.iv_surface_1 is not None:
            try:
                self.stikes_surface_1 = data["stikes_surface_1"]
                self.ttms_surface_1 = data["ttms_surface_1"]
                self.num_plots_req += 1
            except:
                print("Some parameters missing!")
        if self.iv_surface_2 is not None:
            try:
                self.stikes_surface_2 = data["stikes_surface_2"]
                self.ttms_surface_2 = data["ttms_surface_2"]
                self.num_plots_req += 1
            except:
                print("Some parameters missing!")
        if self.iv_grid_1 is not None:
            try:
                self.stikes_grid_1 = data["stikes_grid_1"]
                self.ttms_grid_1 = data["ttms_grid_1"]
                self.num_plots_req += 1
            except:
                print("Some parameters missing!")
        if self.iv_grid_2 is not None:
            try:
                self.stikes_grid_2 = data["stikes_grid_2"]
                self.ttms_grid_2 = data["ttms_grid_2"]
                self.num_plots_req += 1
            except:
                print("Some parameters missing!")
        
        return 

    
    def plot_all_in_one(self, ind: int = 0, view_angle: tuple = (30, 30)) -> plt.figure:
        h = 5
        fig = plt.figure(figsize=(self.num_plots_req*h,h+2))
        ax = fig.add_subplot(1, self.num_plots_req, 1, projection='3d')
        legend_entries = []
        curr_plt = 1
        if self.iv_surface_1 is not None:
            X, Y = np.meshgrid(self.stikes_surface_1, self.ttms_surface_1)
            Z = self.iv_surface_1[ind,:]
            fake2Dline_1 = mpl.lines.Line2D([0],[0], linestyle="none", c='tab:blue', marker = '_')
            legend_entries.append(fake2Dline_1)
            ax.plot_surface(X, Y, Z,cmap="Blues", linewidth=0, antialiased=False)

            curr_plt += 1
            ax1 = fig.add_subplot(1, self.num_plots_req, curr_plt, projection='3d')
            ax1.plot_surface(X, Y, Z,cmap="Blues", linewidth=0, antialiased=False)
            ax1.view_init(view_angle[0], view_angle[1])
            ax1.set_xlabel('Strike')
            ax1.set_ylabel('Time-To-Maturity')
            ax1.set_zlabel('Implied Volatility')

        if self.iv_surface_2 is not None:
            X, Y = np.meshgrid(self.stikes_surface_2, self.ttms_surface_2)
            Z = self.iv_surface_2[ind,:]
            fake2Dline_2 = mpl.lines.Line2D([0],[0], linestyle="none", c='tab:orange', marker = '_')
            legend_entries.append(fake2Dline_2)
            ax.plot_surface(X, Y, Z, cmap="Oranges",linewidth=0, antialiased=False)

            curr_plt += 1
            ax2 = fig.add_subplot(1, self.num_plots_req, curr_plt, projection='3d')
            ax2.plot_surface(X, Y, Z, cmap="Oranges",linewidth=0, antialiased=False)
            ax2.view_init(view_angle[0], view_angle[1])
            ax2.set_xlabel('Strike')
            ax2.set_ylabel('Time-To-Maturity')
            ax2.set_zlabel('Implied Volatility')

        if self.iv_grid_1 is not None:
            X, Y = np.meshgrid(self.stikes_grid_1, self.ttms_grid_1)
            Z = self.iv_grid_1[ind,:]
            fake2Dline_3 = mpl.lines.Line2D([0],[0], linestyle="none", c='tab:blue', marker = 'o')
            legend_entries.append(fake2Dline_3)

            ax.scatter(X, Y, Z, linewidth=0, antialiased=False,c='tab:blue')

            curr_plt += 1
            ax3 = fig.add_subplot(1, self.num_plots_req, curr_plt, projection='3d')
            ax3.scatter(X, Y, Z, linewidth=0, antialiased=False,c='tab:blue')
            ax3.view_init(view_angle[0], view_angle[1])
            ax3.set_xlabel('Strike')
            ax3.set_ylabel('Time-To-Maturity')
            ax3.set_zlabel('Implied Volatility')

        if self.iv_grid_2 is not None:
            X, Y = np.meshgrid(self.stikes_grid_2, self.ttms_grid_2)
            Z = self.iv_grid_2[ind,:]
            fake2Dline_4 = mpl.lines.Line2D([0],[0], linestyle="none", c='tab:orange', marker = 'o')
            legend_entries.append(fake2Dline_4)

            ax.scatter(X, Y, Z, linewidth=0, antialiased=False,c='tab:orange')

            curr_plt += 1
            ax4 = fig.add_subplot(1, self.num_plots_req, curr_plt, projection='3d')
            ax4.scatter(X, Y, Z, linewidth=0, antialiased=False,c='tab:orange')
            ax4.view_init(view_angle[0], view_angle[1])
            ax4.set_xlabel('Strike')
            ax4.set_ylabel('Time-To-Maturity')
            ax4.set_zlabel('Implied Volatility')


        ax.view_init(view_angle[0], view_angle[1])

        ax.set_xlabel('Strike')
        ax.set_ylabel('Time-To-Maturity')
        ax.set_zlabel('Implied Volatility')

        ax.legend(legend_entries, self.legend_, loc='upper right', shadow=True)
        plt.tight_layout(pad=0., w_pad=0., h_pad=0.1)
        self.figure = fig

        return self.figure


    def plot_min_max_median_mape(self, view_angle: tuple = (30, 30), as_grid: bool = True):
        """ Uses grid 1 and grid 2 or grid 1 and surface 1, depending on as_grid """
        fig = plt.figure(figsize=(12,5))
        
        legend_entries = []
        print(self.iv_grid_1.shape)
        num_data_points = self.iv_grid_1.shape[0]
        mape = np.zeros(num_data_points)
        if as_grid:
            mape = np.mean(np.abs(self.iv_grid_1-self.iv_grid_2)/ self.iv_grid_1, axis=(1,2))*100
        else: 
            mape = np.mean(np.abs(self.iv_grid_1-self.iv_surface_1)/ self.iv_grid_1, axis=(1,2))*100

        ind_max = np.argmax(mape)
        ind_min = np.argmin(mape)
        #print(mape.shape)
        ind_median = np.argwhere(mape[:,0] == np.percentile(mape[:,0], 50, interpolation='nearest'))[0][0]
        
        X, Y = np.meshgrid(self.stikes_grid_1, self.ttms_grid_1)
        
        # 1
        ax1 = fig.add_subplot(1, 3, 1, projection='3d')

        fake2Dline1 = mpl.lines.Line2D([0],[0], linestyle="none", c='tab:blue', marker = 'o')
        legend_entries.append(fake2Dline1)

        Z1 = self.iv_grid_1[ind_min,:]
        ax1.scatter(X, Y, Z1, linewidth=0, antialiased=False,c='tab:blue')

        if as_grid:
            Z2 = self.iv_grid_2[ind_min,:]
            ax1.scatter(X, Y, Z2, linewidth=0, antialiased=False,c='tab:orange')

            fake2Dline2 = mpl.lines.Line2D([0],[0], linestyle="none", c='tab:orange', marker = 'o')
            legend_entries.append(fake2Dline2)
        else: 
            Z2 = self.iv_surface_1[ind_min,:,:,0]
            
            ax1.plot_surface(X, Y, Z2, cmap="Oranges", linewidth=0, antialiased=False)
        
            fake2Dline2 = mpl.lines.Line2D([0],[0], linestyle="none", c='tab:orange', marker = '_')
            legend_entries.append(fake2Dline2)

        ax1.view_init(view_angle[0], view_angle[1])
        ax1.set_xlabel('Strike')
        ax1.set_ylabel('Time-To-Maturity')
        ax1.set_zlabel('Implied Volatility')
        ax1.legend(legend_entries, self.legend_, loc='upper right', shadow=True)

        # 2
        ax2 = fig.add_subplot(1, 3, 2, projection='3d')

        fake2Dline3 = mpl.lines.Line2D([0],[0], linestyle="none", c='tab:blue', marker = 'o')
        legend_entries.append(fake2Dline3)

        Z1 = self.iv_grid_1[ind_median,:]
        ax2.scatter(X, Y, Z1, linewidth=0, antialiased=False,c='tab:blue')

        if as_grid:
            Z2 = self.iv_grid_2[ind_median,:]
            ax2.scatter(X, Y, Z2, linewidth=0, antialiased=False,c='tab:orange')

            fake2Dline4 = mpl.lines.Line2D([0],[0], linestyle="none", c='tab:orange', marker = 'o')
            legend_entries.append(fake2Dline4)
        else: 
            Z2 = self.iv_surface_1[ind_median,:,:,0]
            #print(self.iv_surface_1.shape)
            #print(Z2.shape)
            #print(ind_median)
            ax2.plot_surface(X, Y, Z2, cmap="Oranges", linewidth=0, antialiased=False)
        
            fake2Dline4 = mpl.lines.Line2D([0],[0], linestyle="none", c='tab:orange', marker = '_')
            legend_entries.append(fake2Dline4)

        ax2.view_init(view_angle[0], view_angle[1])
        ax2.set_xlabel('Strike')
        ax2.set_ylabel('Time-To-Maturity')
        ax2.set_zlabel('Implied Volatility')
        ax2.legend(legend_entries, self.legend_, loc='upper right', shadow=True)

        # 3
        ax3 = fig.add_subplot(1, 3, 3, projection='3d')

        fake2Dline5 = mpl.lines.Line2D([0],[0], linestyle="none", c='tab:blue', marker = 'o')
        legend_entries.append(fake2Dline5)

        Z1 = self.iv_grid_1[ind_max,:]
        ax3.scatter(X, Y, Z1, linewidth=0, antialiased=False,c='tab:blue')

        if as_grid:
            Z2 = self.iv_grid_2[ind_max,:]

            ax3.scatter(X, Y, Z2, linewidth=0, antialiased=False,c='tab:orange')

            fake2Dline6 = mpl.lines.Line2D([0],[0], linestyle="none", c='tab:orange', marker = 'o')
            legend_entries.append(fake2Dline6)
        else: 
            Z2 = self.iv_surface_1[ind_max,:,:,0]
            ax3.plot_surface(X, Y, Z2, cmap="Oranges", linewidth=0, antialiased=False)
        
            fake2Dline6 = mpl.lines.Line2D([0],[0], linestyle="none", c='tab:orange', marker = '_')
            legend_entries.append(fake2Dline6)

        ax3.view_init(view_angle[0], view_angle[1])
        ax3.set_xlabel('Strike')
        ax3.set_ylabel('Time-To-Maturity')
        ax3.set_zlabel('Implied Volatility')
        ax3.legend(legend_entries, self.legend_, loc='upper right', shadow=True)


        
        plt.tight_layout(pad=0., w_pad=0., h_pad=0.1)
        self.figure = fig

        return self.figure, [ind_min,ind_median,ind_max], [mape[ind_min],mape[ind_median],mape[ind_max]]



    def save_figure(self, path: str) -> None:

        self.figure.savefig(path, bbox_inches='tight')

        return  