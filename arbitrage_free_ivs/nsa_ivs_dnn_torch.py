import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils
import torch.distributions
import numpy as np


class DNN(nn.Module):

    def __init__(self, in_dim: int) -> None:
        super(DNN, self).__init__()
        
        self.linear1 = nn.Linear(in_dim, 50)
        self.linear2 = nn.Linear(50, 50)
        self.linear3 = nn.Linear(50, 1)

        #torch.nn.init.xavier_uniform_(self.linear1.weight)
        #torch.nn.init.xavier_uniform_(self.linear2.weight)
        #torch.nn.init.xavier_uniform_(self.linear3.weight)

    def forward(self, z):
        z = F.normalize(z)
        z = F.relu(self.linear1(z))
        z = F.normalize(z)
        z = F.relu(self.linear2(z))
        #z = F.softplus(self.linear3(z))
        z = F.softplus(self.linear3(z))

        #return z.reshape((-1, 1))
        return z



class NSA_IVS_DNN:

    def __init__(self, data: np.ndarray, grid: np.ndarray, params: dict = {}) -> None:
        # data = { {IV_t(K_i,\tau_j}_i,j }_t
        
        self.strike_grid = grid[0]
        self.ttm_grid = grid[1]

        self.num_strikes = self.strike_grid.shape[0]
        self.num_ttms = self.ttm_grid.shape[0]

        self.input_dim = self.num_strikes*self.num_ttms+2

        self.dnn = DNN(self.input_dim).to("cpu")

        self.beta = np.array([1,1,1,1]) if "betas" not in params else params["betas"]
        self.optimizer = torch.optim.Adam(self.dnn.parameters()) if "optimizer" not in params else params["optimizer"]

        # set batch size to number of IVs in a grid
        self.batch_size = self.num_strikes * self.num_ttms

        self.num_data_grids = data.shape[0]
        self.num_data_points = int(self.num_data_grids)*self.num_strikes*self.num_ttms

        self.current_iv_grid = np.zeros(self.num_strikes*self.num_ttms)

        assert self.num_strikes * self.num_ttms == data.shape[1], print(data.shape[1], self.num_strikes * self.num_ttms)

        self.dataX = np.zeros((self.num_data_points, self.input_dim))
        self.dataY = np.zeros((self.num_data_points, 1))
        self.data = np.zeros((self.num_data_points, self.input_dim+1))

        for i in range(int(self.num_data_grids)):
            for j in range(self.num_strikes*self.num_ttms):
                self.dataX[i*self.num_strikes*self.num_ttms+j][:-2] = data[i][:]
                self.dataX[i*self.num_strikes*self.num_ttms+j][-2] = self.strike_grid[j%self.num_strikes]
                self.dataX[i*self.num_strikes*self.num_ttms+j][-1]= self.ttm_grid[int(j/self.num_strikes)]
                self.dataY[i*self.num_strikes*self.num_ttms+j][0] =  data[i][j]
                self.data[i*self.num_strikes*self.num_ttms+j][:-1] = self.dataX[i*self.num_strikes*self.num_ttms+j]
                self.data[i*self.num_strikes*self.num_ttms+j][-1] = self.dataY[i*self.num_strikes*self.num_ttms+j]

        #print(self.data[0])
        #print(self.data[10])

        #print(np.count_nonzero(self.data == 0.0))

        self.data_ = torch.utils.data.DataLoader(self.data.astype('float32'), batch_size=self.batch_size, shuffle=False)


    def train(self, epochs: int) -> list:
        opt = self.optimizer
        history = []
        for epoch in range(epochs):
            running_loss = 0.0
            running_rec_loss = 0.0

            for x in self.data_:
                x = x.to("cpu") 
                
                opt.zero_grad()
                y_hat = self.dnn(x[:,:-1])
                y = x[:,-1]
                #print("x,y,y_hat:", x[0,:-1],y[0],y_hat[0])
                rec_loss = ((y - y_hat)**2).sum()

                loss = rec_loss

                loss.backward()
                opt.step()

                running_loss += loss.item()
                running_rec_loss += rec_loss.item()

            running_loss /= self.num_data_points
            running_rec_loss /= self.num_data_points
            
            history.append([running_rec_loss, running_loss])
            print("Epoch:", epoch, "Reconstruction Loss:", running_rec_loss, "Total Loss:", running_loss)

        self.model_is_trained = True
        
        return history


    def save_model(self, filename: str) -> None:
        if self.model_is_trained:
            torch.save(self.dnn, "models/" + filename)
        else: 
            print("Model needs to be trained first!")

        return 

    
    def load_model(self, filename: str) -> None:
        self.dnn = torch.load("models/" + filename)
        self.dnn.eval()

        return


    def predict_mult(self, x: np.ndarray) -> np.ndarray:

        res = self.dnn(torch.from_numpy(x.astype('float32')))

        return res.detach().numpy()

    
    def predict_surface(self, iv_grid: np.array, strikes_test: np.array, ttms_test: np.array) -> np.array:

        x = np.zeros((ttms_test.shape[0]*strikes_test.shape[0], self.num_strikes*self.num_ttms+2))

        for i in range(ttms_test.shape[0]):
            for j in range(strikes_test.shape[0]):
                x[i*strikes_test.shape[0]+j] = np.append(iv_grid,[strikes_test[j],ttms_test[i]]).flatten()

        Z = self.predict_mult(x)
        Z = Z.reshape([ttms_test.size,strikes_test.size])

        return Z


    def predict_all_surfaces(self, iv_grids: np.array, strikes_test: np.array, ttms_test: np.array) -> np.array:
        num_grids = iv_grids.shape[0]

        Z = np.zeros((num_grids, ttms_test.shape[0], strikes_test.shape[0]))

        for i in range(num_grids):
            Z[i,:,:] = self.predict_surface(iv_grids[i], strikes_test, ttms_test)

        return Z


