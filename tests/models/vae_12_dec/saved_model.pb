��
��
8
Const
output"dtype"
valuetensor"
dtypetype

NoOp
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
q
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape�"serve*2.2.02v2.2.0-rc4-8-g2b96f3662b8��
}
dense_1_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*!
shared_namedense_1_3/kernel
v
$dense_1_3/kernel/Read/ReadVariableOpReadVariableOpdense_1_3/kernel*
_output_shapes
:	�*
dtype0
u
dense_1_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*
shared_namedense_1_3/bias
n
"dense_1_3/bias/Read/ReadVariableOpReadVariableOpdense_1_3/bias*
_output_shapes	
:�*
dtype0
�
conv_transpose_1_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:@@**
shared_nameconv_transpose_1_2/kernel
�
-conv_transpose_1_2/kernel/Read/ReadVariableOpReadVariableOpconv_transpose_1_2/kernel*&
_output_shapes
:@@*
dtype0
�
conv_transpose_1_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*(
shared_nameconv_transpose_1_2/bias

+conv_transpose_1_2/bias/Read/ReadVariableOpReadVariableOpconv_transpose_1_2/bias*
_output_shapes
:@*
dtype0
p
bn_1_5/gammaVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namebn_1_5/gamma
i
 bn_1_5/gamma/Read/ReadVariableOpReadVariableOpbn_1_5/gamma*
_output_shapes
:@*
dtype0
n
bn_1_5/betaVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namebn_1_5/beta
g
bn_1_5/beta/Read/ReadVariableOpReadVariableOpbn_1_5/beta*
_output_shapes
:@*
dtype0
|
bn_1_5/moving_meanVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*#
shared_namebn_1_5/moving_mean
u
&bn_1_5/moving_mean/Read/ReadVariableOpReadVariableOpbn_1_5/moving_mean*
_output_shapes
:@*
dtype0
�
bn_1_5/moving_varianceVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*'
shared_namebn_1_5/moving_variance
}
*bn_1_5/moving_variance/Read/ReadVariableOpReadVariableOpbn_1_5/moving_variance*
_output_shapes
:@*
dtype0
�
conv_transpose_2_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:@@**
shared_nameconv_transpose_2_2/kernel
�
-conv_transpose_2_2/kernel/Read/ReadVariableOpReadVariableOpconv_transpose_2_2/kernel*&
_output_shapes
:@@*
dtype0
�
conv_transpose_2_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*(
shared_nameconv_transpose_2_2/bias

+conv_transpose_2_2/bias/Read/ReadVariableOpReadVariableOpconv_transpose_2_2/bias*
_output_shapes
:@*
dtype0
p
bn_2_5/gammaVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namebn_2_5/gamma
i
 bn_2_5/gamma/Read/ReadVariableOpReadVariableOpbn_2_5/gamma*
_output_shapes
:@*
dtype0
n
bn_2_5/betaVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namebn_2_5/beta
g
bn_2_5/beta/Read/ReadVariableOpReadVariableOpbn_2_5/beta*
_output_shapes
:@*
dtype0
|
bn_2_5/moving_meanVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*#
shared_namebn_2_5/moving_mean
u
&bn_2_5/moving_mean/Read/ReadVariableOpReadVariableOpbn_2_5/moving_mean*
_output_shapes
:@*
dtype0
�
bn_2_5/moving_varianceVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*'
shared_namebn_2_5/moving_variance
}
*bn_2_5/moving_variance/Read/ReadVariableOpReadVariableOpbn_2_5/moving_variance*
_output_shapes
:@*
dtype0
�
conv_transpose_3_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:@@**
shared_nameconv_transpose_3_2/kernel
�
-conv_transpose_3_2/kernel/Read/ReadVariableOpReadVariableOpconv_transpose_3_2/kernel*&
_output_shapes
:@@*
dtype0
�
conv_transpose_3_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*(
shared_nameconv_transpose_3_2/bias

+conv_transpose_3_2/bias/Read/ReadVariableOpReadVariableOpconv_transpose_3_2/bias*
_output_shapes
:@*
dtype0
p
bn_3_5/gammaVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namebn_3_5/gamma
i
 bn_3_5/gamma/Read/ReadVariableOpReadVariableOpbn_3_5/gamma*
_output_shapes
:@*
dtype0
n
bn_3_5/betaVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namebn_3_5/beta
g
bn_3_5/beta/Read/ReadVariableOpReadVariableOpbn_3_5/beta*
_output_shapes
:@*
dtype0
|
bn_3_5/moving_meanVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*#
shared_namebn_3_5/moving_mean
u
&bn_3_5/moving_mean/Read/ReadVariableOpReadVariableOpbn_3_5/moving_mean*
_output_shapes
:@*
dtype0
�
bn_3_5/moving_varianceVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*'
shared_namebn_3_5/moving_variance
}
*bn_3_5/moving_variance/Read/ReadVariableOpReadVariableOpbn_3_5/moving_variance*
_output_shapes
:@*
dtype0
�
conv_transpose_4_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:@**
shared_nameconv_transpose_4_2/kernel
�
-conv_transpose_4_2/kernel/Read/ReadVariableOpReadVariableOpconv_transpose_4_2/kernel*&
_output_shapes
:@*
dtype0
�
conv_transpose_4_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*(
shared_nameconv_transpose_4_2/bias

+conv_transpose_4_2/bias/Read/ReadVariableOpReadVariableOpconv_transpose_4_2/bias*
_output_shapes
:*
dtype0

NoOpNoOp
�9
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�8
value�8B�8 B�8
�
layer-0
layer_with_weights-0
layer-1
layer-2
layer_with_weights-1
layer-3
layer_with_weights-2
layer-4
layer-5
layer_with_weights-3
layer-6
layer_with_weights-4
layer-7
	layer-8

layer_with_weights-5

layer-9
layer_with_weights-6
layer-10
layer-11
layer_with_weights-7
layer-12
trainable_variables
	variables
regularization_losses
	keras_api

signatures
 
h

kernel
bias
trainable_variables
	variables
regularization_losses
	keras_api
R
trainable_variables
	variables
regularization_losses
	keras_api
h

kernel
bias
trainable_variables
 	variables
!regularization_losses
"	keras_api
�
#axis
	$gamma
%beta
&moving_mean
'moving_variance
(trainable_variables
)	variables
*regularization_losses
+	keras_api
R
,trainable_variables
-	variables
.regularization_losses
/	keras_api
h

0kernel
1bias
2trainable_variables
3	variables
4regularization_losses
5	keras_api
�
6axis
	7gamma
8beta
9moving_mean
:moving_variance
;trainable_variables
<	variables
=regularization_losses
>	keras_api
R
?trainable_variables
@	variables
Aregularization_losses
B	keras_api
h

Ckernel
Dbias
Etrainable_variables
F	variables
Gregularization_losses
H	keras_api
�
Iaxis
	Jgamma
Kbeta
Lmoving_mean
Mmoving_variance
Ntrainable_variables
O	variables
Pregularization_losses
Q	keras_api
R
Rtrainable_variables
S	variables
Tregularization_losses
U	keras_api
h

Vkernel
Wbias
Xtrainable_variables
Y	variables
Zregularization_losses
[	keras_api
v
0
1
2
3
$4
%5
06
17
78
89
C10
D11
J12
K13
V14
W15
�
0
1
2
3
$4
%5
&6
'7
08
19
710
811
912
:13
C14
D15
J16
K17
L18
M19
V20
W21
 
�
trainable_variables
\layer_metrics
]non_trainable_variables

^layers
	variables
_layer_regularization_losses
regularization_losses
`metrics
 
\Z
VARIABLE_VALUEdense_1_3/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE
XV
VARIABLE_VALUEdense_1_3/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1

0
1
 
�
alayer_metrics
trainable_variables
bnon_trainable_variables

clayers
	variables
dlayer_regularization_losses
regularization_losses
emetrics
 
 
 
�
flayer_metrics
trainable_variables
gnon_trainable_variables

hlayers
	variables
ilayer_regularization_losses
regularization_losses
jmetrics
ec
VARIABLE_VALUEconv_transpose_1_2/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
a_
VARIABLE_VALUEconv_transpose_1_2/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1

0
1
 
�
klayer_metrics
trainable_variables
lnon_trainable_variables

mlayers
 	variables
nlayer_regularization_losses
!regularization_losses
ometrics
 
WU
VARIABLE_VALUEbn_1_5/gamma5layer_with_weights-2/gamma/.ATTRIBUTES/VARIABLE_VALUE
US
VARIABLE_VALUEbn_1_5/beta4layer_with_weights-2/beta/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEbn_1_5/moving_mean;layer_with_weights-2/moving_mean/.ATTRIBUTES/VARIABLE_VALUE
ki
VARIABLE_VALUEbn_1_5/moving_variance?layer_with_weights-2/moving_variance/.ATTRIBUTES/VARIABLE_VALUE

$0
%1

$0
%1
&2
'3
 
�
player_metrics
(trainable_variables
qnon_trainable_variables

rlayers
)	variables
slayer_regularization_losses
*regularization_losses
tmetrics
 
 
 
�
ulayer_metrics
,trainable_variables
vnon_trainable_variables

wlayers
-	variables
xlayer_regularization_losses
.regularization_losses
ymetrics
ec
VARIABLE_VALUEconv_transpose_2_2/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE
a_
VARIABLE_VALUEconv_transpose_2_2/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE

00
11

00
11
 
�
zlayer_metrics
2trainable_variables
{non_trainable_variables

|layers
3	variables
}layer_regularization_losses
4regularization_losses
~metrics
 
WU
VARIABLE_VALUEbn_2_5/gamma5layer_with_weights-4/gamma/.ATTRIBUTES/VARIABLE_VALUE
US
VARIABLE_VALUEbn_2_5/beta4layer_with_weights-4/beta/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEbn_2_5/moving_mean;layer_with_weights-4/moving_mean/.ATTRIBUTES/VARIABLE_VALUE
ki
VARIABLE_VALUEbn_2_5/moving_variance?layer_with_weights-4/moving_variance/.ATTRIBUTES/VARIABLE_VALUE

70
81

70
81
92
:3
 
�
layer_metrics
;trainable_variables
�non_trainable_variables
�layers
<	variables
 �layer_regularization_losses
=regularization_losses
�metrics
 
 
 
�
�layer_metrics
?trainable_variables
�non_trainable_variables
�layers
@	variables
 �layer_regularization_losses
Aregularization_losses
�metrics
ec
VARIABLE_VALUEconv_transpose_3_2/kernel6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUE
a_
VARIABLE_VALUEconv_transpose_3_2/bias4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUE

C0
D1

C0
D1
 
�
�layer_metrics
Etrainable_variables
�non_trainable_variables
�layers
F	variables
 �layer_regularization_losses
Gregularization_losses
�metrics
 
WU
VARIABLE_VALUEbn_3_5/gamma5layer_with_weights-6/gamma/.ATTRIBUTES/VARIABLE_VALUE
US
VARIABLE_VALUEbn_3_5/beta4layer_with_weights-6/beta/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEbn_3_5/moving_mean;layer_with_weights-6/moving_mean/.ATTRIBUTES/VARIABLE_VALUE
ki
VARIABLE_VALUEbn_3_5/moving_variance?layer_with_weights-6/moving_variance/.ATTRIBUTES/VARIABLE_VALUE

J0
K1

J0
K1
L2
M3
 
�
�layer_metrics
Ntrainable_variables
�non_trainable_variables
�layers
O	variables
 �layer_regularization_losses
Pregularization_losses
�metrics
 
 
 
�
�layer_metrics
Rtrainable_variables
�non_trainable_variables
�layers
S	variables
 �layer_regularization_losses
Tregularization_losses
�metrics
ec
VARIABLE_VALUEconv_transpose_4_2/kernel6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUE
a_
VARIABLE_VALUEconv_transpose_4_2/bias4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUE

V0
W1

V0
W1
 
�
�layer_metrics
Xtrainable_variables
�non_trainable_variables
�layers
Y	variables
 �layer_regularization_losses
Zregularization_losses
�metrics
 
*
&0
'1
92
:3
L4
M5
^
0
1
2
3
4
5
6
7
	8

9
10
11
12
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

&0
'1
 
 
 
 
 
 
 
 
 
 
 
 
 
 

90
:1
 
 
 
 
 
 
 
 
 
 
 
 
 
 

L0
M1
 
 
 
 
 
 
 
 
 
 
 
 
 
�
serving_default_decoder_inputPlaceholder*'
_output_shapes
:���������*
dtype0*
shape:���������
�
StatefulPartitionedCallStatefulPartitionedCallserving_default_decoder_inputdense_1_3/kerneldense_1_3/biasconv_transpose_1_2/kernelconv_transpose_1_2/biasbn_1_5/gammabn_1_5/betabn_1_5/moving_meanbn_1_5/moving_varianceconv_transpose_2_2/kernelconv_transpose_2_2/biasbn_2_5/gammabn_2_5/betabn_2_5/moving_meanbn_2_5/moving_varianceconv_transpose_3_2/kernelconv_transpose_3_2/biasbn_3_5/gammabn_3_5/betabn_3_5/moving_meanbn_3_5/moving_varianceconv_transpose_4_2/kernelconv_transpose_4_2/bias*"
Tin
2*
Tout
2*/
_output_shapes
:���������*8
_read_only_resource_inputs
	
**
config_proto

GPU 

CPU2J 8*,
f'R%
#__inference_signature_wrapper_45342
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�	
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename$dense_1_3/kernel/Read/ReadVariableOp"dense_1_3/bias/Read/ReadVariableOp-conv_transpose_1_2/kernel/Read/ReadVariableOp+conv_transpose_1_2/bias/Read/ReadVariableOp bn_1_5/gamma/Read/ReadVariableOpbn_1_5/beta/Read/ReadVariableOp&bn_1_5/moving_mean/Read/ReadVariableOp*bn_1_5/moving_variance/Read/ReadVariableOp-conv_transpose_2_2/kernel/Read/ReadVariableOp+conv_transpose_2_2/bias/Read/ReadVariableOp bn_2_5/gamma/Read/ReadVariableOpbn_2_5/beta/Read/ReadVariableOp&bn_2_5/moving_mean/Read/ReadVariableOp*bn_2_5/moving_variance/Read/ReadVariableOp-conv_transpose_3_2/kernel/Read/ReadVariableOp+conv_transpose_3_2/bias/Read/ReadVariableOp bn_3_5/gamma/Read/ReadVariableOpbn_3_5/beta/Read/ReadVariableOp&bn_3_5/moving_mean/Read/ReadVariableOp*bn_3_5/moving_variance/Read/ReadVariableOp-conv_transpose_4_2/kernel/Read/ReadVariableOp+conv_transpose_4_2/bias/Read/ReadVariableOpConst*#
Tin
2*
Tout
2*
_output_shapes
: * 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*'
f"R 
__inference__traced_save_46265
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamedense_1_3/kerneldense_1_3/biasconv_transpose_1_2/kernelconv_transpose_1_2/biasbn_1_5/gammabn_1_5/betabn_1_5/moving_meanbn_1_5/moving_varianceconv_transpose_2_2/kernelconv_transpose_2_2/biasbn_2_5/gammabn_2_5/betabn_2_5/moving_meanbn_2_5/moving_varianceconv_transpose_3_2/kernelconv_transpose_3_2/biasbn_3_5/gammabn_3_5/betabn_3_5/moving_meanbn_3_5/moving_varianceconv_transpose_4_2/kernelconv_transpose_4_2/bias*"
Tin
2*
Tout
2*
_output_shapes
: * 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8**
f%R#
!__inference__traced_restore_46343��
�
�
B__inference_dense_1_layer_call_and_return_conditional_losses_44773

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAdde
IdentityIdentityBiasAdd:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������:::O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
'__inference_Decoder_layer_call_fn_45794

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20*"
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������*2
_read_only_resource_inputs
	
**
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_Decoder_layer_call_and_return_conditional_losses_450992
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������2

Identity"
identityIdentity:output:0*~
_input_shapesm
k:���������::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
'__inference_Decoder_layer_call_fn_45843

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20*"
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������*8
_read_only_resource_inputs
	
**
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_Decoder_layer_call_and_return_conditional_losses_452082
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������2

Identity"
identityIdentity:output:0*~
_input_shapesm
k:���������::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
'__inference_Decoder_layer_call_fn_45146
decoder_input
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCalldecoder_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20*"
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������*2
_read_only_resource_inputs
	
**
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_Decoder_layer_call_and_return_conditional_losses_450992
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������2

Identity"
identityIdentity:output:0*~
_input_shapesm
k:���������::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:V R
'
_output_shapes
:���������
'
_user_specified_namedecoder_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
$__inference_bn_3_layer_call_fn_46162

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*&
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_3_layer_call_and_return_conditional_losses_447032
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@::::22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�$
�
?__inference_bn_3_layer_call_and_return_conditional_losses_44672

inputs
readvariableop_resource
readvariableop_1_resource,
(fusedbatchnormv3_readvariableop_resource.
*fusedbatchnormv3_readvariableop_1_resource
identity��#AssignMovingAvg/AssignSubVariableOp�%AssignMovingAvg_1/AssignSubVariableOpt
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype02
ReadVariableOpz
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype02
ReadVariableOp_1�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02!
FusedBatchNormV3/ReadVariableOp�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02#
!FusedBatchNormV3/ReadVariableOp_1�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:2
FusedBatchNormV3S
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *�p}?2
Const�
AssignMovingAvg/sub/xConst*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
AssignMovingAvg/sub/x�
AssignMovingAvg/subSubAssignMovingAvg/sub/x:output:0Const:output:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: 2
AssignMovingAvg/sub�
AssignMovingAvg/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/sub_1Sub&AssignMovingAvg/ReadVariableOp:value:0FusedBatchNormV3:batch_mean:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
AssignMovingAvg/sub_1�
AssignMovingAvg/mulMulAssignMovingAvg/sub_1:z:0AssignMovingAvg/sub:z:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
AssignMovingAvg/mul�
#AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp(fusedbatchnormv3_readvariableop_resourceAssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp ^FusedBatchNormV3/ReadVariableOp*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
 *
dtype02%
#AssignMovingAvg/AssignSubVariableOp�
AssignMovingAvg_1/sub/xConst*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
AssignMovingAvg_1/sub/x�
AssignMovingAvg_1/subSub AssignMovingAvg_1/sub/x:output:0Const:output:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: 2
AssignMovingAvg_1/sub�
 AssignMovingAvg_1/ReadVariableOpReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/sub_1Sub(AssignMovingAvg_1/ReadVariableOp:value:0!FusedBatchNormV3:batch_variance:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
AssignMovingAvg_1/sub_1�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub_1:z:0AssignMovingAvg_1/sub:z:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
AssignMovingAvg_1/mul�
%AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp*fusedbatchnormv3_readvariableop_1_resourceAssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
 *
dtype02'
%AssignMovingAvg_1/AssignSubVariableOp�
IdentityIdentityFusedBatchNormV3:y:0$^AssignMovingAvg/AssignSubVariableOp&^AssignMovingAvg_1/AssignSubVariableOp*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@::::2J
#AssignMovingAvg/AssignSubVariableOp#AssignMovingAvg/AssignSubVariableOp2N
%AssignMovingAvg_1/AssignSubVariableOp%AssignMovingAvg_1/AssignSubVariableOp:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�=
�
B__inference_Decoder_layer_call_and_return_conditional_losses_45036
decoder_input
dense_1_44979
dense_1_44981
conv_transpose_1_44985
conv_transpose_1_44987

bn_1_44990

bn_1_44992

bn_1_44994

bn_1_44996
conv_transpose_2_45000
conv_transpose_2_45002

bn_2_45005

bn_2_45007

bn_2_45009

bn_2_45011
conv_transpose_3_45015
conv_transpose_3_45017

bn_3_45020

bn_3_45022

bn_3_45024

bn_3_45026
conv_transpose_4_45030
conv_transpose_4_45032
identity��bn_1/StatefulPartitionedCall�bn_2/StatefulPartitionedCall�bn_3/StatefulPartitionedCall�(conv_transpose_1/StatefulPartitionedCall�(conv_transpose_2/StatefulPartitionedCall�(conv_transpose_3/StatefulPartitionedCall�(conv_transpose_4/StatefulPartitionedCall�dense_1/StatefulPartitionedCall�
dense_1/StatefulPartitionedCallStatefulPartitionedCalldecoder_inputdense_1_44979dense_1_44981*
Tin
2*
Tout
2*(
_output_shapes
:����������*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_dense_1_layer_call_and_return_conditional_losses_447732!
dense_1/StatefulPartitionedCall�
Reshape_Layer/PartitionedCallPartitionedCall(dense_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*/
_output_shapes
:���������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*Q
fLRJ
H__inference_Reshape_Layer_layer_call_and_return_conditional_losses_448032
Reshape_Layer/PartitionedCall�
(conv_transpose_1/StatefulPartitionedCallStatefulPartitionedCall&Reshape_Layer/PartitionedCall:output:0conv_transpose_1_44985conv_transpose_1_44987*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_1_layer_call_and_return_conditional_losses_442382*
(conv_transpose_1/StatefulPartitionedCall�
bn_1/StatefulPartitionedCallStatefulPartitionedCall1conv_transpose_1/StatefulPartitionedCall:output:0
bn_1_44990
bn_1_44992
bn_1_44994
bn_1_44996*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*&
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_1_layer_call_and_return_conditional_losses_443632
bn_1/StatefulPartitionedCall�
lrelu_1/PartitionedCallPartitionedCall%bn_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_lrelu_1_layer_call_and_return_conditional_losses_448562
lrelu_1/PartitionedCall�
(conv_transpose_2/StatefulPartitionedCallStatefulPartitionedCall lrelu_1/PartitionedCall:output:0conv_transpose_2_45000conv_transpose_2_45002*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_2_layer_call_and_return_conditional_losses_444082*
(conv_transpose_2/StatefulPartitionedCall�
bn_2/StatefulPartitionedCallStatefulPartitionedCall1conv_transpose_2/StatefulPartitionedCall:output:0
bn_2_45005
bn_2_45007
bn_2_45009
bn_2_45011*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*&
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_2_layer_call_and_return_conditional_losses_445332
bn_2/StatefulPartitionedCall�
lrelu_2/PartitionedCallPartitionedCall%bn_2/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_lrelu_2_layer_call_and_return_conditional_losses_449092
lrelu_2/PartitionedCall�
(conv_transpose_3/StatefulPartitionedCallStatefulPartitionedCall lrelu_2/PartitionedCall:output:0conv_transpose_3_45015conv_transpose_3_45017*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_3_layer_call_and_return_conditional_losses_445782*
(conv_transpose_3/StatefulPartitionedCall�
bn_3/StatefulPartitionedCallStatefulPartitionedCall1conv_transpose_3/StatefulPartitionedCall:output:0
bn_3_45020
bn_3_45022
bn_3_45024
bn_3_45026*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*&
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_3_layer_call_and_return_conditional_losses_447032
bn_3/StatefulPartitionedCall�
lrelu_3/PartitionedCallPartitionedCall%bn_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_lrelu_3_layer_call_and_return_conditional_losses_449622
lrelu_3/PartitionedCall�
(conv_transpose_4/StatefulPartitionedCallStatefulPartitionedCall lrelu_3/PartitionedCall:output:0conv_transpose_4_45030conv_transpose_4_45032*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_4_layer_call_and_return_conditional_losses_447492*
(conv_transpose_4/StatefulPartitionedCall�
IdentityIdentity1conv_transpose_4/StatefulPartitionedCall:output:0^bn_1/StatefulPartitionedCall^bn_2/StatefulPartitionedCall^bn_3/StatefulPartitionedCall)^conv_transpose_1/StatefulPartitionedCall)^conv_transpose_2/StatefulPartitionedCall)^conv_transpose_3/StatefulPartitionedCall)^conv_transpose_4/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������2

Identity"
identityIdentity:output:0*~
_input_shapesm
k:���������::::::::::::::::::::::2<
bn_1/StatefulPartitionedCallbn_1/StatefulPartitionedCall2<
bn_2/StatefulPartitionedCallbn_2/StatefulPartitionedCall2<
bn_3/StatefulPartitionedCallbn_3/StatefulPartitionedCall2T
(conv_transpose_1/StatefulPartitionedCall(conv_transpose_1/StatefulPartitionedCall2T
(conv_transpose_2/StatefulPartitionedCall(conv_transpose_2/StatefulPartitionedCall2T
(conv_transpose_3/StatefulPartitionedCall(conv_transpose_3/StatefulPartitionedCall2T
(conv_transpose_4/StatefulPartitionedCall(conv_transpose_4/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall:V R
'
_output_shapes
:���������
'
_user_specified_namedecoder_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
$__inference_bn_1_layer_call_fn_45955

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_1_layer_call_and_return_conditional_losses_443322
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@::::22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�#
�
K__inference_conv_transpose_4_layer_call_and_return_conditional_losses_44749

inputs,
(conv2d_transpose_readvariableop_resource#
biasadd_readvariableop_resource
identity�D
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slicex
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2�
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1x
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2�
strided_slice_2StridedSliceShape:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2P
mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
mul/y\
mulMulstrided_slice_1:output:0mul/y:output:0*
T0*
_output_shapes
: 2
mulT
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2	
mul_1/yb
mul_1Mulstrided_slice_2:output:0mul_1/y:output:0*
T0*
_output_shapes
: 2
mul_1T
stack/3Const*
_output_shapes
: *
dtype0*
value	B :2	
stack/3�
stackPackstrided_slice:output:0mul:z:0	mul_1:z:0stack/3:output:0*
N*
T0*
_output_shapes
:2
stackx
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2�
strided_slice_3StridedSlicestack:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_3�
conv2d_transpose/ReadVariableOpReadVariableOp(conv2d_transpose_readvariableop_resource*&
_output_shapes
:@*
dtype02!
conv2d_transpose/ReadVariableOp�
conv2d_transposeConv2DBackpropInputstack:output:0'conv2d_transpose/ReadVariableOp:value:0inputs*
T0*A
_output_shapes/
-:+���������������������������*
paddingSAME*
strides
2
conv2d_transpose�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddconv2d_transpose:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+���������������������������2	
BiasAdd~
SoftplusSoftplusBiasAdd:output:0*
T0*A
_output_shapes/
-:+���������������������������2

Softplus�
IdentityIdentitySoftplus:activations:0*
T0*A
_output_shapes/
-:+���������������������������2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+���������������������������@:::i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
$__inference_bn_2_layer_call_fn_46052

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_2_layer_call_and_return_conditional_losses_445022
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@::::22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�=
�
B__inference_Decoder_layer_call_and_return_conditional_losses_45208

inputs
dense_1_45151
dense_1_45153
conv_transpose_1_45157
conv_transpose_1_45159

bn_1_45162

bn_1_45164

bn_1_45166

bn_1_45168
conv_transpose_2_45172
conv_transpose_2_45174

bn_2_45177

bn_2_45179

bn_2_45181

bn_2_45183
conv_transpose_3_45187
conv_transpose_3_45189

bn_3_45192

bn_3_45194

bn_3_45196

bn_3_45198
conv_transpose_4_45202
conv_transpose_4_45204
identity��bn_1/StatefulPartitionedCall�bn_2/StatefulPartitionedCall�bn_3/StatefulPartitionedCall�(conv_transpose_1/StatefulPartitionedCall�(conv_transpose_2/StatefulPartitionedCall�(conv_transpose_3/StatefulPartitionedCall�(conv_transpose_4/StatefulPartitionedCall�dense_1/StatefulPartitionedCall�
dense_1/StatefulPartitionedCallStatefulPartitionedCallinputsdense_1_45151dense_1_45153*
Tin
2*
Tout
2*(
_output_shapes
:����������*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_dense_1_layer_call_and_return_conditional_losses_447732!
dense_1/StatefulPartitionedCall�
Reshape_Layer/PartitionedCallPartitionedCall(dense_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*/
_output_shapes
:���������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*Q
fLRJ
H__inference_Reshape_Layer_layer_call_and_return_conditional_losses_448032
Reshape_Layer/PartitionedCall�
(conv_transpose_1/StatefulPartitionedCallStatefulPartitionedCall&Reshape_Layer/PartitionedCall:output:0conv_transpose_1_45157conv_transpose_1_45159*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_1_layer_call_and_return_conditional_losses_442382*
(conv_transpose_1/StatefulPartitionedCall�
bn_1/StatefulPartitionedCallStatefulPartitionedCall1conv_transpose_1/StatefulPartitionedCall:output:0
bn_1_45162
bn_1_45164
bn_1_45166
bn_1_45168*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*&
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_1_layer_call_and_return_conditional_losses_443632
bn_1/StatefulPartitionedCall�
lrelu_1/PartitionedCallPartitionedCall%bn_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_lrelu_1_layer_call_and_return_conditional_losses_448562
lrelu_1/PartitionedCall�
(conv_transpose_2/StatefulPartitionedCallStatefulPartitionedCall lrelu_1/PartitionedCall:output:0conv_transpose_2_45172conv_transpose_2_45174*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_2_layer_call_and_return_conditional_losses_444082*
(conv_transpose_2/StatefulPartitionedCall�
bn_2/StatefulPartitionedCallStatefulPartitionedCall1conv_transpose_2/StatefulPartitionedCall:output:0
bn_2_45177
bn_2_45179
bn_2_45181
bn_2_45183*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*&
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_2_layer_call_and_return_conditional_losses_445332
bn_2/StatefulPartitionedCall�
lrelu_2/PartitionedCallPartitionedCall%bn_2/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_lrelu_2_layer_call_and_return_conditional_losses_449092
lrelu_2/PartitionedCall�
(conv_transpose_3/StatefulPartitionedCallStatefulPartitionedCall lrelu_2/PartitionedCall:output:0conv_transpose_3_45187conv_transpose_3_45189*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_3_layer_call_and_return_conditional_losses_445782*
(conv_transpose_3/StatefulPartitionedCall�
bn_3/StatefulPartitionedCallStatefulPartitionedCall1conv_transpose_3/StatefulPartitionedCall:output:0
bn_3_45192
bn_3_45194
bn_3_45196
bn_3_45198*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*&
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_3_layer_call_and_return_conditional_losses_447032
bn_3/StatefulPartitionedCall�
lrelu_3/PartitionedCallPartitionedCall%bn_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_lrelu_3_layer_call_and_return_conditional_losses_449622
lrelu_3/PartitionedCall�
(conv_transpose_4/StatefulPartitionedCallStatefulPartitionedCall lrelu_3/PartitionedCall:output:0conv_transpose_4_45202conv_transpose_4_45204*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_4_layer_call_and_return_conditional_losses_447492*
(conv_transpose_4/StatefulPartitionedCall�
IdentityIdentity1conv_transpose_4/StatefulPartitionedCall:output:0^bn_1/StatefulPartitionedCall^bn_2/StatefulPartitionedCall^bn_3/StatefulPartitionedCall)^conv_transpose_1/StatefulPartitionedCall)^conv_transpose_2/StatefulPartitionedCall)^conv_transpose_3/StatefulPartitionedCall)^conv_transpose_4/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������2

Identity"
identityIdentity:output:0*~
_input_shapesm
k:���������::::::::::::::::::::::2<
bn_1/StatefulPartitionedCallbn_1/StatefulPartitionedCall2<
bn_2/StatefulPartitionedCallbn_2/StatefulPartitionedCall2<
bn_3/StatefulPartitionedCallbn_3/StatefulPartitionedCall2T
(conv_transpose_1/StatefulPartitionedCall(conv_transpose_1/StatefulPartitionedCall2T
(conv_transpose_2/StatefulPartitionedCall(conv_transpose_2/StatefulPartitionedCall2T
(conv_transpose_3/StatefulPartitionedCall(conv_transpose_3/StatefulPartitionedCall2T
(conv_transpose_4/StatefulPartitionedCall(conv_transpose_4/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�$
�
?__inference_bn_1_layer_call_and_return_conditional_losses_44332

inputs
readvariableop_resource
readvariableop_1_resource,
(fusedbatchnormv3_readvariableop_resource.
*fusedbatchnormv3_readvariableop_1_resource
identity��#AssignMovingAvg/AssignSubVariableOp�%AssignMovingAvg_1/AssignSubVariableOpt
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype02
ReadVariableOpz
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype02
ReadVariableOp_1�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02!
FusedBatchNormV3/ReadVariableOp�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02#
!FusedBatchNormV3/ReadVariableOp_1�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:2
FusedBatchNormV3S
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *�p}?2
Const�
AssignMovingAvg/sub/xConst*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
AssignMovingAvg/sub/x�
AssignMovingAvg/subSubAssignMovingAvg/sub/x:output:0Const:output:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: 2
AssignMovingAvg/sub�
AssignMovingAvg/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/sub_1Sub&AssignMovingAvg/ReadVariableOp:value:0FusedBatchNormV3:batch_mean:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
AssignMovingAvg/sub_1�
AssignMovingAvg/mulMulAssignMovingAvg/sub_1:z:0AssignMovingAvg/sub:z:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
AssignMovingAvg/mul�
#AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp(fusedbatchnormv3_readvariableop_resourceAssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp ^FusedBatchNormV3/ReadVariableOp*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
 *
dtype02%
#AssignMovingAvg/AssignSubVariableOp�
AssignMovingAvg_1/sub/xConst*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
AssignMovingAvg_1/sub/x�
AssignMovingAvg_1/subSub AssignMovingAvg_1/sub/x:output:0Const:output:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: 2
AssignMovingAvg_1/sub�
 AssignMovingAvg_1/ReadVariableOpReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/sub_1Sub(AssignMovingAvg_1/ReadVariableOp:value:0!FusedBatchNormV3:batch_variance:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
AssignMovingAvg_1/sub_1�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub_1:z:0AssignMovingAvg_1/sub:z:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
AssignMovingAvg_1/mul�
%AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp*fusedbatchnormv3_readvariableop_1_resourceAssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
 *
dtype02'
%AssignMovingAvg_1/AssignSubVariableOp�
IdentityIdentityFusedBatchNormV3:y:0$^AssignMovingAvg/AssignSubVariableOp&^AssignMovingAvg_1/AssignSubVariableOp*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@::::2J
#AssignMovingAvg/AssignSubVariableOp#AssignMovingAvg/AssignSubVariableOp2N
%AssignMovingAvg_1/AssignSubVariableOp%AssignMovingAvg_1/AssignSubVariableOp:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
?__inference_bn_2_layer_call_and_return_conditional_losses_46039

inputs
readvariableop_resource
readvariableop_1_resource,
(fusedbatchnormv3_readvariableop_resource.
*fusedbatchnormv3_readvariableop_1_resource
identity�t
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype02
ReadVariableOpz
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype02
ReadVariableOp_1�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02!
FusedBatchNormV3/ReadVariableOp�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02#
!FusedBatchNormV3/ReadVariableOp_1�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:*
is_training( 2
FusedBatchNormV3�
IdentityIdentityFusedBatchNormV3:y:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@:::::i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
?__inference_bn_3_layer_call_and_return_conditional_losses_44703

inputs
readvariableop_resource
readvariableop_1_resource,
(fusedbatchnormv3_readvariableop_resource.
*fusedbatchnormv3_readvariableop_1_resource
identity�t
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype02
ReadVariableOpz
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype02
ReadVariableOp_1�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02!
FusedBatchNormV3/ReadVariableOp�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02#
!FusedBatchNormV3/ReadVariableOp_1�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:*
is_training( 2
FusedBatchNormV3�
IdentityIdentityFusedBatchNormV3:y:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@:::::i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
^
B__inference_lrelu_3_layer_call_and_return_conditional_losses_44962

inputs
identity~
	LeakyRelu	LeakyReluinputs*A
_output_shapes/
-:+���������������������������@*
alpha%���>2
	LeakyRelu�
IdentityIdentityLeakyRelu:activations:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*@
_input_shapes/
-:+���������������������������@:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�
|
'__inference_dense_1_layer_call_fn_45862

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*(
_output_shapes
:����������*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_dense_1_layer_call_and_return_conditional_losses_447732
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
#__inference_signature_wrapper_45342
decoder_input
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCalldecoder_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20*"
Tin
2*
Tout
2*/
_output_shapes
:���������*8
_read_only_resource_inputs
	
**
config_proto

GPU 

CPU2J 8*)
f$R"
 __inference__wrapped_model_442042
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*~
_input_shapesm
k:���������::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:V R
'
_output_shapes
:���������
'
_user_specified_namedecoder_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
$__inference_bn_2_layer_call_fn_46065

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*&
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_2_layer_call_and_return_conditional_losses_445332
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@::::22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�$
�
?__inference_bn_3_layer_call_and_return_conditional_losses_46118

inputs
readvariableop_resource
readvariableop_1_resource,
(fusedbatchnormv3_readvariableop_resource.
*fusedbatchnormv3_readvariableop_1_resource
identity��#AssignMovingAvg/AssignSubVariableOp�%AssignMovingAvg_1/AssignSubVariableOpt
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype02
ReadVariableOpz
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype02
ReadVariableOp_1�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02!
FusedBatchNormV3/ReadVariableOp�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02#
!FusedBatchNormV3/ReadVariableOp_1�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:2
FusedBatchNormV3S
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *�p}?2
Const�
AssignMovingAvg/sub/xConst*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
AssignMovingAvg/sub/x�
AssignMovingAvg/subSubAssignMovingAvg/sub/x:output:0Const:output:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: 2
AssignMovingAvg/sub�
AssignMovingAvg/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/sub_1Sub&AssignMovingAvg/ReadVariableOp:value:0FusedBatchNormV3:batch_mean:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
AssignMovingAvg/sub_1�
AssignMovingAvg/mulMulAssignMovingAvg/sub_1:z:0AssignMovingAvg/sub:z:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
AssignMovingAvg/mul�
#AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp(fusedbatchnormv3_readvariableop_resourceAssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp ^FusedBatchNormV3/ReadVariableOp*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
 *
dtype02%
#AssignMovingAvg/AssignSubVariableOp�
AssignMovingAvg_1/sub/xConst*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
AssignMovingAvg_1/sub/x�
AssignMovingAvg_1/subSub AssignMovingAvg_1/sub/x:output:0Const:output:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: 2
AssignMovingAvg_1/sub�
 AssignMovingAvg_1/ReadVariableOpReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/sub_1Sub(AssignMovingAvg_1/ReadVariableOp:value:0!FusedBatchNormV3:batch_variance:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
AssignMovingAvg_1/sub_1�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub_1:z:0AssignMovingAvg_1/sub:z:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
AssignMovingAvg_1/mul�
%AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp*fusedbatchnormv3_readvariableop_1_resourceAssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
 *
dtype02'
%AssignMovingAvg_1/AssignSubVariableOp�
IdentityIdentityFusedBatchNormV3:y:0$^AssignMovingAvg/AssignSubVariableOp&^AssignMovingAvg_1/AssignSubVariableOp*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@::::2J
#AssignMovingAvg/AssignSubVariableOp#AssignMovingAvg/AssignSubVariableOp2N
%AssignMovingAvg_1/AssignSubVariableOp%AssignMovingAvg_1/AssignSubVariableOp:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�"
�
K__inference_conv_transpose_2_layer_call_and_return_conditional_losses_44408

inputs,
(conv2d_transpose_readvariableop_resource#
biasadd_readvariableop_resource
identity�D
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slicex
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2�
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1x
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2�
strided_slice_2StridedSliceShape:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2P
mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
mul/y\
mulMulstrided_slice_1:output:0mul/y:output:0*
T0*
_output_shapes
: 2
mulT
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2	
mul_1/yb
mul_1Mulstrided_slice_2:output:0mul_1/y:output:0*
T0*
_output_shapes
: 2
mul_1T
stack/3Const*
_output_shapes
: *
dtype0*
value	B :@2	
stack/3�
stackPackstrided_slice:output:0mul:z:0	mul_1:z:0stack/3:output:0*
N*
T0*
_output_shapes
:2
stackx
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2�
strided_slice_3StridedSlicestack:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_3�
conv2d_transpose/ReadVariableOpReadVariableOp(conv2d_transpose_readvariableop_resource*&
_output_shapes
:@@*
dtype02!
conv2d_transpose/ReadVariableOp�
conv2d_transposeConv2DBackpropInputstack:output:0'conv2d_transpose/ReadVariableOp:value:0inputs*
T0*A
_output_shapes/
-:+���������������������������@*
paddingSAME*
strides
2
conv2d_transpose�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddconv2d_transpose:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+���������������������������@2	
BiasAdd~
IdentityIdentityBiasAdd:output:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+���������������������������@:::i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
^
B__inference_lrelu_2_layer_call_and_return_conditional_losses_44909

inputs
identity~
	LeakyRelu	LeakyReluinputs*A
_output_shapes/
-:+���������������������������@*
alpha%���>2
	LeakyRelu�
IdentityIdentityLeakyRelu:activations:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*@
_input_shapes/
-:+���������������������������@:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�=
�
B__inference_Decoder_layer_call_and_return_conditional_losses_44976
decoder_input
dense_1_44784
dense_1_44786
conv_transpose_1_44811
conv_transpose_1_44813

bn_1_44842

bn_1_44844

bn_1_44846

bn_1_44848
conv_transpose_2_44864
conv_transpose_2_44866

bn_2_44895

bn_2_44897

bn_2_44899

bn_2_44901
conv_transpose_3_44917
conv_transpose_3_44919

bn_3_44948

bn_3_44950

bn_3_44952

bn_3_44954
conv_transpose_4_44970
conv_transpose_4_44972
identity��bn_1/StatefulPartitionedCall�bn_2/StatefulPartitionedCall�bn_3/StatefulPartitionedCall�(conv_transpose_1/StatefulPartitionedCall�(conv_transpose_2/StatefulPartitionedCall�(conv_transpose_3/StatefulPartitionedCall�(conv_transpose_4/StatefulPartitionedCall�dense_1/StatefulPartitionedCall�
dense_1/StatefulPartitionedCallStatefulPartitionedCalldecoder_inputdense_1_44784dense_1_44786*
Tin
2*
Tout
2*(
_output_shapes
:����������*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_dense_1_layer_call_and_return_conditional_losses_447732!
dense_1/StatefulPartitionedCall�
Reshape_Layer/PartitionedCallPartitionedCall(dense_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*/
_output_shapes
:���������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*Q
fLRJ
H__inference_Reshape_Layer_layer_call_and_return_conditional_losses_448032
Reshape_Layer/PartitionedCall�
(conv_transpose_1/StatefulPartitionedCallStatefulPartitionedCall&Reshape_Layer/PartitionedCall:output:0conv_transpose_1_44811conv_transpose_1_44813*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_1_layer_call_and_return_conditional_losses_442382*
(conv_transpose_1/StatefulPartitionedCall�
bn_1/StatefulPartitionedCallStatefulPartitionedCall1conv_transpose_1/StatefulPartitionedCall:output:0
bn_1_44842
bn_1_44844
bn_1_44846
bn_1_44848*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_1_layer_call_and_return_conditional_losses_443322
bn_1/StatefulPartitionedCall�
lrelu_1/PartitionedCallPartitionedCall%bn_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_lrelu_1_layer_call_and_return_conditional_losses_448562
lrelu_1/PartitionedCall�
(conv_transpose_2/StatefulPartitionedCallStatefulPartitionedCall lrelu_1/PartitionedCall:output:0conv_transpose_2_44864conv_transpose_2_44866*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_2_layer_call_and_return_conditional_losses_444082*
(conv_transpose_2/StatefulPartitionedCall�
bn_2/StatefulPartitionedCallStatefulPartitionedCall1conv_transpose_2/StatefulPartitionedCall:output:0
bn_2_44895
bn_2_44897
bn_2_44899
bn_2_44901*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_2_layer_call_and_return_conditional_losses_445022
bn_2/StatefulPartitionedCall�
lrelu_2/PartitionedCallPartitionedCall%bn_2/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_lrelu_2_layer_call_and_return_conditional_losses_449092
lrelu_2/PartitionedCall�
(conv_transpose_3/StatefulPartitionedCallStatefulPartitionedCall lrelu_2/PartitionedCall:output:0conv_transpose_3_44917conv_transpose_3_44919*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_3_layer_call_and_return_conditional_losses_445782*
(conv_transpose_3/StatefulPartitionedCall�
bn_3/StatefulPartitionedCallStatefulPartitionedCall1conv_transpose_3/StatefulPartitionedCall:output:0
bn_3_44948
bn_3_44950
bn_3_44952
bn_3_44954*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_3_layer_call_and_return_conditional_losses_446722
bn_3/StatefulPartitionedCall�
lrelu_3/PartitionedCallPartitionedCall%bn_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_lrelu_3_layer_call_and_return_conditional_losses_449622
lrelu_3/PartitionedCall�
(conv_transpose_4/StatefulPartitionedCallStatefulPartitionedCall lrelu_3/PartitionedCall:output:0conv_transpose_4_44970conv_transpose_4_44972*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_4_layer_call_and_return_conditional_losses_447492*
(conv_transpose_4/StatefulPartitionedCall�
IdentityIdentity1conv_transpose_4/StatefulPartitionedCall:output:0^bn_1/StatefulPartitionedCall^bn_2/StatefulPartitionedCall^bn_3/StatefulPartitionedCall)^conv_transpose_1/StatefulPartitionedCall)^conv_transpose_2/StatefulPartitionedCall)^conv_transpose_3/StatefulPartitionedCall)^conv_transpose_4/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������2

Identity"
identityIdentity:output:0*~
_input_shapesm
k:���������::::::::::::::::::::::2<
bn_1/StatefulPartitionedCallbn_1/StatefulPartitionedCall2<
bn_2/StatefulPartitionedCallbn_2/StatefulPartitionedCall2<
bn_3/StatefulPartitionedCallbn_3/StatefulPartitionedCall2T
(conv_transpose_1/StatefulPartitionedCall(conv_transpose_1/StatefulPartitionedCall2T
(conv_transpose_2/StatefulPartitionedCall(conv_transpose_2/StatefulPartitionedCall2T
(conv_transpose_3/StatefulPartitionedCall(conv_transpose_3/StatefulPartitionedCall2T
(conv_transpose_4/StatefulPartitionedCall(conv_transpose_4/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall:V R
'
_output_shapes
:���������
'
_user_specified_namedecoder_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
^
B__inference_lrelu_2_layer_call_and_return_conditional_losses_46070

inputs
identity~
	LeakyRelu	LeakyReluinputs*A
_output_shapes/
-:+���������������������������@*
alpha%���>2
	LeakyRelu�
IdentityIdentityLeakyRelu:activations:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*@
_input_shapes/
-:+���������������������������@:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�b
�
!__inference__traced_restore_46343
file_prefix%
!assignvariableop_dense_1_3_kernel%
!assignvariableop_1_dense_1_3_bias0
,assignvariableop_2_conv_transpose_1_2_kernel.
*assignvariableop_3_conv_transpose_1_2_bias#
assignvariableop_4_bn_1_5_gamma"
assignvariableop_5_bn_1_5_beta)
%assignvariableop_6_bn_1_5_moving_mean-
)assignvariableop_7_bn_1_5_moving_variance0
,assignvariableop_8_conv_transpose_2_2_kernel.
*assignvariableop_9_conv_transpose_2_2_bias$
 assignvariableop_10_bn_2_5_gamma#
assignvariableop_11_bn_2_5_beta*
&assignvariableop_12_bn_2_5_moving_mean.
*assignvariableop_13_bn_2_5_moving_variance1
-assignvariableop_14_conv_transpose_3_2_kernel/
+assignvariableop_15_conv_transpose_3_2_bias$
 assignvariableop_16_bn_3_5_gamma#
assignvariableop_17_bn_3_5_beta*
&assignvariableop_18_bn_3_5_moving_mean.
*assignvariableop_19_bn_3_5_moving_variance1
-assignvariableop_20_conv_transpose_4_2_kernel/
+assignvariableop_21_conv_transpose_4_2_bias
identity_23��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_3�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�	RestoreV2�RestoreV2_1�

RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�	
value�	B�	B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-2/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-2/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-2/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-4/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-4/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-4/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-6/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-6/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-6/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUE2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*?
value6B4B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*l
_output_shapesZ
X::::::::::::::::::::::*$
dtypes
22
	RestoreV2X
IdentityIdentityRestoreV2:tensors:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOp!assignvariableop_dense_1_3_kernelIdentity:output:0*
_output_shapes
 *
dtype02
AssignVariableOp\

Identity_1IdentityRestoreV2:tensors:1*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOp!assignvariableop_1_dense_1_3_biasIdentity_1:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_1\

Identity_2IdentityRestoreV2:tensors:2*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp,assignvariableop_2_conv_transpose_1_2_kernelIdentity_2:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_2\

Identity_3IdentityRestoreV2:tensors:3*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOp*assignvariableop_3_conv_transpose_1_2_biasIdentity_3:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_3\

Identity_4IdentityRestoreV2:tensors:4*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOpassignvariableop_4_bn_1_5_gammaIdentity_4:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_4\

Identity_5IdentityRestoreV2:tensors:5*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOpassignvariableop_5_bn_1_5_betaIdentity_5:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_5\

Identity_6IdentityRestoreV2:tensors:6*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp%assignvariableop_6_bn_1_5_moving_meanIdentity_6:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_6\

Identity_7IdentityRestoreV2:tensors:7*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOp)assignvariableop_7_bn_1_5_moving_varianceIdentity_7:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_7\

Identity_8IdentityRestoreV2:tensors:8*
T0*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOp,assignvariableop_8_conv_transpose_2_2_kernelIdentity_8:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_8\

Identity_9IdentityRestoreV2:tensors:9*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOp*assignvariableop_9_conv_transpose_2_2_biasIdentity_9:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_9_
Identity_10IdentityRestoreV2:tensors:10*
T0*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOp assignvariableop_10_bn_2_5_gammaIdentity_10:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_10_
Identity_11IdentityRestoreV2:tensors:11*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOpassignvariableop_11_bn_2_5_betaIdentity_11:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_11_
Identity_12IdentityRestoreV2:tensors:12*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOp&assignvariableop_12_bn_2_5_moving_meanIdentity_12:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_12_
Identity_13IdentityRestoreV2:tensors:13*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOp*assignvariableop_13_bn_2_5_moving_varianceIdentity_13:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_13_
Identity_14IdentityRestoreV2:tensors:14*
T0*
_output_shapes
:2
Identity_14�
AssignVariableOp_14AssignVariableOp-assignvariableop_14_conv_transpose_3_2_kernelIdentity_14:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_14_
Identity_15IdentityRestoreV2:tensors:15*
T0*
_output_shapes
:2
Identity_15�
AssignVariableOp_15AssignVariableOp+assignvariableop_15_conv_transpose_3_2_biasIdentity_15:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_15_
Identity_16IdentityRestoreV2:tensors:16*
T0*
_output_shapes
:2
Identity_16�
AssignVariableOp_16AssignVariableOp assignvariableop_16_bn_3_5_gammaIdentity_16:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_16_
Identity_17IdentityRestoreV2:tensors:17*
T0*
_output_shapes
:2
Identity_17�
AssignVariableOp_17AssignVariableOpassignvariableop_17_bn_3_5_betaIdentity_17:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_17_
Identity_18IdentityRestoreV2:tensors:18*
T0*
_output_shapes
:2
Identity_18�
AssignVariableOp_18AssignVariableOp&assignvariableop_18_bn_3_5_moving_meanIdentity_18:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_18_
Identity_19IdentityRestoreV2:tensors:19*
T0*
_output_shapes
:2
Identity_19�
AssignVariableOp_19AssignVariableOp*assignvariableop_19_bn_3_5_moving_varianceIdentity_19:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_19_
Identity_20IdentityRestoreV2:tensors:20*
T0*
_output_shapes
:2
Identity_20�
AssignVariableOp_20AssignVariableOp-assignvariableop_20_conv_transpose_4_2_kernelIdentity_20:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_20_
Identity_21IdentityRestoreV2:tensors:21*
T0*
_output_shapes
:2
Identity_21�
AssignVariableOp_21AssignVariableOp+assignvariableop_21_conv_transpose_4_2_biasIdentity_21:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_21�
RestoreV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2_1/tensor_names�
RestoreV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
RestoreV2_1/shape_and_slices�
RestoreV2_1	RestoreV2file_prefix!RestoreV2_1/tensor_names:output:0%RestoreV2_1/shape_and_slices:output:0
^RestoreV2"/device:CPU:0*
_output_shapes
:*
dtypes
22
RestoreV2_19
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_22Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_22�
Identity_23IdentityIdentity_22:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9
^RestoreV2^RestoreV2_1*
T0*
_output_shapes
: 2
Identity_23"#
identity_23Identity_23:output:0*m
_input_shapes\
Z: ::::::::::::::::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_92
	RestoreV2	RestoreV22
RestoreV2_1RestoreV2_1:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
d
H__inference_Reshape_Layer_layer_call_and_return_conditional_losses_45876

inputs
identityD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_sliced
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/1d
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/2d
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :@2
Reshape/shape/3�
Reshape/shapePackstrided_slice:output:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape/shapew
ReshapeReshapeinputsReshape/shape:output:0*
T0*/
_output_shapes
:���������@2	
Reshapel
IdentityIdentityReshape:output:0*
T0*/
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
C
'__inference_lrelu_1_layer_call_fn_45978

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_lrelu_1_layer_call_and_return_conditional_losses_448562
PartitionedCall�
IdentityIdentityPartitionedCall:output:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*@
_input_shapes/
-:+���������������������������@:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�"
�
K__inference_conv_transpose_1_layer_call_and_return_conditional_losses_44238

inputs,
(conv2d_transpose_readvariableop_resource#
biasadd_readvariableop_resource
identity�D
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slicex
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2�
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1x
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2�
strided_slice_2StridedSliceShape:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2P
mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
mul/y\
mulMulstrided_slice_1:output:0mul/y:output:0*
T0*
_output_shapes
: 2
mulT
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2	
mul_1/yb
mul_1Mulstrided_slice_2:output:0mul_1/y:output:0*
T0*
_output_shapes
: 2
mul_1T
stack/3Const*
_output_shapes
: *
dtype0*
value	B :@2	
stack/3�
stackPackstrided_slice:output:0mul:z:0	mul_1:z:0stack/3:output:0*
N*
T0*
_output_shapes
:2
stackx
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2�
strided_slice_3StridedSlicestack:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_3�
conv2d_transpose/ReadVariableOpReadVariableOp(conv2d_transpose_readvariableop_resource*&
_output_shapes
:@@*
dtype02!
conv2d_transpose/ReadVariableOp�
conv2d_transposeConv2DBackpropInputstack:output:0'conv2d_transpose/ReadVariableOp:value:0inputs*
T0*A
_output_shapes/
-:+���������������������������@*
paddingSAME*
strides
2
conv2d_transpose�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddconv2d_transpose:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+���������������������������@2	
BiasAdd~
IdentityIdentityBiasAdd:output:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+���������������������������@:::i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�$
�
?__inference_bn_2_layer_call_and_return_conditional_losses_44502

inputs
readvariableop_resource
readvariableop_1_resource,
(fusedbatchnormv3_readvariableop_resource.
*fusedbatchnormv3_readvariableop_1_resource
identity��#AssignMovingAvg/AssignSubVariableOp�%AssignMovingAvg_1/AssignSubVariableOpt
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype02
ReadVariableOpz
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype02
ReadVariableOp_1�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02!
FusedBatchNormV3/ReadVariableOp�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02#
!FusedBatchNormV3/ReadVariableOp_1�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:2
FusedBatchNormV3S
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *�p}?2
Const�
AssignMovingAvg/sub/xConst*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
AssignMovingAvg/sub/x�
AssignMovingAvg/subSubAssignMovingAvg/sub/x:output:0Const:output:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: 2
AssignMovingAvg/sub�
AssignMovingAvg/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/sub_1Sub&AssignMovingAvg/ReadVariableOp:value:0FusedBatchNormV3:batch_mean:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
AssignMovingAvg/sub_1�
AssignMovingAvg/mulMulAssignMovingAvg/sub_1:z:0AssignMovingAvg/sub:z:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
AssignMovingAvg/mul�
#AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp(fusedbatchnormv3_readvariableop_resourceAssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp ^FusedBatchNormV3/ReadVariableOp*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
 *
dtype02%
#AssignMovingAvg/AssignSubVariableOp�
AssignMovingAvg_1/sub/xConst*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
AssignMovingAvg_1/sub/x�
AssignMovingAvg_1/subSub AssignMovingAvg_1/sub/x:output:0Const:output:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: 2
AssignMovingAvg_1/sub�
 AssignMovingAvg_1/ReadVariableOpReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/sub_1Sub(AssignMovingAvg_1/ReadVariableOp:value:0!FusedBatchNormV3:batch_variance:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
AssignMovingAvg_1/sub_1�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub_1:z:0AssignMovingAvg_1/sub:z:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
AssignMovingAvg_1/mul�
%AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp*fusedbatchnormv3_readvariableop_1_resourceAssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
 *
dtype02'
%AssignMovingAvg_1/AssignSubVariableOp�
IdentityIdentityFusedBatchNormV3:y:0$^AssignMovingAvg/AssignSubVariableOp&^AssignMovingAvg_1/AssignSubVariableOp*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@::::2J
#AssignMovingAvg/AssignSubVariableOp#AssignMovingAvg/AssignSubVariableOp2N
%AssignMovingAvg_1/AssignSubVariableOp%AssignMovingAvg_1/AssignSubVariableOp:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
C
'__inference_lrelu_3_layer_call_fn_46172

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_lrelu_3_layer_call_and_return_conditional_losses_449622
PartitionedCall�
IdentityIdentityPartitionedCall:output:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*@
_input_shapes/
-:+���������������������������@:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�
�
0__inference_conv_transpose_1_layer_call_fn_44248

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_1_layer_call_and_return_conditional_losses_442382
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+���������������������������@::22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�=
�	
__inference__traced_save_46265
file_prefix/
+savev2_dense_1_3_kernel_read_readvariableop-
)savev2_dense_1_3_bias_read_readvariableop8
4savev2_conv_transpose_1_2_kernel_read_readvariableop6
2savev2_conv_transpose_1_2_bias_read_readvariableop+
'savev2_bn_1_5_gamma_read_readvariableop*
&savev2_bn_1_5_beta_read_readvariableop1
-savev2_bn_1_5_moving_mean_read_readvariableop5
1savev2_bn_1_5_moving_variance_read_readvariableop8
4savev2_conv_transpose_2_2_kernel_read_readvariableop6
2savev2_conv_transpose_2_2_bias_read_readvariableop+
'savev2_bn_2_5_gamma_read_readvariableop*
&savev2_bn_2_5_beta_read_readvariableop1
-savev2_bn_2_5_moving_mean_read_readvariableop5
1savev2_bn_2_5_moving_variance_read_readvariableop8
4savev2_conv_transpose_3_2_kernel_read_readvariableop6
2savev2_conv_transpose_3_2_bias_read_readvariableop+
'savev2_bn_3_5_gamma_read_readvariableop*
&savev2_bn_3_5_beta_read_readvariableop1
-savev2_bn_3_5_moving_mean_read_readvariableop5
1savev2_bn_3_5_moving_variance_read_readvariableop8
4savev2_conv_transpose_4_2_kernel_read_readvariableop6
2savev2_conv_transpose_4_2_bias_read_readvariableop
savev2_1_const

identity_1��MergeV2Checkpoints�SaveV2�SaveV2_1�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Const�
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*<
value3B1 B+_temp_860bc06d23114cd087f04d5c58222a58/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�

SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�	
value�	B�	B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-2/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-2/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-2/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-4/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-4/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-4/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-6/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-6/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-6/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUE2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*?
value6B4B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�	
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0+savev2_dense_1_3_kernel_read_readvariableop)savev2_dense_1_3_bias_read_readvariableop4savev2_conv_transpose_1_2_kernel_read_readvariableop2savev2_conv_transpose_1_2_bias_read_readvariableop'savev2_bn_1_5_gamma_read_readvariableop&savev2_bn_1_5_beta_read_readvariableop-savev2_bn_1_5_moving_mean_read_readvariableop1savev2_bn_1_5_moving_variance_read_readvariableop4savev2_conv_transpose_2_2_kernel_read_readvariableop2savev2_conv_transpose_2_2_bias_read_readvariableop'savev2_bn_2_5_gamma_read_readvariableop&savev2_bn_2_5_beta_read_readvariableop-savev2_bn_2_5_moving_mean_read_readvariableop1savev2_bn_2_5_moving_variance_read_readvariableop4savev2_conv_transpose_3_2_kernel_read_readvariableop2savev2_conv_transpose_3_2_bias_read_readvariableop'savev2_bn_3_5_gamma_read_readvariableop&savev2_bn_3_5_beta_read_readvariableop-savev2_bn_3_5_moving_mean_read_readvariableop1savev2_bn_3_5_moving_variance_read_readvariableop4savev2_conv_transpose_4_2_kernel_read_readvariableop2savev2_conv_transpose_4_2_bias_read_readvariableop"/device:CPU:0*
_output_shapes
 *$
dtypes
22
SaveV2�
ShardedFilename_1/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B :2
ShardedFilename_1/shard�
ShardedFilename_1ShardedFilenameStringJoin:output:0 ShardedFilename_1/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename_1�
SaveV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2_1/tensor_names�
SaveV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
SaveV2_1/shape_and_slices�
SaveV2_1SaveV2ShardedFilename_1:filename:0SaveV2_1/tensor_names:output:0"SaveV2_1/shape_and_slices:output:0savev2_1_const^SaveV2"/device:CPU:0*
_output_shapes
 *
dtypes
22

SaveV2_1�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0ShardedFilename_1:filename:0^SaveV2	^SaveV2_1"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix	^SaveV2_1"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity�

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints^SaveV2	^SaveV2_1*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*�
_input_shapes�
�: :	�:�:@@:@:@:@:@:@:@@:@:@:@:@:@:@@:@:@:@:@:@:@:: 2(
MergeV2CheckpointsMergeV2Checkpoints2
SaveV2SaveV22
SaveV2_1SaveV2_1:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:%!

_output_shapes
:	�:!

_output_shapes	
:�:,(
&
_output_shapes
:@@: 

_output_shapes
:@: 

_output_shapes
:@: 

_output_shapes
:@: 

_output_shapes
:@: 

_output_shapes
:@:,	(
&
_output_shapes
:@@: 


_output_shapes
:@: 

_output_shapes
:@: 

_output_shapes
:@: 

_output_shapes
:@: 

_output_shapes
:@:,(
&
_output_shapes
:@@: 

_output_shapes
:@: 

_output_shapes
:@: 

_output_shapes
:@: 

_output_shapes
:@: 

_output_shapes
:@:,(
&
_output_shapes
:@: 

_output_shapes
::

_output_shapes
: 
�
d
H__inference_Reshape_Layer_layer_call_and_return_conditional_losses_44803

inputs
identityD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_sliced
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/1d
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/2d
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :@2
Reshape/shape/3�
Reshape/shapePackstrided_slice:output:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape/shapew
ReshapeReshapeinputsReshape/shape:output:0*
T0*/
_output_shapes
:���������@2	
Reshapel
IdentityIdentityReshape:output:0*
T0*/
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
$__inference_bn_3_layer_call_fn_46149

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_3_layer_call_and_return_conditional_losses_446722
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@::::22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
?__inference_bn_1_layer_call_and_return_conditional_losses_45942

inputs
readvariableop_resource
readvariableop_1_resource,
(fusedbatchnormv3_readvariableop_resource.
*fusedbatchnormv3_readvariableop_1_resource
identity�t
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype02
ReadVariableOpz
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype02
ReadVariableOp_1�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02!
FusedBatchNormV3/ReadVariableOp�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02#
!FusedBatchNormV3/ReadVariableOp_1�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:*
is_training( 2
FusedBatchNormV3�
IdentityIdentityFusedBatchNormV3:y:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@:::::i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
0__inference_conv_transpose_2_layer_call_fn_44418

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_2_layer_call_and_return_conditional_losses_444082
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+���������������������������@::22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�"
�
K__inference_conv_transpose_3_layer_call_and_return_conditional_losses_44578

inputs,
(conv2d_transpose_readvariableop_resource#
biasadd_readvariableop_resource
identity�D
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slicex
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2�
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1x
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2�
strided_slice_2StridedSliceShape:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2P
mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
mul/y\
mulMulstrided_slice_1:output:0mul/y:output:0*
T0*
_output_shapes
: 2
mulT
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2	
mul_1/yb
mul_1Mulstrided_slice_2:output:0mul_1/y:output:0*
T0*
_output_shapes
: 2
mul_1T
stack/3Const*
_output_shapes
: *
dtype0*
value	B :@2	
stack/3�
stackPackstrided_slice:output:0mul:z:0	mul_1:z:0stack/3:output:0*
N*
T0*
_output_shapes
:2
stackx
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2�
strided_slice_3StridedSlicestack:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_3�
conv2d_transpose/ReadVariableOpReadVariableOp(conv2d_transpose_readvariableop_resource*&
_output_shapes
:@@*
dtype02!
conv2d_transpose/ReadVariableOp�
conv2d_transposeConv2DBackpropInputstack:output:0'conv2d_transpose/ReadVariableOp:value:0inputs*
T0*A
_output_shapes/
-:+���������������������������@*
paddingSAME*
strides
2
conv2d_transpose�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddconv2d_transpose:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+���������������������������@2	
BiasAdd~
IdentityIdentityBiasAdd:output:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+���������������������������@:::i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
^
B__inference_lrelu_1_layer_call_and_return_conditional_losses_44856

inputs
identity~
	LeakyRelu	LeakyReluinputs*A
_output_shapes/
-:+���������������������������@*
alpha%���>2
	LeakyRelu�
IdentityIdentityLeakyRelu:activations:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*@
_input_shapes/
-:+���������������������������@:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�
^
B__inference_lrelu_1_layer_call_and_return_conditional_losses_45973

inputs
identity~
	LeakyRelu	LeakyReluinputs*A
_output_shapes/
-:+���������������������������@*
alpha%���>2
	LeakyRelu�
IdentityIdentityLeakyRelu:activations:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*@
_input_shapes/
-:+���������������������������@:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
҃
�

 __inference__wrapped_model_44204
decoder_input2
.decoder_dense_1_matmul_readvariableop_resource3
/decoder_dense_1_biasadd_readvariableop_resourceE
Adecoder_conv_transpose_1_conv2d_transpose_readvariableop_resource<
8decoder_conv_transpose_1_biasadd_readvariableop_resource(
$decoder_bn_1_readvariableop_resource*
&decoder_bn_1_readvariableop_1_resource9
5decoder_bn_1_fusedbatchnormv3_readvariableop_resource;
7decoder_bn_1_fusedbatchnormv3_readvariableop_1_resourceE
Adecoder_conv_transpose_2_conv2d_transpose_readvariableop_resource<
8decoder_conv_transpose_2_biasadd_readvariableop_resource(
$decoder_bn_2_readvariableop_resource*
&decoder_bn_2_readvariableop_1_resource9
5decoder_bn_2_fusedbatchnormv3_readvariableop_resource;
7decoder_bn_2_fusedbatchnormv3_readvariableop_1_resourceE
Adecoder_conv_transpose_3_conv2d_transpose_readvariableop_resource<
8decoder_conv_transpose_3_biasadd_readvariableop_resource(
$decoder_bn_3_readvariableop_resource*
&decoder_bn_3_readvariableop_1_resource9
5decoder_bn_3_fusedbatchnormv3_readvariableop_resource;
7decoder_bn_3_fusedbatchnormv3_readvariableop_1_resourceE
Adecoder_conv_transpose_4_conv2d_transpose_readvariableop_resource<
8decoder_conv_transpose_4_biasadd_readvariableop_resource
identity��
%Decoder/dense_1/MatMul/ReadVariableOpReadVariableOp.decoder_dense_1_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02'
%Decoder/dense_1/MatMul/ReadVariableOp�
Decoder/dense_1/MatMulMatMuldecoder_input-Decoder/dense_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
Decoder/dense_1/MatMul�
&Decoder/dense_1/BiasAdd/ReadVariableOpReadVariableOp/decoder_dense_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02(
&Decoder/dense_1/BiasAdd/ReadVariableOp�
Decoder/dense_1/BiasAddBiasAdd Decoder/dense_1/MatMul:product:0.Decoder/dense_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
Decoder/dense_1/BiasAdd�
Decoder/Reshape_Layer/ShapeShape Decoder/dense_1/BiasAdd:output:0*
T0*
_output_shapes
:2
Decoder/Reshape_Layer/Shape�
)Decoder/Reshape_Layer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2+
)Decoder/Reshape_Layer/strided_slice/stack�
+Decoder/Reshape_Layer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+Decoder/Reshape_Layer/strided_slice/stack_1�
+Decoder/Reshape_Layer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+Decoder/Reshape_Layer/strided_slice/stack_2�
#Decoder/Reshape_Layer/strided_sliceStridedSlice$Decoder/Reshape_Layer/Shape:output:02Decoder/Reshape_Layer/strided_slice/stack:output:04Decoder/Reshape_Layer/strided_slice/stack_1:output:04Decoder/Reshape_Layer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#Decoder/Reshape_Layer/strided_slice�
%Decoder/Reshape_Layer/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2'
%Decoder/Reshape_Layer/Reshape/shape/1�
%Decoder/Reshape_Layer/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2'
%Decoder/Reshape_Layer/Reshape/shape/2�
%Decoder/Reshape_Layer/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :@2'
%Decoder/Reshape_Layer/Reshape/shape/3�
#Decoder/Reshape_Layer/Reshape/shapePack,Decoder/Reshape_Layer/strided_slice:output:0.Decoder/Reshape_Layer/Reshape/shape/1:output:0.Decoder/Reshape_Layer/Reshape/shape/2:output:0.Decoder/Reshape_Layer/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2%
#Decoder/Reshape_Layer/Reshape/shape�
Decoder/Reshape_Layer/ReshapeReshape Decoder/dense_1/BiasAdd:output:0,Decoder/Reshape_Layer/Reshape/shape:output:0*
T0*/
_output_shapes
:���������@2
Decoder/Reshape_Layer/Reshape�
Decoder/conv_transpose_1/ShapeShape&Decoder/Reshape_Layer/Reshape:output:0*
T0*
_output_shapes
:2 
Decoder/conv_transpose_1/Shape�
,Decoder/conv_transpose_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2.
,Decoder/conv_transpose_1/strided_slice/stack�
.Decoder/conv_transpose_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_1/strided_slice/stack_1�
.Decoder/conv_transpose_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_1/strided_slice/stack_2�
&Decoder/conv_transpose_1/strided_sliceStridedSlice'Decoder/conv_transpose_1/Shape:output:05Decoder/conv_transpose_1/strided_slice/stack:output:07Decoder/conv_transpose_1/strided_slice/stack_1:output:07Decoder/conv_transpose_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2(
&Decoder/conv_transpose_1/strided_slice�
.Decoder/conv_transpose_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_1/strided_slice_1/stack�
0Decoder/conv_transpose_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_1/strided_slice_1/stack_1�
0Decoder/conv_transpose_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_1/strided_slice_1/stack_2�
(Decoder/conv_transpose_1/strided_slice_1StridedSlice'Decoder/conv_transpose_1/Shape:output:07Decoder/conv_transpose_1/strided_slice_1/stack:output:09Decoder/conv_transpose_1/strided_slice_1/stack_1:output:09Decoder/conv_transpose_1/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(Decoder/conv_transpose_1/strided_slice_1�
.Decoder/conv_transpose_1/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_1/strided_slice_2/stack�
0Decoder/conv_transpose_1/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_1/strided_slice_2/stack_1�
0Decoder/conv_transpose_1/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_1/strided_slice_2/stack_2�
(Decoder/conv_transpose_1/strided_slice_2StridedSlice'Decoder/conv_transpose_1/Shape:output:07Decoder/conv_transpose_1/strided_slice_2/stack:output:09Decoder/conv_transpose_1/strided_slice_2/stack_1:output:09Decoder/conv_transpose_1/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(Decoder/conv_transpose_1/strided_slice_2�
Decoder/conv_transpose_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2 
Decoder/conv_transpose_1/mul/y�
Decoder/conv_transpose_1/mulMul1Decoder/conv_transpose_1/strided_slice_1:output:0'Decoder/conv_transpose_1/mul/y:output:0*
T0*
_output_shapes
: 2
Decoder/conv_transpose_1/mul�
 Decoder/conv_transpose_1/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2"
 Decoder/conv_transpose_1/mul_1/y�
Decoder/conv_transpose_1/mul_1Mul1Decoder/conv_transpose_1/strided_slice_2:output:0)Decoder/conv_transpose_1/mul_1/y:output:0*
T0*
_output_shapes
: 2 
Decoder/conv_transpose_1/mul_1�
 Decoder/conv_transpose_1/stack/3Const*
_output_shapes
: *
dtype0*
value	B :@2"
 Decoder/conv_transpose_1/stack/3�
Decoder/conv_transpose_1/stackPack/Decoder/conv_transpose_1/strided_slice:output:0 Decoder/conv_transpose_1/mul:z:0"Decoder/conv_transpose_1/mul_1:z:0)Decoder/conv_transpose_1/stack/3:output:0*
N*
T0*
_output_shapes
:2 
Decoder/conv_transpose_1/stack�
.Decoder/conv_transpose_1/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 20
.Decoder/conv_transpose_1/strided_slice_3/stack�
0Decoder/conv_transpose_1/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_1/strided_slice_3/stack_1�
0Decoder/conv_transpose_1/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_1/strided_slice_3/stack_2�
(Decoder/conv_transpose_1/strided_slice_3StridedSlice'Decoder/conv_transpose_1/stack:output:07Decoder/conv_transpose_1/strided_slice_3/stack:output:09Decoder/conv_transpose_1/strided_slice_3/stack_1:output:09Decoder/conv_transpose_1/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(Decoder/conv_transpose_1/strided_slice_3�
8Decoder/conv_transpose_1/conv2d_transpose/ReadVariableOpReadVariableOpAdecoder_conv_transpose_1_conv2d_transpose_readvariableop_resource*&
_output_shapes
:@@*
dtype02:
8Decoder/conv_transpose_1/conv2d_transpose/ReadVariableOp�
)Decoder/conv_transpose_1/conv2d_transposeConv2DBackpropInput'Decoder/conv_transpose_1/stack:output:0@Decoder/conv_transpose_1/conv2d_transpose/ReadVariableOp:value:0&Decoder/Reshape_Layer/Reshape:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
2+
)Decoder/conv_transpose_1/conv2d_transpose�
/Decoder/conv_transpose_1/BiasAdd/ReadVariableOpReadVariableOp8decoder_conv_transpose_1_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype021
/Decoder/conv_transpose_1/BiasAdd/ReadVariableOp�
 Decoder/conv_transpose_1/BiasAddBiasAdd2Decoder/conv_transpose_1/conv2d_transpose:output:07Decoder/conv_transpose_1/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@2"
 Decoder/conv_transpose_1/BiasAdd�
Decoder/bn_1/ReadVariableOpReadVariableOp$decoder_bn_1_readvariableop_resource*
_output_shapes
:@*
dtype02
Decoder/bn_1/ReadVariableOp�
Decoder/bn_1/ReadVariableOp_1ReadVariableOp&decoder_bn_1_readvariableop_1_resource*
_output_shapes
:@*
dtype02
Decoder/bn_1/ReadVariableOp_1�
,Decoder/bn_1/FusedBatchNormV3/ReadVariableOpReadVariableOp5decoder_bn_1_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02.
,Decoder/bn_1/FusedBatchNormV3/ReadVariableOp�
.Decoder/bn_1/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp7decoder_bn_1_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype020
.Decoder/bn_1/FusedBatchNormV3/ReadVariableOp_1�
Decoder/bn_1/FusedBatchNormV3FusedBatchNormV3)Decoder/conv_transpose_1/BiasAdd:output:0#Decoder/bn_1/ReadVariableOp:value:0%Decoder/bn_1/ReadVariableOp_1:value:04Decoder/bn_1/FusedBatchNormV3/ReadVariableOp:value:06Decoder/bn_1/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:���������@:@:@:@:@:*
epsilon%o�:*
is_training( 2
Decoder/bn_1/FusedBatchNormV3�
Decoder/lrelu_1/LeakyRelu	LeakyRelu!Decoder/bn_1/FusedBatchNormV3:y:0*/
_output_shapes
:���������@*
alpha%���>2
Decoder/lrelu_1/LeakyRelu�
Decoder/conv_transpose_2/ShapeShape'Decoder/lrelu_1/LeakyRelu:activations:0*
T0*
_output_shapes
:2 
Decoder/conv_transpose_2/Shape�
,Decoder/conv_transpose_2/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2.
,Decoder/conv_transpose_2/strided_slice/stack�
.Decoder/conv_transpose_2/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_2/strided_slice/stack_1�
.Decoder/conv_transpose_2/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_2/strided_slice/stack_2�
&Decoder/conv_transpose_2/strided_sliceStridedSlice'Decoder/conv_transpose_2/Shape:output:05Decoder/conv_transpose_2/strided_slice/stack:output:07Decoder/conv_transpose_2/strided_slice/stack_1:output:07Decoder/conv_transpose_2/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2(
&Decoder/conv_transpose_2/strided_slice�
.Decoder/conv_transpose_2/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_2/strided_slice_1/stack�
0Decoder/conv_transpose_2/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_2/strided_slice_1/stack_1�
0Decoder/conv_transpose_2/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_2/strided_slice_1/stack_2�
(Decoder/conv_transpose_2/strided_slice_1StridedSlice'Decoder/conv_transpose_2/Shape:output:07Decoder/conv_transpose_2/strided_slice_1/stack:output:09Decoder/conv_transpose_2/strided_slice_1/stack_1:output:09Decoder/conv_transpose_2/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(Decoder/conv_transpose_2/strided_slice_1�
.Decoder/conv_transpose_2/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_2/strided_slice_2/stack�
0Decoder/conv_transpose_2/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_2/strided_slice_2/stack_1�
0Decoder/conv_transpose_2/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_2/strided_slice_2/stack_2�
(Decoder/conv_transpose_2/strided_slice_2StridedSlice'Decoder/conv_transpose_2/Shape:output:07Decoder/conv_transpose_2/strided_slice_2/stack:output:09Decoder/conv_transpose_2/strided_slice_2/stack_1:output:09Decoder/conv_transpose_2/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(Decoder/conv_transpose_2/strided_slice_2�
Decoder/conv_transpose_2/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2 
Decoder/conv_transpose_2/mul/y�
Decoder/conv_transpose_2/mulMul1Decoder/conv_transpose_2/strided_slice_1:output:0'Decoder/conv_transpose_2/mul/y:output:0*
T0*
_output_shapes
: 2
Decoder/conv_transpose_2/mul�
 Decoder/conv_transpose_2/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2"
 Decoder/conv_transpose_2/mul_1/y�
Decoder/conv_transpose_2/mul_1Mul1Decoder/conv_transpose_2/strided_slice_2:output:0)Decoder/conv_transpose_2/mul_1/y:output:0*
T0*
_output_shapes
: 2 
Decoder/conv_transpose_2/mul_1�
 Decoder/conv_transpose_2/stack/3Const*
_output_shapes
: *
dtype0*
value	B :@2"
 Decoder/conv_transpose_2/stack/3�
Decoder/conv_transpose_2/stackPack/Decoder/conv_transpose_2/strided_slice:output:0 Decoder/conv_transpose_2/mul:z:0"Decoder/conv_transpose_2/mul_1:z:0)Decoder/conv_transpose_2/stack/3:output:0*
N*
T0*
_output_shapes
:2 
Decoder/conv_transpose_2/stack�
.Decoder/conv_transpose_2/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 20
.Decoder/conv_transpose_2/strided_slice_3/stack�
0Decoder/conv_transpose_2/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_2/strided_slice_3/stack_1�
0Decoder/conv_transpose_2/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_2/strided_slice_3/stack_2�
(Decoder/conv_transpose_2/strided_slice_3StridedSlice'Decoder/conv_transpose_2/stack:output:07Decoder/conv_transpose_2/strided_slice_3/stack:output:09Decoder/conv_transpose_2/strided_slice_3/stack_1:output:09Decoder/conv_transpose_2/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(Decoder/conv_transpose_2/strided_slice_3�
8Decoder/conv_transpose_2/conv2d_transpose/ReadVariableOpReadVariableOpAdecoder_conv_transpose_2_conv2d_transpose_readvariableop_resource*&
_output_shapes
:@@*
dtype02:
8Decoder/conv_transpose_2/conv2d_transpose/ReadVariableOp�
)Decoder/conv_transpose_2/conv2d_transposeConv2DBackpropInput'Decoder/conv_transpose_2/stack:output:0@Decoder/conv_transpose_2/conv2d_transpose/ReadVariableOp:value:0'Decoder/lrelu_1/LeakyRelu:activations:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
2+
)Decoder/conv_transpose_2/conv2d_transpose�
/Decoder/conv_transpose_2/BiasAdd/ReadVariableOpReadVariableOp8decoder_conv_transpose_2_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype021
/Decoder/conv_transpose_2/BiasAdd/ReadVariableOp�
 Decoder/conv_transpose_2/BiasAddBiasAdd2Decoder/conv_transpose_2/conv2d_transpose:output:07Decoder/conv_transpose_2/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@2"
 Decoder/conv_transpose_2/BiasAdd�
Decoder/bn_2/ReadVariableOpReadVariableOp$decoder_bn_2_readvariableop_resource*
_output_shapes
:@*
dtype02
Decoder/bn_2/ReadVariableOp�
Decoder/bn_2/ReadVariableOp_1ReadVariableOp&decoder_bn_2_readvariableop_1_resource*
_output_shapes
:@*
dtype02
Decoder/bn_2/ReadVariableOp_1�
,Decoder/bn_2/FusedBatchNormV3/ReadVariableOpReadVariableOp5decoder_bn_2_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02.
,Decoder/bn_2/FusedBatchNormV3/ReadVariableOp�
.Decoder/bn_2/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp7decoder_bn_2_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype020
.Decoder/bn_2/FusedBatchNormV3/ReadVariableOp_1�
Decoder/bn_2/FusedBatchNormV3FusedBatchNormV3)Decoder/conv_transpose_2/BiasAdd:output:0#Decoder/bn_2/ReadVariableOp:value:0%Decoder/bn_2/ReadVariableOp_1:value:04Decoder/bn_2/FusedBatchNormV3/ReadVariableOp:value:06Decoder/bn_2/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:���������@:@:@:@:@:*
epsilon%o�:*
is_training( 2
Decoder/bn_2/FusedBatchNormV3�
Decoder/lrelu_2/LeakyRelu	LeakyRelu!Decoder/bn_2/FusedBatchNormV3:y:0*/
_output_shapes
:���������@*
alpha%���>2
Decoder/lrelu_2/LeakyRelu�
Decoder/conv_transpose_3/ShapeShape'Decoder/lrelu_2/LeakyRelu:activations:0*
T0*
_output_shapes
:2 
Decoder/conv_transpose_3/Shape�
,Decoder/conv_transpose_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2.
,Decoder/conv_transpose_3/strided_slice/stack�
.Decoder/conv_transpose_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_3/strided_slice/stack_1�
.Decoder/conv_transpose_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_3/strided_slice/stack_2�
&Decoder/conv_transpose_3/strided_sliceStridedSlice'Decoder/conv_transpose_3/Shape:output:05Decoder/conv_transpose_3/strided_slice/stack:output:07Decoder/conv_transpose_3/strided_slice/stack_1:output:07Decoder/conv_transpose_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2(
&Decoder/conv_transpose_3/strided_slice�
.Decoder/conv_transpose_3/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_3/strided_slice_1/stack�
0Decoder/conv_transpose_3/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_3/strided_slice_1/stack_1�
0Decoder/conv_transpose_3/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_3/strided_slice_1/stack_2�
(Decoder/conv_transpose_3/strided_slice_1StridedSlice'Decoder/conv_transpose_3/Shape:output:07Decoder/conv_transpose_3/strided_slice_1/stack:output:09Decoder/conv_transpose_3/strided_slice_1/stack_1:output:09Decoder/conv_transpose_3/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(Decoder/conv_transpose_3/strided_slice_1�
.Decoder/conv_transpose_3/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_3/strided_slice_2/stack�
0Decoder/conv_transpose_3/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_3/strided_slice_2/stack_1�
0Decoder/conv_transpose_3/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_3/strided_slice_2/stack_2�
(Decoder/conv_transpose_3/strided_slice_2StridedSlice'Decoder/conv_transpose_3/Shape:output:07Decoder/conv_transpose_3/strided_slice_2/stack:output:09Decoder/conv_transpose_3/strided_slice_2/stack_1:output:09Decoder/conv_transpose_3/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(Decoder/conv_transpose_3/strided_slice_2�
Decoder/conv_transpose_3/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2 
Decoder/conv_transpose_3/mul/y�
Decoder/conv_transpose_3/mulMul1Decoder/conv_transpose_3/strided_slice_1:output:0'Decoder/conv_transpose_3/mul/y:output:0*
T0*
_output_shapes
: 2
Decoder/conv_transpose_3/mul�
 Decoder/conv_transpose_3/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2"
 Decoder/conv_transpose_3/mul_1/y�
Decoder/conv_transpose_3/mul_1Mul1Decoder/conv_transpose_3/strided_slice_2:output:0)Decoder/conv_transpose_3/mul_1/y:output:0*
T0*
_output_shapes
: 2 
Decoder/conv_transpose_3/mul_1�
 Decoder/conv_transpose_3/stack/3Const*
_output_shapes
: *
dtype0*
value	B :@2"
 Decoder/conv_transpose_3/stack/3�
Decoder/conv_transpose_3/stackPack/Decoder/conv_transpose_3/strided_slice:output:0 Decoder/conv_transpose_3/mul:z:0"Decoder/conv_transpose_3/mul_1:z:0)Decoder/conv_transpose_3/stack/3:output:0*
N*
T0*
_output_shapes
:2 
Decoder/conv_transpose_3/stack�
.Decoder/conv_transpose_3/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 20
.Decoder/conv_transpose_3/strided_slice_3/stack�
0Decoder/conv_transpose_3/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_3/strided_slice_3/stack_1�
0Decoder/conv_transpose_3/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_3/strided_slice_3/stack_2�
(Decoder/conv_transpose_3/strided_slice_3StridedSlice'Decoder/conv_transpose_3/stack:output:07Decoder/conv_transpose_3/strided_slice_3/stack:output:09Decoder/conv_transpose_3/strided_slice_3/stack_1:output:09Decoder/conv_transpose_3/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(Decoder/conv_transpose_3/strided_slice_3�
8Decoder/conv_transpose_3/conv2d_transpose/ReadVariableOpReadVariableOpAdecoder_conv_transpose_3_conv2d_transpose_readvariableop_resource*&
_output_shapes
:@@*
dtype02:
8Decoder/conv_transpose_3/conv2d_transpose/ReadVariableOp�
)Decoder/conv_transpose_3/conv2d_transposeConv2DBackpropInput'Decoder/conv_transpose_3/stack:output:0@Decoder/conv_transpose_3/conv2d_transpose/ReadVariableOp:value:0'Decoder/lrelu_2/LeakyRelu:activations:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
2+
)Decoder/conv_transpose_3/conv2d_transpose�
/Decoder/conv_transpose_3/BiasAdd/ReadVariableOpReadVariableOp8decoder_conv_transpose_3_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype021
/Decoder/conv_transpose_3/BiasAdd/ReadVariableOp�
 Decoder/conv_transpose_3/BiasAddBiasAdd2Decoder/conv_transpose_3/conv2d_transpose:output:07Decoder/conv_transpose_3/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@2"
 Decoder/conv_transpose_3/BiasAdd�
Decoder/bn_3/ReadVariableOpReadVariableOp$decoder_bn_3_readvariableop_resource*
_output_shapes
:@*
dtype02
Decoder/bn_3/ReadVariableOp�
Decoder/bn_3/ReadVariableOp_1ReadVariableOp&decoder_bn_3_readvariableop_1_resource*
_output_shapes
:@*
dtype02
Decoder/bn_3/ReadVariableOp_1�
,Decoder/bn_3/FusedBatchNormV3/ReadVariableOpReadVariableOp5decoder_bn_3_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02.
,Decoder/bn_3/FusedBatchNormV3/ReadVariableOp�
.Decoder/bn_3/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp7decoder_bn_3_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype020
.Decoder/bn_3/FusedBatchNormV3/ReadVariableOp_1�
Decoder/bn_3/FusedBatchNormV3FusedBatchNormV3)Decoder/conv_transpose_3/BiasAdd:output:0#Decoder/bn_3/ReadVariableOp:value:0%Decoder/bn_3/ReadVariableOp_1:value:04Decoder/bn_3/FusedBatchNormV3/ReadVariableOp:value:06Decoder/bn_3/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:���������@:@:@:@:@:*
epsilon%o�:*
is_training( 2
Decoder/bn_3/FusedBatchNormV3�
Decoder/lrelu_3/LeakyRelu	LeakyRelu!Decoder/bn_3/FusedBatchNormV3:y:0*/
_output_shapes
:���������@*
alpha%���>2
Decoder/lrelu_3/LeakyRelu�
Decoder/conv_transpose_4/ShapeShape'Decoder/lrelu_3/LeakyRelu:activations:0*
T0*
_output_shapes
:2 
Decoder/conv_transpose_4/Shape�
,Decoder/conv_transpose_4/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2.
,Decoder/conv_transpose_4/strided_slice/stack�
.Decoder/conv_transpose_4/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_4/strided_slice/stack_1�
.Decoder/conv_transpose_4/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_4/strided_slice/stack_2�
&Decoder/conv_transpose_4/strided_sliceStridedSlice'Decoder/conv_transpose_4/Shape:output:05Decoder/conv_transpose_4/strided_slice/stack:output:07Decoder/conv_transpose_4/strided_slice/stack_1:output:07Decoder/conv_transpose_4/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2(
&Decoder/conv_transpose_4/strided_slice�
.Decoder/conv_transpose_4/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_4/strided_slice_1/stack�
0Decoder/conv_transpose_4/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_4/strided_slice_1/stack_1�
0Decoder/conv_transpose_4/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_4/strided_slice_1/stack_2�
(Decoder/conv_transpose_4/strided_slice_1StridedSlice'Decoder/conv_transpose_4/Shape:output:07Decoder/conv_transpose_4/strided_slice_1/stack:output:09Decoder/conv_transpose_4/strided_slice_1/stack_1:output:09Decoder/conv_transpose_4/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(Decoder/conv_transpose_4/strided_slice_1�
.Decoder/conv_transpose_4/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:20
.Decoder/conv_transpose_4/strided_slice_2/stack�
0Decoder/conv_transpose_4/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_4/strided_slice_2/stack_1�
0Decoder/conv_transpose_4/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_4/strided_slice_2/stack_2�
(Decoder/conv_transpose_4/strided_slice_2StridedSlice'Decoder/conv_transpose_4/Shape:output:07Decoder/conv_transpose_4/strided_slice_2/stack:output:09Decoder/conv_transpose_4/strided_slice_2/stack_1:output:09Decoder/conv_transpose_4/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(Decoder/conv_transpose_4/strided_slice_2�
Decoder/conv_transpose_4/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2 
Decoder/conv_transpose_4/mul/y�
Decoder/conv_transpose_4/mulMul1Decoder/conv_transpose_4/strided_slice_1:output:0'Decoder/conv_transpose_4/mul/y:output:0*
T0*
_output_shapes
: 2
Decoder/conv_transpose_4/mul�
 Decoder/conv_transpose_4/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2"
 Decoder/conv_transpose_4/mul_1/y�
Decoder/conv_transpose_4/mul_1Mul1Decoder/conv_transpose_4/strided_slice_2:output:0)Decoder/conv_transpose_4/mul_1/y:output:0*
T0*
_output_shapes
: 2 
Decoder/conv_transpose_4/mul_1�
 Decoder/conv_transpose_4/stack/3Const*
_output_shapes
: *
dtype0*
value	B :2"
 Decoder/conv_transpose_4/stack/3�
Decoder/conv_transpose_4/stackPack/Decoder/conv_transpose_4/strided_slice:output:0 Decoder/conv_transpose_4/mul:z:0"Decoder/conv_transpose_4/mul_1:z:0)Decoder/conv_transpose_4/stack/3:output:0*
N*
T0*
_output_shapes
:2 
Decoder/conv_transpose_4/stack�
.Decoder/conv_transpose_4/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 20
.Decoder/conv_transpose_4/strided_slice_3/stack�
0Decoder/conv_transpose_4/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_4/strided_slice_3/stack_1�
0Decoder/conv_transpose_4/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0Decoder/conv_transpose_4/strided_slice_3/stack_2�
(Decoder/conv_transpose_4/strided_slice_3StridedSlice'Decoder/conv_transpose_4/stack:output:07Decoder/conv_transpose_4/strided_slice_3/stack:output:09Decoder/conv_transpose_4/strided_slice_3/stack_1:output:09Decoder/conv_transpose_4/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(Decoder/conv_transpose_4/strided_slice_3�
8Decoder/conv_transpose_4/conv2d_transpose/ReadVariableOpReadVariableOpAdecoder_conv_transpose_4_conv2d_transpose_readvariableop_resource*&
_output_shapes
:@*
dtype02:
8Decoder/conv_transpose_4/conv2d_transpose/ReadVariableOp�
)Decoder/conv_transpose_4/conv2d_transposeConv2DBackpropInput'Decoder/conv_transpose_4/stack:output:0@Decoder/conv_transpose_4/conv2d_transpose/ReadVariableOp:value:0'Decoder/lrelu_3/LeakyRelu:activations:0*
T0*/
_output_shapes
:���������*
paddingSAME*
strides
2+
)Decoder/conv_transpose_4/conv2d_transpose�
/Decoder/conv_transpose_4/BiasAdd/ReadVariableOpReadVariableOp8decoder_conv_transpose_4_biasadd_readvariableop_resource*
_output_shapes
:*
dtype021
/Decoder/conv_transpose_4/BiasAdd/ReadVariableOp�
 Decoder/conv_transpose_4/BiasAddBiasAdd2Decoder/conv_transpose_4/conv2d_transpose:output:07Decoder/conv_transpose_4/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2"
 Decoder/conv_transpose_4/BiasAdd�
!Decoder/conv_transpose_4/SoftplusSoftplus)Decoder/conv_transpose_4/BiasAdd:output:0*
T0*/
_output_shapes
:���������2#
!Decoder/conv_transpose_4/Softplus�
IdentityIdentity/Decoder/conv_transpose_4/Softplus:activations:0*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*~
_input_shapesm
k:���������:::::::::::::::::::::::V R
'
_output_shapes
:���������
'
_user_specified_namedecoder_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
?__inference_bn_2_layer_call_and_return_conditional_losses_44533

inputs
readvariableop_resource
readvariableop_1_resource,
(fusedbatchnormv3_readvariableop_resource.
*fusedbatchnormv3_readvariableop_1_resource
identity�t
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype02
ReadVariableOpz
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype02
ReadVariableOp_1�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02!
FusedBatchNormV3/ReadVariableOp�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02#
!FusedBatchNormV3/ReadVariableOp_1�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:*
is_training( 2
FusedBatchNormV3�
IdentityIdentityFusedBatchNormV3:y:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@:::::i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_dense_1_layer_call_and_return_conditional_losses_45853

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAdde
IdentityIdentityBiasAdd:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������:::O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
?__inference_bn_3_layer_call_and_return_conditional_losses_46136

inputs
readvariableop_resource
readvariableop_1_resource,
(fusedbatchnormv3_readvariableop_resource.
*fusedbatchnormv3_readvariableop_1_resource
identity�t
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype02
ReadVariableOpz
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype02
ReadVariableOp_1�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02!
FusedBatchNormV3/ReadVariableOp�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02#
!FusedBatchNormV3/ReadVariableOp_1�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:*
is_training( 2
FusedBatchNormV3�
IdentityIdentityFusedBatchNormV3:y:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@:::::i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
0__inference_conv_transpose_3_layer_call_fn_44588

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_3_layer_call_and_return_conditional_losses_445782
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+���������������������������@::22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
^
B__inference_lrelu_3_layer_call_and_return_conditional_losses_46167

inputs
identity~
	LeakyRelu	LeakyReluinputs*A
_output_shapes/
-:+���������������������������@*
alpha%���>2
	LeakyRelu�
IdentityIdentityLeakyRelu:activations:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*@
_input_shapes/
-:+���������������������������@:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�
�
$__inference_bn_1_layer_call_fn_45968

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*&
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_1_layer_call_and_return_conditional_losses_443632
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@::::22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
��
�
B__inference_Decoder_layer_call_and_return_conditional_losses_45563

inputs*
&dense_1_matmul_readvariableop_resource+
'dense_1_biasadd_readvariableop_resource=
9conv_transpose_1_conv2d_transpose_readvariableop_resource4
0conv_transpose_1_biasadd_readvariableop_resource 
bn_1_readvariableop_resource"
bn_1_readvariableop_1_resource1
-bn_1_fusedbatchnormv3_readvariableop_resource3
/bn_1_fusedbatchnormv3_readvariableop_1_resource=
9conv_transpose_2_conv2d_transpose_readvariableop_resource4
0conv_transpose_2_biasadd_readvariableop_resource 
bn_2_readvariableop_resource"
bn_2_readvariableop_1_resource1
-bn_2_fusedbatchnormv3_readvariableop_resource3
/bn_2_fusedbatchnormv3_readvariableop_1_resource=
9conv_transpose_3_conv2d_transpose_readvariableop_resource4
0conv_transpose_3_biasadd_readvariableop_resource 
bn_3_readvariableop_resource"
bn_3_readvariableop_1_resource1
-bn_3_fusedbatchnormv3_readvariableop_resource3
/bn_3_fusedbatchnormv3_readvariableop_1_resource=
9conv_transpose_4_conv2d_transpose_readvariableop_resource4
0conv_transpose_4_biasadd_readvariableop_resource
identity��(bn_1/AssignMovingAvg/AssignSubVariableOp�*bn_1/AssignMovingAvg_1/AssignSubVariableOp�(bn_2/AssignMovingAvg/AssignSubVariableOp�*bn_2/AssignMovingAvg_1/AssignSubVariableOp�(bn_3/AssignMovingAvg/AssignSubVariableOp�*bn_3/AssignMovingAvg_1/AssignSubVariableOp�
dense_1/MatMul/ReadVariableOpReadVariableOp&dense_1_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
dense_1/MatMul/ReadVariableOp�
dense_1/MatMulMatMulinputs%dense_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_1/MatMul�
dense_1/BiasAdd/ReadVariableOpReadVariableOp'dense_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02 
dense_1/BiasAdd/ReadVariableOp�
dense_1/BiasAddBiasAdddense_1/MatMul:product:0&dense_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_1/BiasAddr
Reshape_Layer/ShapeShapedense_1/BiasAdd:output:0*
T0*
_output_shapes
:2
Reshape_Layer/Shape�
!Reshape_Layer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2#
!Reshape_Layer/strided_slice/stack�
#Reshape_Layer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2%
#Reshape_Layer/strided_slice/stack_1�
#Reshape_Layer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2%
#Reshape_Layer/strided_slice/stack_2�
Reshape_Layer/strided_sliceStridedSliceReshape_Layer/Shape:output:0*Reshape_Layer/strided_slice/stack:output:0,Reshape_Layer/strided_slice/stack_1:output:0,Reshape_Layer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
Reshape_Layer/strided_slice�
Reshape_Layer/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_Layer/Reshape/shape/1�
Reshape_Layer/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_Layer/Reshape/shape/2�
Reshape_Layer/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :@2
Reshape_Layer/Reshape/shape/3�
Reshape_Layer/Reshape/shapePack$Reshape_Layer/strided_slice:output:0&Reshape_Layer/Reshape/shape/1:output:0&Reshape_Layer/Reshape/shape/2:output:0&Reshape_Layer/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape_Layer/Reshape/shape�
Reshape_Layer/ReshapeReshapedense_1/BiasAdd:output:0$Reshape_Layer/Reshape/shape:output:0*
T0*/
_output_shapes
:���������@2
Reshape_Layer/Reshape~
conv_transpose_1/ShapeShapeReshape_Layer/Reshape:output:0*
T0*
_output_shapes
:2
conv_transpose_1/Shape�
$conv_transpose_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2&
$conv_transpose_1/strided_slice/stack�
&conv_transpose_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_1/strided_slice/stack_1�
&conv_transpose_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_1/strided_slice/stack_2�
conv_transpose_1/strided_sliceStridedSliceconv_transpose_1/Shape:output:0-conv_transpose_1/strided_slice/stack:output:0/conv_transpose_1/strided_slice/stack_1:output:0/conv_transpose_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2 
conv_transpose_1/strided_slice�
&conv_transpose_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_1/strided_slice_1/stack�
(conv_transpose_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_1/strided_slice_1/stack_1�
(conv_transpose_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_1/strided_slice_1/stack_2�
 conv_transpose_1/strided_slice_1StridedSliceconv_transpose_1/Shape:output:0/conv_transpose_1/strided_slice_1/stack:output:01conv_transpose_1/strided_slice_1/stack_1:output:01conv_transpose_1/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_1/strided_slice_1�
&conv_transpose_1/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_1/strided_slice_2/stack�
(conv_transpose_1/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_1/strided_slice_2/stack_1�
(conv_transpose_1/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_1/strided_slice_2/stack_2�
 conv_transpose_1/strided_slice_2StridedSliceconv_transpose_1/Shape:output:0/conv_transpose_1/strided_slice_2/stack:output:01conv_transpose_1/strided_slice_2/stack_1:output:01conv_transpose_1/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_1/strided_slice_2r
conv_transpose_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_1/mul/y�
conv_transpose_1/mulMul)conv_transpose_1/strided_slice_1:output:0conv_transpose_1/mul/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_1/mulv
conv_transpose_1/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_1/mul_1/y�
conv_transpose_1/mul_1Mul)conv_transpose_1/strided_slice_2:output:0!conv_transpose_1/mul_1/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_1/mul_1v
conv_transpose_1/stack/3Const*
_output_shapes
: *
dtype0*
value	B :@2
conv_transpose_1/stack/3�
conv_transpose_1/stackPack'conv_transpose_1/strided_slice:output:0conv_transpose_1/mul:z:0conv_transpose_1/mul_1:z:0!conv_transpose_1/stack/3:output:0*
N*
T0*
_output_shapes
:2
conv_transpose_1/stack�
&conv_transpose_1/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 2(
&conv_transpose_1/strided_slice_3/stack�
(conv_transpose_1/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_1/strided_slice_3/stack_1�
(conv_transpose_1/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_1/strided_slice_3/stack_2�
 conv_transpose_1/strided_slice_3StridedSliceconv_transpose_1/stack:output:0/conv_transpose_1/strided_slice_3/stack:output:01conv_transpose_1/strided_slice_3/stack_1:output:01conv_transpose_1/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_1/strided_slice_3�
0conv_transpose_1/conv2d_transpose/ReadVariableOpReadVariableOp9conv_transpose_1_conv2d_transpose_readvariableop_resource*&
_output_shapes
:@@*
dtype022
0conv_transpose_1/conv2d_transpose/ReadVariableOp�
!conv_transpose_1/conv2d_transposeConv2DBackpropInputconv_transpose_1/stack:output:08conv_transpose_1/conv2d_transpose/ReadVariableOp:value:0Reshape_Layer/Reshape:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
2#
!conv_transpose_1/conv2d_transpose�
'conv_transpose_1/BiasAdd/ReadVariableOpReadVariableOp0conv_transpose_1_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02)
'conv_transpose_1/BiasAdd/ReadVariableOp�
conv_transpose_1/BiasAddBiasAdd*conv_transpose_1/conv2d_transpose:output:0/conv_transpose_1/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@2
conv_transpose_1/BiasAdd�
bn_1/ReadVariableOpReadVariableOpbn_1_readvariableop_resource*
_output_shapes
:@*
dtype02
bn_1/ReadVariableOp�
bn_1/ReadVariableOp_1ReadVariableOpbn_1_readvariableop_1_resource*
_output_shapes
:@*
dtype02
bn_1/ReadVariableOp_1�
$bn_1/FusedBatchNormV3/ReadVariableOpReadVariableOp-bn_1_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02&
$bn_1/FusedBatchNormV3/ReadVariableOp�
&bn_1/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp/bn_1_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02(
&bn_1/FusedBatchNormV3/ReadVariableOp_1�
bn_1/FusedBatchNormV3FusedBatchNormV3!conv_transpose_1/BiasAdd:output:0bn_1/ReadVariableOp:value:0bn_1/ReadVariableOp_1:value:0,bn_1/FusedBatchNormV3/ReadVariableOp:value:0.bn_1/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:���������@:@:@:@:@:*
epsilon%o�:2
bn_1/FusedBatchNormV3]

bn_1/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *�p}?2

bn_1/Const�
bn_1/AssignMovingAvg/sub/xConst*@
_class6
42loc:@bn_1/FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
bn_1/AssignMovingAvg/sub/x�
bn_1/AssignMovingAvg/subSub#bn_1/AssignMovingAvg/sub/x:output:0bn_1/Const:output:0*
T0*@
_class6
42loc:@bn_1/FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: 2
bn_1/AssignMovingAvg/sub�
#bn_1/AssignMovingAvg/ReadVariableOpReadVariableOp-bn_1_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02%
#bn_1/AssignMovingAvg/ReadVariableOp�
bn_1/AssignMovingAvg/sub_1Sub+bn_1/AssignMovingAvg/ReadVariableOp:value:0"bn_1/FusedBatchNormV3:batch_mean:0*
T0*@
_class6
42loc:@bn_1/FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
bn_1/AssignMovingAvg/sub_1�
bn_1/AssignMovingAvg/mulMulbn_1/AssignMovingAvg/sub_1:z:0bn_1/AssignMovingAvg/sub:z:0*
T0*@
_class6
42loc:@bn_1/FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
bn_1/AssignMovingAvg/mul�
(bn_1/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp-bn_1_fusedbatchnormv3_readvariableop_resourcebn_1/AssignMovingAvg/mul:z:0$^bn_1/AssignMovingAvg/ReadVariableOp%^bn_1/FusedBatchNormV3/ReadVariableOp*@
_class6
42loc:@bn_1/FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
 *
dtype02*
(bn_1/AssignMovingAvg/AssignSubVariableOp�
bn_1/AssignMovingAvg_1/sub/xConst*B
_class8
64loc:@bn_1/FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
bn_1/AssignMovingAvg_1/sub/x�
bn_1/AssignMovingAvg_1/subSub%bn_1/AssignMovingAvg_1/sub/x:output:0bn_1/Const:output:0*
T0*B
_class8
64loc:@bn_1/FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: 2
bn_1/AssignMovingAvg_1/sub�
%bn_1/AssignMovingAvg_1/ReadVariableOpReadVariableOp/bn_1_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02'
%bn_1/AssignMovingAvg_1/ReadVariableOp�
bn_1/AssignMovingAvg_1/sub_1Sub-bn_1/AssignMovingAvg_1/ReadVariableOp:value:0&bn_1/FusedBatchNormV3:batch_variance:0*
T0*B
_class8
64loc:@bn_1/FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
bn_1/AssignMovingAvg_1/sub_1�
bn_1/AssignMovingAvg_1/mulMul bn_1/AssignMovingAvg_1/sub_1:z:0bn_1/AssignMovingAvg_1/sub:z:0*
T0*B
_class8
64loc:@bn_1/FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
bn_1/AssignMovingAvg_1/mul�
*bn_1/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp/bn_1_fusedbatchnormv3_readvariableop_1_resourcebn_1/AssignMovingAvg_1/mul:z:0&^bn_1/AssignMovingAvg_1/ReadVariableOp'^bn_1/FusedBatchNormV3/ReadVariableOp_1*B
_class8
64loc:@bn_1/FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
 *
dtype02,
*bn_1/AssignMovingAvg_1/AssignSubVariableOp�
lrelu_1/LeakyRelu	LeakyRelubn_1/FusedBatchNormV3:y:0*/
_output_shapes
:���������@*
alpha%���>2
lrelu_1/LeakyRelu
conv_transpose_2/ShapeShapelrelu_1/LeakyRelu:activations:0*
T0*
_output_shapes
:2
conv_transpose_2/Shape�
$conv_transpose_2/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2&
$conv_transpose_2/strided_slice/stack�
&conv_transpose_2/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_2/strided_slice/stack_1�
&conv_transpose_2/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_2/strided_slice/stack_2�
conv_transpose_2/strided_sliceStridedSliceconv_transpose_2/Shape:output:0-conv_transpose_2/strided_slice/stack:output:0/conv_transpose_2/strided_slice/stack_1:output:0/conv_transpose_2/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2 
conv_transpose_2/strided_slice�
&conv_transpose_2/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_2/strided_slice_1/stack�
(conv_transpose_2/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_2/strided_slice_1/stack_1�
(conv_transpose_2/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_2/strided_slice_1/stack_2�
 conv_transpose_2/strided_slice_1StridedSliceconv_transpose_2/Shape:output:0/conv_transpose_2/strided_slice_1/stack:output:01conv_transpose_2/strided_slice_1/stack_1:output:01conv_transpose_2/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_2/strided_slice_1�
&conv_transpose_2/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_2/strided_slice_2/stack�
(conv_transpose_2/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_2/strided_slice_2/stack_1�
(conv_transpose_2/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_2/strided_slice_2/stack_2�
 conv_transpose_2/strided_slice_2StridedSliceconv_transpose_2/Shape:output:0/conv_transpose_2/strided_slice_2/stack:output:01conv_transpose_2/strided_slice_2/stack_1:output:01conv_transpose_2/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_2/strided_slice_2r
conv_transpose_2/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_2/mul/y�
conv_transpose_2/mulMul)conv_transpose_2/strided_slice_1:output:0conv_transpose_2/mul/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_2/mulv
conv_transpose_2/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_2/mul_1/y�
conv_transpose_2/mul_1Mul)conv_transpose_2/strided_slice_2:output:0!conv_transpose_2/mul_1/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_2/mul_1v
conv_transpose_2/stack/3Const*
_output_shapes
: *
dtype0*
value	B :@2
conv_transpose_2/stack/3�
conv_transpose_2/stackPack'conv_transpose_2/strided_slice:output:0conv_transpose_2/mul:z:0conv_transpose_2/mul_1:z:0!conv_transpose_2/stack/3:output:0*
N*
T0*
_output_shapes
:2
conv_transpose_2/stack�
&conv_transpose_2/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 2(
&conv_transpose_2/strided_slice_3/stack�
(conv_transpose_2/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_2/strided_slice_3/stack_1�
(conv_transpose_2/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_2/strided_slice_3/stack_2�
 conv_transpose_2/strided_slice_3StridedSliceconv_transpose_2/stack:output:0/conv_transpose_2/strided_slice_3/stack:output:01conv_transpose_2/strided_slice_3/stack_1:output:01conv_transpose_2/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_2/strided_slice_3�
0conv_transpose_2/conv2d_transpose/ReadVariableOpReadVariableOp9conv_transpose_2_conv2d_transpose_readvariableop_resource*&
_output_shapes
:@@*
dtype022
0conv_transpose_2/conv2d_transpose/ReadVariableOp�
!conv_transpose_2/conv2d_transposeConv2DBackpropInputconv_transpose_2/stack:output:08conv_transpose_2/conv2d_transpose/ReadVariableOp:value:0lrelu_1/LeakyRelu:activations:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
2#
!conv_transpose_2/conv2d_transpose�
'conv_transpose_2/BiasAdd/ReadVariableOpReadVariableOp0conv_transpose_2_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02)
'conv_transpose_2/BiasAdd/ReadVariableOp�
conv_transpose_2/BiasAddBiasAdd*conv_transpose_2/conv2d_transpose:output:0/conv_transpose_2/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@2
conv_transpose_2/BiasAdd�
bn_2/ReadVariableOpReadVariableOpbn_2_readvariableop_resource*
_output_shapes
:@*
dtype02
bn_2/ReadVariableOp�
bn_2/ReadVariableOp_1ReadVariableOpbn_2_readvariableop_1_resource*
_output_shapes
:@*
dtype02
bn_2/ReadVariableOp_1�
$bn_2/FusedBatchNormV3/ReadVariableOpReadVariableOp-bn_2_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02&
$bn_2/FusedBatchNormV3/ReadVariableOp�
&bn_2/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp/bn_2_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02(
&bn_2/FusedBatchNormV3/ReadVariableOp_1�
bn_2/FusedBatchNormV3FusedBatchNormV3!conv_transpose_2/BiasAdd:output:0bn_2/ReadVariableOp:value:0bn_2/ReadVariableOp_1:value:0,bn_2/FusedBatchNormV3/ReadVariableOp:value:0.bn_2/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:���������@:@:@:@:@:*
epsilon%o�:2
bn_2/FusedBatchNormV3]

bn_2/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *�p}?2

bn_2/Const�
bn_2/AssignMovingAvg/sub/xConst*@
_class6
42loc:@bn_2/FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
bn_2/AssignMovingAvg/sub/x�
bn_2/AssignMovingAvg/subSub#bn_2/AssignMovingAvg/sub/x:output:0bn_2/Const:output:0*
T0*@
_class6
42loc:@bn_2/FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: 2
bn_2/AssignMovingAvg/sub�
#bn_2/AssignMovingAvg/ReadVariableOpReadVariableOp-bn_2_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02%
#bn_2/AssignMovingAvg/ReadVariableOp�
bn_2/AssignMovingAvg/sub_1Sub+bn_2/AssignMovingAvg/ReadVariableOp:value:0"bn_2/FusedBatchNormV3:batch_mean:0*
T0*@
_class6
42loc:@bn_2/FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
bn_2/AssignMovingAvg/sub_1�
bn_2/AssignMovingAvg/mulMulbn_2/AssignMovingAvg/sub_1:z:0bn_2/AssignMovingAvg/sub:z:0*
T0*@
_class6
42loc:@bn_2/FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
bn_2/AssignMovingAvg/mul�
(bn_2/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp-bn_2_fusedbatchnormv3_readvariableop_resourcebn_2/AssignMovingAvg/mul:z:0$^bn_2/AssignMovingAvg/ReadVariableOp%^bn_2/FusedBatchNormV3/ReadVariableOp*@
_class6
42loc:@bn_2/FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
 *
dtype02*
(bn_2/AssignMovingAvg/AssignSubVariableOp�
bn_2/AssignMovingAvg_1/sub/xConst*B
_class8
64loc:@bn_2/FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
bn_2/AssignMovingAvg_1/sub/x�
bn_2/AssignMovingAvg_1/subSub%bn_2/AssignMovingAvg_1/sub/x:output:0bn_2/Const:output:0*
T0*B
_class8
64loc:@bn_2/FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: 2
bn_2/AssignMovingAvg_1/sub�
%bn_2/AssignMovingAvg_1/ReadVariableOpReadVariableOp/bn_2_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02'
%bn_2/AssignMovingAvg_1/ReadVariableOp�
bn_2/AssignMovingAvg_1/sub_1Sub-bn_2/AssignMovingAvg_1/ReadVariableOp:value:0&bn_2/FusedBatchNormV3:batch_variance:0*
T0*B
_class8
64loc:@bn_2/FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
bn_2/AssignMovingAvg_1/sub_1�
bn_2/AssignMovingAvg_1/mulMul bn_2/AssignMovingAvg_1/sub_1:z:0bn_2/AssignMovingAvg_1/sub:z:0*
T0*B
_class8
64loc:@bn_2/FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
bn_2/AssignMovingAvg_1/mul�
*bn_2/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp/bn_2_fusedbatchnormv3_readvariableop_1_resourcebn_2/AssignMovingAvg_1/mul:z:0&^bn_2/AssignMovingAvg_1/ReadVariableOp'^bn_2/FusedBatchNormV3/ReadVariableOp_1*B
_class8
64loc:@bn_2/FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
 *
dtype02,
*bn_2/AssignMovingAvg_1/AssignSubVariableOp�
lrelu_2/LeakyRelu	LeakyRelubn_2/FusedBatchNormV3:y:0*/
_output_shapes
:���������@*
alpha%���>2
lrelu_2/LeakyRelu
conv_transpose_3/ShapeShapelrelu_2/LeakyRelu:activations:0*
T0*
_output_shapes
:2
conv_transpose_3/Shape�
$conv_transpose_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2&
$conv_transpose_3/strided_slice/stack�
&conv_transpose_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_3/strided_slice/stack_1�
&conv_transpose_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_3/strided_slice/stack_2�
conv_transpose_3/strided_sliceStridedSliceconv_transpose_3/Shape:output:0-conv_transpose_3/strided_slice/stack:output:0/conv_transpose_3/strided_slice/stack_1:output:0/conv_transpose_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2 
conv_transpose_3/strided_slice�
&conv_transpose_3/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_3/strided_slice_1/stack�
(conv_transpose_3/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_3/strided_slice_1/stack_1�
(conv_transpose_3/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_3/strided_slice_1/stack_2�
 conv_transpose_3/strided_slice_1StridedSliceconv_transpose_3/Shape:output:0/conv_transpose_3/strided_slice_1/stack:output:01conv_transpose_3/strided_slice_1/stack_1:output:01conv_transpose_3/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_3/strided_slice_1�
&conv_transpose_3/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_3/strided_slice_2/stack�
(conv_transpose_3/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_3/strided_slice_2/stack_1�
(conv_transpose_3/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_3/strided_slice_2/stack_2�
 conv_transpose_3/strided_slice_2StridedSliceconv_transpose_3/Shape:output:0/conv_transpose_3/strided_slice_2/stack:output:01conv_transpose_3/strided_slice_2/stack_1:output:01conv_transpose_3/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_3/strided_slice_2r
conv_transpose_3/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_3/mul/y�
conv_transpose_3/mulMul)conv_transpose_3/strided_slice_1:output:0conv_transpose_3/mul/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_3/mulv
conv_transpose_3/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_3/mul_1/y�
conv_transpose_3/mul_1Mul)conv_transpose_3/strided_slice_2:output:0!conv_transpose_3/mul_1/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_3/mul_1v
conv_transpose_3/stack/3Const*
_output_shapes
: *
dtype0*
value	B :@2
conv_transpose_3/stack/3�
conv_transpose_3/stackPack'conv_transpose_3/strided_slice:output:0conv_transpose_3/mul:z:0conv_transpose_3/mul_1:z:0!conv_transpose_3/stack/3:output:0*
N*
T0*
_output_shapes
:2
conv_transpose_3/stack�
&conv_transpose_3/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 2(
&conv_transpose_3/strided_slice_3/stack�
(conv_transpose_3/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_3/strided_slice_3/stack_1�
(conv_transpose_3/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_3/strided_slice_3/stack_2�
 conv_transpose_3/strided_slice_3StridedSliceconv_transpose_3/stack:output:0/conv_transpose_3/strided_slice_3/stack:output:01conv_transpose_3/strided_slice_3/stack_1:output:01conv_transpose_3/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_3/strided_slice_3�
0conv_transpose_3/conv2d_transpose/ReadVariableOpReadVariableOp9conv_transpose_3_conv2d_transpose_readvariableop_resource*&
_output_shapes
:@@*
dtype022
0conv_transpose_3/conv2d_transpose/ReadVariableOp�
!conv_transpose_3/conv2d_transposeConv2DBackpropInputconv_transpose_3/stack:output:08conv_transpose_3/conv2d_transpose/ReadVariableOp:value:0lrelu_2/LeakyRelu:activations:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
2#
!conv_transpose_3/conv2d_transpose�
'conv_transpose_3/BiasAdd/ReadVariableOpReadVariableOp0conv_transpose_3_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02)
'conv_transpose_3/BiasAdd/ReadVariableOp�
conv_transpose_3/BiasAddBiasAdd*conv_transpose_3/conv2d_transpose:output:0/conv_transpose_3/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@2
conv_transpose_3/BiasAdd�
bn_3/ReadVariableOpReadVariableOpbn_3_readvariableop_resource*
_output_shapes
:@*
dtype02
bn_3/ReadVariableOp�
bn_3/ReadVariableOp_1ReadVariableOpbn_3_readvariableop_1_resource*
_output_shapes
:@*
dtype02
bn_3/ReadVariableOp_1�
$bn_3/FusedBatchNormV3/ReadVariableOpReadVariableOp-bn_3_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02&
$bn_3/FusedBatchNormV3/ReadVariableOp�
&bn_3/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp/bn_3_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02(
&bn_3/FusedBatchNormV3/ReadVariableOp_1�
bn_3/FusedBatchNormV3FusedBatchNormV3!conv_transpose_3/BiasAdd:output:0bn_3/ReadVariableOp:value:0bn_3/ReadVariableOp_1:value:0,bn_3/FusedBatchNormV3/ReadVariableOp:value:0.bn_3/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:���������@:@:@:@:@:*
epsilon%o�:2
bn_3/FusedBatchNormV3]

bn_3/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *�p}?2

bn_3/Const�
bn_3/AssignMovingAvg/sub/xConst*@
_class6
42loc:@bn_3/FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
bn_3/AssignMovingAvg/sub/x�
bn_3/AssignMovingAvg/subSub#bn_3/AssignMovingAvg/sub/x:output:0bn_3/Const:output:0*
T0*@
_class6
42loc:@bn_3/FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: 2
bn_3/AssignMovingAvg/sub�
#bn_3/AssignMovingAvg/ReadVariableOpReadVariableOp-bn_3_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02%
#bn_3/AssignMovingAvg/ReadVariableOp�
bn_3/AssignMovingAvg/sub_1Sub+bn_3/AssignMovingAvg/ReadVariableOp:value:0"bn_3/FusedBatchNormV3:batch_mean:0*
T0*@
_class6
42loc:@bn_3/FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
bn_3/AssignMovingAvg/sub_1�
bn_3/AssignMovingAvg/mulMulbn_3/AssignMovingAvg/sub_1:z:0bn_3/AssignMovingAvg/sub:z:0*
T0*@
_class6
42loc:@bn_3/FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
bn_3/AssignMovingAvg/mul�
(bn_3/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp-bn_3_fusedbatchnormv3_readvariableop_resourcebn_3/AssignMovingAvg/mul:z:0$^bn_3/AssignMovingAvg/ReadVariableOp%^bn_3/FusedBatchNormV3/ReadVariableOp*@
_class6
42loc:@bn_3/FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
 *
dtype02*
(bn_3/AssignMovingAvg/AssignSubVariableOp�
bn_3/AssignMovingAvg_1/sub/xConst*B
_class8
64loc:@bn_3/FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
bn_3/AssignMovingAvg_1/sub/x�
bn_3/AssignMovingAvg_1/subSub%bn_3/AssignMovingAvg_1/sub/x:output:0bn_3/Const:output:0*
T0*B
_class8
64loc:@bn_3/FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: 2
bn_3/AssignMovingAvg_1/sub�
%bn_3/AssignMovingAvg_1/ReadVariableOpReadVariableOp/bn_3_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02'
%bn_3/AssignMovingAvg_1/ReadVariableOp�
bn_3/AssignMovingAvg_1/sub_1Sub-bn_3/AssignMovingAvg_1/ReadVariableOp:value:0&bn_3/FusedBatchNormV3:batch_variance:0*
T0*B
_class8
64loc:@bn_3/FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
bn_3/AssignMovingAvg_1/sub_1�
bn_3/AssignMovingAvg_1/mulMul bn_3/AssignMovingAvg_1/sub_1:z:0bn_3/AssignMovingAvg_1/sub:z:0*
T0*B
_class8
64loc:@bn_3/FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
bn_3/AssignMovingAvg_1/mul�
*bn_3/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp/bn_3_fusedbatchnormv3_readvariableop_1_resourcebn_3/AssignMovingAvg_1/mul:z:0&^bn_3/AssignMovingAvg_1/ReadVariableOp'^bn_3/FusedBatchNormV3/ReadVariableOp_1*B
_class8
64loc:@bn_3/FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
 *
dtype02,
*bn_3/AssignMovingAvg_1/AssignSubVariableOp�
lrelu_3/LeakyRelu	LeakyRelubn_3/FusedBatchNormV3:y:0*/
_output_shapes
:���������@*
alpha%���>2
lrelu_3/LeakyRelu
conv_transpose_4/ShapeShapelrelu_3/LeakyRelu:activations:0*
T0*
_output_shapes
:2
conv_transpose_4/Shape�
$conv_transpose_4/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2&
$conv_transpose_4/strided_slice/stack�
&conv_transpose_4/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_4/strided_slice/stack_1�
&conv_transpose_4/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_4/strided_slice/stack_2�
conv_transpose_4/strided_sliceStridedSliceconv_transpose_4/Shape:output:0-conv_transpose_4/strided_slice/stack:output:0/conv_transpose_4/strided_slice/stack_1:output:0/conv_transpose_4/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2 
conv_transpose_4/strided_slice�
&conv_transpose_4/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_4/strided_slice_1/stack�
(conv_transpose_4/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_4/strided_slice_1/stack_1�
(conv_transpose_4/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_4/strided_slice_1/stack_2�
 conv_transpose_4/strided_slice_1StridedSliceconv_transpose_4/Shape:output:0/conv_transpose_4/strided_slice_1/stack:output:01conv_transpose_4/strided_slice_1/stack_1:output:01conv_transpose_4/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_4/strided_slice_1�
&conv_transpose_4/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_4/strided_slice_2/stack�
(conv_transpose_4/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_4/strided_slice_2/stack_1�
(conv_transpose_4/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_4/strided_slice_2/stack_2�
 conv_transpose_4/strided_slice_2StridedSliceconv_transpose_4/Shape:output:0/conv_transpose_4/strided_slice_2/stack:output:01conv_transpose_4/strided_slice_2/stack_1:output:01conv_transpose_4/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_4/strided_slice_2r
conv_transpose_4/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_4/mul/y�
conv_transpose_4/mulMul)conv_transpose_4/strided_slice_1:output:0conv_transpose_4/mul/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_4/mulv
conv_transpose_4/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_4/mul_1/y�
conv_transpose_4/mul_1Mul)conv_transpose_4/strided_slice_2:output:0!conv_transpose_4/mul_1/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_4/mul_1v
conv_transpose_4/stack/3Const*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_4/stack/3�
conv_transpose_4/stackPack'conv_transpose_4/strided_slice:output:0conv_transpose_4/mul:z:0conv_transpose_4/mul_1:z:0!conv_transpose_4/stack/3:output:0*
N*
T0*
_output_shapes
:2
conv_transpose_4/stack�
&conv_transpose_4/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 2(
&conv_transpose_4/strided_slice_3/stack�
(conv_transpose_4/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_4/strided_slice_3/stack_1�
(conv_transpose_4/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_4/strided_slice_3/stack_2�
 conv_transpose_4/strided_slice_3StridedSliceconv_transpose_4/stack:output:0/conv_transpose_4/strided_slice_3/stack:output:01conv_transpose_4/strided_slice_3/stack_1:output:01conv_transpose_4/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_4/strided_slice_3�
0conv_transpose_4/conv2d_transpose/ReadVariableOpReadVariableOp9conv_transpose_4_conv2d_transpose_readvariableop_resource*&
_output_shapes
:@*
dtype022
0conv_transpose_4/conv2d_transpose/ReadVariableOp�
!conv_transpose_4/conv2d_transposeConv2DBackpropInputconv_transpose_4/stack:output:08conv_transpose_4/conv2d_transpose/ReadVariableOp:value:0lrelu_3/LeakyRelu:activations:0*
T0*/
_output_shapes
:���������*
paddingSAME*
strides
2#
!conv_transpose_4/conv2d_transpose�
'conv_transpose_4/BiasAdd/ReadVariableOpReadVariableOp0conv_transpose_4_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02)
'conv_transpose_4/BiasAdd/ReadVariableOp�
conv_transpose_4/BiasAddBiasAdd*conv_transpose_4/conv2d_transpose:output:0/conv_transpose_4/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
conv_transpose_4/BiasAdd�
conv_transpose_4/SoftplusSoftplus!conv_transpose_4/BiasAdd:output:0*
T0*/
_output_shapes
:���������2
conv_transpose_4/Softplus�
IdentityIdentity'conv_transpose_4/Softplus:activations:0)^bn_1/AssignMovingAvg/AssignSubVariableOp+^bn_1/AssignMovingAvg_1/AssignSubVariableOp)^bn_2/AssignMovingAvg/AssignSubVariableOp+^bn_2/AssignMovingAvg_1/AssignSubVariableOp)^bn_3/AssignMovingAvg/AssignSubVariableOp+^bn_3/AssignMovingAvg_1/AssignSubVariableOp*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*~
_input_shapesm
k:���������::::::::::::::::::::::2T
(bn_1/AssignMovingAvg/AssignSubVariableOp(bn_1/AssignMovingAvg/AssignSubVariableOp2X
*bn_1/AssignMovingAvg_1/AssignSubVariableOp*bn_1/AssignMovingAvg_1/AssignSubVariableOp2T
(bn_2/AssignMovingAvg/AssignSubVariableOp(bn_2/AssignMovingAvg/AssignSubVariableOp2X
*bn_2/AssignMovingAvg_1/AssignSubVariableOp*bn_2/AssignMovingAvg_1/AssignSubVariableOp2T
(bn_3/AssignMovingAvg/AssignSubVariableOp(bn_3/AssignMovingAvg/AssignSubVariableOp2X
*bn_3/AssignMovingAvg_1/AssignSubVariableOp*bn_3/AssignMovingAvg_1/AssignSubVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
0__inference_conv_transpose_4_layer_call_fn_44759

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_4_layer_call_and_return_conditional_losses_447492
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+���������������������������@::22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
?__inference_bn_1_layer_call_and_return_conditional_losses_44363

inputs
readvariableop_resource
readvariableop_1_resource,
(fusedbatchnormv3_readvariableop_resource.
*fusedbatchnormv3_readvariableop_1_resource
identity�t
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype02
ReadVariableOpz
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype02
ReadVariableOp_1�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02!
FusedBatchNormV3/ReadVariableOp�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02#
!FusedBatchNormV3/ReadVariableOp_1�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:*
is_training( 2
FusedBatchNormV3�
IdentityIdentityFusedBatchNormV3:y:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@:::::i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
C
'__inference_lrelu_2_layer_call_fn_46075

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_lrelu_2_layer_call_and_return_conditional_losses_449092
PartitionedCall�
IdentityIdentityPartitionedCall:output:0*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*@
_input_shapes/
-:+���������������������������@:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�
I
-__inference_Reshape_Layer_layer_call_fn_45881

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*/
_output_shapes
:���������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*Q
fLRJ
H__inference_Reshape_Layer_layer_call_and_return_conditional_losses_448032
PartitionedCallt
IdentityIdentityPartitionedCall:output:0*
T0*/
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
��
�	
B__inference_Decoder_layer_call_and_return_conditional_losses_45745

inputs*
&dense_1_matmul_readvariableop_resource+
'dense_1_biasadd_readvariableop_resource=
9conv_transpose_1_conv2d_transpose_readvariableop_resource4
0conv_transpose_1_biasadd_readvariableop_resource 
bn_1_readvariableop_resource"
bn_1_readvariableop_1_resource1
-bn_1_fusedbatchnormv3_readvariableop_resource3
/bn_1_fusedbatchnormv3_readvariableop_1_resource=
9conv_transpose_2_conv2d_transpose_readvariableop_resource4
0conv_transpose_2_biasadd_readvariableop_resource 
bn_2_readvariableop_resource"
bn_2_readvariableop_1_resource1
-bn_2_fusedbatchnormv3_readvariableop_resource3
/bn_2_fusedbatchnormv3_readvariableop_1_resource=
9conv_transpose_3_conv2d_transpose_readvariableop_resource4
0conv_transpose_3_biasadd_readvariableop_resource 
bn_3_readvariableop_resource"
bn_3_readvariableop_1_resource1
-bn_3_fusedbatchnormv3_readvariableop_resource3
/bn_3_fusedbatchnormv3_readvariableop_1_resource=
9conv_transpose_4_conv2d_transpose_readvariableop_resource4
0conv_transpose_4_biasadd_readvariableop_resource
identity��
dense_1/MatMul/ReadVariableOpReadVariableOp&dense_1_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype02
dense_1/MatMul/ReadVariableOp�
dense_1/MatMulMatMulinputs%dense_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_1/MatMul�
dense_1/BiasAdd/ReadVariableOpReadVariableOp'dense_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02 
dense_1/BiasAdd/ReadVariableOp�
dense_1/BiasAddBiasAdddense_1/MatMul:product:0&dense_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_1/BiasAddr
Reshape_Layer/ShapeShapedense_1/BiasAdd:output:0*
T0*
_output_shapes
:2
Reshape_Layer/Shape�
!Reshape_Layer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2#
!Reshape_Layer/strided_slice/stack�
#Reshape_Layer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2%
#Reshape_Layer/strided_slice/stack_1�
#Reshape_Layer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2%
#Reshape_Layer/strided_slice/stack_2�
Reshape_Layer/strided_sliceStridedSliceReshape_Layer/Shape:output:0*Reshape_Layer/strided_slice/stack:output:0,Reshape_Layer/strided_slice/stack_1:output:0,Reshape_Layer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
Reshape_Layer/strided_slice�
Reshape_Layer/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_Layer/Reshape/shape/1�
Reshape_Layer/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_Layer/Reshape/shape/2�
Reshape_Layer/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :@2
Reshape_Layer/Reshape/shape/3�
Reshape_Layer/Reshape/shapePack$Reshape_Layer/strided_slice:output:0&Reshape_Layer/Reshape/shape/1:output:0&Reshape_Layer/Reshape/shape/2:output:0&Reshape_Layer/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape_Layer/Reshape/shape�
Reshape_Layer/ReshapeReshapedense_1/BiasAdd:output:0$Reshape_Layer/Reshape/shape:output:0*
T0*/
_output_shapes
:���������@2
Reshape_Layer/Reshape~
conv_transpose_1/ShapeShapeReshape_Layer/Reshape:output:0*
T0*
_output_shapes
:2
conv_transpose_1/Shape�
$conv_transpose_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2&
$conv_transpose_1/strided_slice/stack�
&conv_transpose_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_1/strided_slice/stack_1�
&conv_transpose_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_1/strided_slice/stack_2�
conv_transpose_1/strided_sliceStridedSliceconv_transpose_1/Shape:output:0-conv_transpose_1/strided_slice/stack:output:0/conv_transpose_1/strided_slice/stack_1:output:0/conv_transpose_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2 
conv_transpose_1/strided_slice�
&conv_transpose_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_1/strided_slice_1/stack�
(conv_transpose_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_1/strided_slice_1/stack_1�
(conv_transpose_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_1/strided_slice_1/stack_2�
 conv_transpose_1/strided_slice_1StridedSliceconv_transpose_1/Shape:output:0/conv_transpose_1/strided_slice_1/stack:output:01conv_transpose_1/strided_slice_1/stack_1:output:01conv_transpose_1/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_1/strided_slice_1�
&conv_transpose_1/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_1/strided_slice_2/stack�
(conv_transpose_1/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_1/strided_slice_2/stack_1�
(conv_transpose_1/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_1/strided_slice_2/stack_2�
 conv_transpose_1/strided_slice_2StridedSliceconv_transpose_1/Shape:output:0/conv_transpose_1/strided_slice_2/stack:output:01conv_transpose_1/strided_slice_2/stack_1:output:01conv_transpose_1/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_1/strided_slice_2r
conv_transpose_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_1/mul/y�
conv_transpose_1/mulMul)conv_transpose_1/strided_slice_1:output:0conv_transpose_1/mul/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_1/mulv
conv_transpose_1/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_1/mul_1/y�
conv_transpose_1/mul_1Mul)conv_transpose_1/strided_slice_2:output:0!conv_transpose_1/mul_1/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_1/mul_1v
conv_transpose_1/stack/3Const*
_output_shapes
: *
dtype0*
value	B :@2
conv_transpose_1/stack/3�
conv_transpose_1/stackPack'conv_transpose_1/strided_slice:output:0conv_transpose_1/mul:z:0conv_transpose_1/mul_1:z:0!conv_transpose_1/stack/3:output:0*
N*
T0*
_output_shapes
:2
conv_transpose_1/stack�
&conv_transpose_1/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 2(
&conv_transpose_1/strided_slice_3/stack�
(conv_transpose_1/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_1/strided_slice_3/stack_1�
(conv_transpose_1/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_1/strided_slice_3/stack_2�
 conv_transpose_1/strided_slice_3StridedSliceconv_transpose_1/stack:output:0/conv_transpose_1/strided_slice_3/stack:output:01conv_transpose_1/strided_slice_3/stack_1:output:01conv_transpose_1/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_1/strided_slice_3�
0conv_transpose_1/conv2d_transpose/ReadVariableOpReadVariableOp9conv_transpose_1_conv2d_transpose_readvariableop_resource*&
_output_shapes
:@@*
dtype022
0conv_transpose_1/conv2d_transpose/ReadVariableOp�
!conv_transpose_1/conv2d_transposeConv2DBackpropInputconv_transpose_1/stack:output:08conv_transpose_1/conv2d_transpose/ReadVariableOp:value:0Reshape_Layer/Reshape:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
2#
!conv_transpose_1/conv2d_transpose�
'conv_transpose_1/BiasAdd/ReadVariableOpReadVariableOp0conv_transpose_1_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02)
'conv_transpose_1/BiasAdd/ReadVariableOp�
conv_transpose_1/BiasAddBiasAdd*conv_transpose_1/conv2d_transpose:output:0/conv_transpose_1/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@2
conv_transpose_1/BiasAdd�
bn_1/ReadVariableOpReadVariableOpbn_1_readvariableop_resource*
_output_shapes
:@*
dtype02
bn_1/ReadVariableOp�
bn_1/ReadVariableOp_1ReadVariableOpbn_1_readvariableop_1_resource*
_output_shapes
:@*
dtype02
bn_1/ReadVariableOp_1�
$bn_1/FusedBatchNormV3/ReadVariableOpReadVariableOp-bn_1_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02&
$bn_1/FusedBatchNormV3/ReadVariableOp�
&bn_1/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp/bn_1_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02(
&bn_1/FusedBatchNormV3/ReadVariableOp_1�
bn_1/FusedBatchNormV3FusedBatchNormV3!conv_transpose_1/BiasAdd:output:0bn_1/ReadVariableOp:value:0bn_1/ReadVariableOp_1:value:0,bn_1/FusedBatchNormV3/ReadVariableOp:value:0.bn_1/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:���������@:@:@:@:@:*
epsilon%o�:*
is_training( 2
bn_1/FusedBatchNormV3�
lrelu_1/LeakyRelu	LeakyRelubn_1/FusedBatchNormV3:y:0*/
_output_shapes
:���������@*
alpha%���>2
lrelu_1/LeakyRelu
conv_transpose_2/ShapeShapelrelu_1/LeakyRelu:activations:0*
T0*
_output_shapes
:2
conv_transpose_2/Shape�
$conv_transpose_2/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2&
$conv_transpose_2/strided_slice/stack�
&conv_transpose_2/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_2/strided_slice/stack_1�
&conv_transpose_2/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_2/strided_slice/stack_2�
conv_transpose_2/strided_sliceStridedSliceconv_transpose_2/Shape:output:0-conv_transpose_2/strided_slice/stack:output:0/conv_transpose_2/strided_slice/stack_1:output:0/conv_transpose_2/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2 
conv_transpose_2/strided_slice�
&conv_transpose_2/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_2/strided_slice_1/stack�
(conv_transpose_2/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_2/strided_slice_1/stack_1�
(conv_transpose_2/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_2/strided_slice_1/stack_2�
 conv_transpose_2/strided_slice_1StridedSliceconv_transpose_2/Shape:output:0/conv_transpose_2/strided_slice_1/stack:output:01conv_transpose_2/strided_slice_1/stack_1:output:01conv_transpose_2/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_2/strided_slice_1�
&conv_transpose_2/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_2/strided_slice_2/stack�
(conv_transpose_2/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_2/strided_slice_2/stack_1�
(conv_transpose_2/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_2/strided_slice_2/stack_2�
 conv_transpose_2/strided_slice_2StridedSliceconv_transpose_2/Shape:output:0/conv_transpose_2/strided_slice_2/stack:output:01conv_transpose_2/strided_slice_2/stack_1:output:01conv_transpose_2/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_2/strided_slice_2r
conv_transpose_2/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_2/mul/y�
conv_transpose_2/mulMul)conv_transpose_2/strided_slice_1:output:0conv_transpose_2/mul/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_2/mulv
conv_transpose_2/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_2/mul_1/y�
conv_transpose_2/mul_1Mul)conv_transpose_2/strided_slice_2:output:0!conv_transpose_2/mul_1/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_2/mul_1v
conv_transpose_2/stack/3Const*
_output_shapes
: *
dtype0*
value	B :@2
conv_transpose_2/stack/3�
conv_transpose_2/stackPack'conv_transpose_2/strided_slice:output:0conv_transpose_2/mul:z:0conv_transpose_2/mul_1:z:0!conv_transpose_2/stack/3:output:0*
N*
T0*
_output_shapes
:2
conv_transpose_2/stack�
&conv_transpose_2/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 2(
&conv_transpose_2/strided_slice_3/stack�
(conv_transpose_2/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_2/strided_slice_3/stack_1�
(conv_transpose_2/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_2/strided_slice_3/stack_2�
 conv_transpose_2/strided_slice_3StridedSliceconv_transpose_2/stack:output:0/conv_transpose_2/strided_slice_3/stack:output:01conv_transpose_2/strided_slice_3/stack_1:output:01conv_transpose_2/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_2/strided_slice_3�
0conv_transpose_2/conv2d_transpose/ReadVariableOpReadVariableOp9conv_transpose_2_conv2d_transpose_readvariableop_resource*&
_output_shapes
:@@*
dtype022
0conv_transpose_2/conv2d_transpose/ReadVariableOp�
!conv_transpose_2/conv2d_transposeConv2DBackpropInputconv_transpose_2/stack:output:08conv_transpose_2/conv2d_transpose/ReadVariableOp:value:0lrelu_1/LeakyRelu:activations:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
2#
!conv_transpose_2/conv2d_transpose�
'conv_transpose_2/BiasAdd/ReadVariableOpReadVariableOp0conv_transpose_2_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02)
'conv_transpose_2/BiasAdd/ReadVariableOp�
conv_transpose_2/BiasAddBiasAdd*conv_transpose_2/conv2d_transpose:output:0/conv_transpose_2/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@2
conv_transpose_2/BiasAdd�
bn_2/ReadVariableOpReadVariableOpbn_2_readvariableop_resource*
_output_shapes
:@*
dtype02
bn_2/ReadVariableOp�
bn_2/ReadVariableOp_1ReadVariableOpbn_2_readvariableop_1_resource*
_output_shapes
:@*
dtype02
bn_2/ReadVariableOp_1�
$bn_2/FusedBatchNormV3/ReadVariableOpReadVariableOp-bn_2_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02&
$bn_2/FusedBatchNormV3/ReadVariableOp�
&bn_2/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp/bn_2_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02(
&bn_2/FusedBatchNormV3/ReadVariableOp_1�
bn_2/FusedBatchNormV3FusedBatchNormV3!conv_transpose_2/BiasAdd:output:0bn_2/ReadVariableOp:value:0bn_2/ReadVariableOp_1:value:0,bn_2/FusedBatchNormV3/ReadVariableOp:value:0.bn_2/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:���������@:@:@:@:@:*
epsilon%o�:*
is_training( 2
bn_2/FusedBatchNormV3�
lrelu_2/LeakyRelu	LeakyRelubn_2/FusedBatchNormV3:y:0*/
_output_shapes
:���������@*
alpha%���>2
lrelu_2/LeakyRelu
conv_transpose_3/ShapeShapelrelu_2/LeakyRelu:activations:0*
T0*
_output_shapes
:2
conv_transpose_3/Shape�
$conv_transpose_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2&
$conv_transpose_3/strided_slice/stack�
&conv_transpose_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_3/strided_slice/stack_1�
&conv_transpose_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_3/strided_slice/stack_2�
conv_transpose_3/strided_sliceStridedSliceconv_transpose_3/Shape:output:0-conv_transpose_3/strided_slice/stack:output:0/conv_transpose_3/strided_slice/stack_1:output:0/conv_transpose_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2 
conv_transpose_3/strided_slice�
&conv_transpose_3/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_3/strided_slice_1/stack�
(conv_transpose_3/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_3/strided_slice_1/stack_1�
(conv_transpose_3/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_3/strided_slice_1/stack_2�
 conv_transpose_3/strided_slice_1StridedSliceconv_transpose_3/Shape:output:0/conv_transpose_3/strided_slice_1/stack:output:01conv_transpose_3/strided_slice_1/stack_1:output:01conv_transpose_3/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_3/strided_slice_1�
&conv_transpose_3/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_3/strided_slice_2/stack�
(conv_transpose_3/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_3/strided_slice_2/stack_1�
(conv_transpose_3/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_3/strided_slice_2/stack_2�
 conv_transpose_3/strided_slice_2StridedSliceconv_transpose_3/Shape:output:0/conv_transpose_3/strided_slice_2/stack:output:01conv_transpose_3/strided_slice_2/stack_1:output:01conv_transpose_3/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_3/strided_slice_2r
conv_transpose_3/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_3/mul/y�
conv_transpose_3/mulMul)conv_transpose_3/strided_slice_1:output:0conv_transpose_3/mul/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_3/mulv
conv_transpose_3/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_3/mul_1/y�
conv_transpose_3/mul_1Mul)conv_transpose_3/strided_slice_2:output:0!conv_transpose_3/mul_1/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_3/mul_1v
conv_transpose_3/stack/3Const*
_output_shapes
: *
dtype0*
value	B :@2
conv_transpose_3/stack/3�
conv_transpose_3/stackPack'conv_transpose_3/strided_slice:output:0conv_transpose_3/mul:z:0conv_transpose_3/mul_1:z:0!conv_transpose_3/stack/3:output:0*
N*
T0*
_output_shapes
:2
conv_transpose_3/stack�
&conv_transpose_3/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 2(
&conv_transpose_3/strided_slice_3/stack�
(conv_transpose_3/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_3/strided_slice_3/stack_1�
(conv_transpose_3/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_3/strided_slice_3/stack_2�
 conv_transpose_3/strided_slice_3StridedSliceconv_transpose_3/stack:output:0/conv_transpose_3/strided_slice_3/stack:output:01conv_transpose_3/strided_slice_3/stack_1:output:01conv_transpose_3/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_3/strided_slice_3�
0conv_transpose_3/conv2d_transpose/ReadVariableOpReadVariableOp9conv_transpose_3_conv2d_transpose_readvariableop_resource*&
_output_shapes
:@@*
dtype022
0conv_transpose_3/conv2d_transpose/ReadVariableOp�
!conv_transpose_3/conv2d_transposeConv2DBackpropInputconv_transpose_3/stack:output:08conv_transpose_3/conv2d_transpose/ReadVariableOp:value:0lrelu_2/LeakyRelu:activations:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
2#
!conv_transpose_3/conv2d_transpose�
'conv_transpose_3/BiasAdd/ReadVariableOpReadVariableOp0conv_transpose_3_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02)
'conv_transpose_3/BiasAdd/ReadVariableOp�
conv_transpose_3/BiasAddBiasAdd*conv_transpose_3/conv2d_transpose:output:0/conv_transpose_3/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@2
conv_transpose_3/BiasAdd�
bn_3/ReadVariableOpReadVariableOpbn_3_readvariableop_resource*
_output_shapes
:@*
dtype02
bn_3/ReadVariableOp�
bn_3/ReadVariableOp_1ReadVariableOpbn_3_readvariableop_1_resource*
_output_shapes
:@*
dtype02
bn_3/ReadVariableOp_1�
$bn_3/FusedBatchNormV3/ReadVariableOpReadVariableOp-bn_3_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02&
$bn_3/FusedBatchNormV3/ReadVariableOp�
&bn_3/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp/bn_3_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02(
&bn_3/FusedBatchNormV3/ReadVariableOp_1�
bn_3/FusedBatchNormV3FusedBatchNormV3!conv_transpose_3/BiasAdd:output:0bn_3/ReadVariableOp:value:0bn_3/ReadVariableOp_1:value:0,bn_3/FusedBatchNormV3/ReadVariableOp:value:0.bn_3/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:���������@:@:@:@:@:*
epsilon%o�:*
is_training( 2
bn_3/FusedBatchNormV3�
lrelu_3/LeakyRelu	LeakyRelubn_3/FusedBatchNormV3:y:0*/
_output_shapes
:���������@*
alpha%���>2
lrelu_3/LeakyRelu
conv_transpose_4/ShapeShapelrelu_3/LeakyRelu:activations:0*
T0*
_output_shapes
:2
conv_transpose_4/Shape�
$conv_transpose_4/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2&
$conv_transpose_4/strided_slice/stack�
&conv_transpose_4/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_4/strided_slice/stack_1�
&conv_transpose_4/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_4/strided_slice/stack_2�
conv_transpose_4/strided_sliceStridedSliceconv_transpose_4/Shape:output:0-conv_transpose_4/strided_slice/stack:output:0/conv_transpose_4/strided_slice/stack_1:output:0/conv_transpose_4/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2 
conv_transpose_4/strided_slice�
&conv_transpose_4/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_4/strided_slice_1/stack�
(conv_transpose_4/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_4/strided_slice_1/stack_1�
(conv_transpose_4/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_4/strided_slice_1/stack_2�
 conv_transpose_4/strided_slice_1StridedSliceconv_transpose_4/Shape:output:0/conv_transpose_4/strided_slice_1/stack:output:01conv_transpose_4/strided_slice_1/stack_1:output:01conv_transpose_4/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_4/strided_slice_1�
&conv_transpose_4/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&conv_transpose_4/strided_slice_2/stack�
(conv_transpose_4/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_4/strided_slice_2/stack_1�
(conv_transpose_4/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_4/strided_slice_2/stack_2�
 conv_transpose_4/strided_slice_2StridedSliceconv_transpose_4/Shape:output:0/conv_transpose_4/strided_slice_2/stack:output:01conv_transpose_4/strided_slice_2/stack_1:output:01conv_transpose_4/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_4/strided_slice_2r
conv_transpose_4/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_4/mul/y�
conv_transpose_4/mulMul)conv_transpose_4/strided_slice_1:output:0conv_transpose_4/mul/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_4/mulv
conv_transpose_4/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_4/mul_1/y�
conv_transpose_4/mul_1Mul)conv_transpose_4/strided_slice_2:output:0!conv_transpose_4/mul_1/y:output:0*
T0*
_output_shapes
: 2
conv_transpose_4/mul_1v
conv_transpose_4/stack/3Const*
_output_shapes
: *
dtype0*
value	B :2
conv_transpose_4/stack/3�
conv_transpose_4/stackPack'conv_transpose_4/strided_slice:output:0conv_transpose_4/mul:z:0conv_transpose_4/mul_1:z:0!conv_transpose_4/stack/3:output:0*
N*
T0*
_output_shapes
:2
conv_transpose_4/stack�
&conv_transpose_4/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: 2(
&conv_transpose_4/strided_slice_3/stack�
(conv_transpose_4/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_4/strided_slice_3/stack_1�
(conv_transpose_4/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(conv_transpose_4/strided_slice_3/stack_2�
 conv_transpose_4/strided_slice_3StridedSliceconv_transpose_4/stack:output:0/conv_transpose_4/strided_slice_3/stack:output:01conv_transpose_4/strided_slice_3/stack_1:output:01conv_transpose_4/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 conv_transpose_4/strided_slice_3�
0conv_transpose_4/conv2d_transpose/ReadVariableOpReadVariableOp9conv_transpose_4_conv2d_transpose_readvariableop_resource*&
_output_shapes
:@*
dtype022
0conv_transpose_4/conv2d_transpose/ReadVariableOp�
!conv_transpose_4/conv2d_transposeConv2DBackpropInputconv_transpose_4/stack:output:08conv_transpose_4/conv2d_transpose/ReadVariableOp:value:0lrelu_3/LeakyRelu:activations:0*
T0*/
_output_shapes
:���������*
paddingSAME*
strides
2#
!conv_transpose_4/conv2d_transpose�
'conv_transpose_4/BiasAdd/ReadVariableOpReadVariableOp0conv_transpose_4_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02)
'conv_transpose_4/BiasAdd/ReadVariableOp�
conv_transpose_4/BiasAddBiasAdd*conv_transpose_4/conv2d_transpose:output:0/conv_transpose_4/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
conv_transpose_4/BiasAdd�
conv_transpose_4/SoftplusSoftplus!conv_transpose_4/BiasAdd:output:0*
T0*/
_output_shapes
:���������2
conv_transpose_4/Softplus�
IdentityIdentity'conv_transpose_4/Softplus:activations:0*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*~
_input_shapesm
k:���������:::::::::::::::::::::::O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�=
�
B__inference_Decoder_layer_call_and_return_conditional_losses_45099

inputs
dense_1_45042
dense_1_45044
conv_transpose_1_45048
conv_transpose_1_45050

bn_1_45053

bn_1_45055

bn_1_45057

bn_1_45059
conv_transpose_2_45063
conv_transpose_2_45065

bn_2_45068

bn_2_45070

bn_2_45072

bn_2_45074
conv_transpose_3_45078
conv_transpose_3_45080

bn_3_45083

bn_3_45085

bn_3_45087

bn_3_45089
conv_transpose_4_45093
conv_transpose_4_45095
identity��bn_1/StatefulPartitionedCall�bn_2/StatefulPartitionedCall�bn_3/StatefulPartitionedCall�(conv_transpose_1/StatefulPartitionedCall�(conv_transpose_2/StatefulPartitionedCall�(conv_transpose_3/StatefulPartitionedCall�(conv_transpose_4/StatefulPartitionedCall�dense_1/StatefulPartitionedCall�
dense_1/StatefulPartitionedCallStatefulPartitionedCallinputsdense_1_45042dense_1_45044*
Tin
2*
Tout
2*(
_output_shapes
:����������*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_dense_1_layer_call_and_return_conditional_losses_447732!
dense_1/StatefulPartitionedCall�
Reshape_Layer/PartitionedCallPartitionedCall(dense_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*/
_output_shapes
:���������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*Q
fLRJ
H__inference_Reshape_Layer_layer_call_and_return_conditional_losses_448032
Reshape_Layer/PartitionedCall�
(conv_transpose_1/StatefulPartitionedCallStatefulPartitionedCall&Reshape_Layer/PartitionedCall:output:0conv_transpose_1_45048conv_transpose_1_45050*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_1_layer_call_and_return_conditional_losses_442382*
(conv_transpose_1/StatefulPartitionedCall�
bn_1/StatefulPartitionedCallStatefulPartitionedCall1conv_transpose_1/StatefulPartitionedCall:output:0
bn_1_45053
bn_1_45055
bn_1_45057
bn_1_45059*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_1_layer_call_and_return_conditional_losses_443322
bn_1/StatefulPartitionedCall�
lrelu_1/PartitionedCallPartitionedCall%bn_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_lrelu_1_layer_call_and_return_conditional_losses_448562
lrelu_1/PartitionedCall�
(conv_transpose_2/StatefulPartitionedCallStatefulPartitionedCall lrelu_1/PartitionedCall:output:0conv_transpose_2_45063conv_transpose_2_45065*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_2_layer_call_and_return_conditional_losses_444082*
(conv_transpose_2/StatefulPartitionedCall�
bn_2/StatefulPartitionedCallStatefulPartitionedCall1conv_transpose_2/StatefulPartitionedCall:output:0
bn_2_45068
bn_2_45070
bn_2_45072
bn_2_45074*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_2_layer_call_and_return_conditional_losses_445022
bn_2/StatefulPartitionedCall�
lrelu_2/PartitionedCallPartitionedCall%bn_2/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_lrelu_2_layer_call_and_return_conditional_losses_449092
lrelu_2/PartitionedCall�
(conv_transpose_3/StatefulPartitionedCallStatefulPartitionedCall lrelu_2/PartitionedCall:output:0conv_transpose_3_45078conv_transpose_3_45080*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_3_layer_call_and_return_conditional_losses_445782*
(conv_transpose_3/StatefulPartitionedCall�
bn_3/StatefulPartitionedCallStatefulPartitionedCall1conv_transpose_3/StatefulPartitionedCall:output:0
bn_3_45083
bn_3_45085
bn_3_45087
bn_3_45089*
Tin	
2*
Tout
2*A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*H
fCRA
?__inference_bn_3_layer_call_and_return_conditional_losses_446722
bn_3/StatefulPartitionedCall�
lrelu_3/PartitionedCallPartitionedCall%bn_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������@* 
_read_only_resource_inputs
 **
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_lrelu_3_layer_call_and_return_conditional_losses_449622
lrelu_3/PartitionedCall�
(conv_transpose_4/StatefulPartitionedCallStatefulPartitionedCall lrelu_3/PartitionedCall:output:0conv_transpose_4_45093conv_transpose_4_45095*
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������*$
_read_only_resource_inputs
**
config_proto

GPU 

CPU2J 8*T
fORM
K__inference_conv_transpose_4_layer_call_and_return_conditional_losses_447492*
(conv_transpose_4/StatefulPartitionedCall�
IdentityIdentity1conv_transpose_4/StatefulPartitionedCall:output:0^bn_1/StatefulPartitionedCall^bn_2/StatefulPartitionedCall^bn_3/StatefulPartitionedCall)^conv_transpose_1/StatefulPartitionedCall)^conv_transpose_2/StatefulPartitionedCall)^conv_transpose_3/StatefulPartitionedCall)^conv_transpose_4/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������2

Identity"
identityIdentity:output:0*~
_input_shapesm
k:���������::::::::::::::::::::::2<
bn_1/StatefulPartitionedCallbn_1/StatefulPartitionedCall2<
bn_2/StatefulPartitionedCallbn_2/StatefulPartitionedCall2<
bn_3/StatefulPartitionedCallbn_3/StatefulPartitionedCall2T
(conv_transpose_1/StatefulPartitionedCall(conv_transpose_1/StatefulPartitionedCall2T
(conv_transpose_2/StatefulPartitionedCall(conv_transpose_2/StatefulPartitionedCall2T
(conv_transpose_3/StatefulPartitionedCall(conv_transpose_3/StatefulPartitionedCall2T
(conv_transpose_4/StatefulPartitionedCall(conv_transpose_4/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
'__inference_Decoder_layer_call_fn_45255
decoder_input
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCalldecoder_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20*"
Tin
2*
Tout
2*A
_output_shapes/
-:+���������������������������*8
_read_only_resource_inputs
	
**
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_Decoder_layer_call_and_return_conditional_losses_452082
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������2

Identity"
identityIdentity:output:0*~
_input_shapesm
k:���������::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:V R
'
_output_shapes
:���������
'
_user_specified_namedecoder_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�$
�
?__inference_bn_1_layer_call_and_return_conditional_losses_45924

inputs
readvariableop_resource
readvariableop_1_resource,
(fusedbatchnormv3_readvariableop_resource.
*fusedbatchnormv3_readvariableop_1_resource
identity��#AssignMovingAvg/AssignSubVariableOp�%AssignMovingAvg_1/AssignSubVariableOpt
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype02
ReadVariableOpz
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype02
ReadVariableOp_1�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02!
FusedBatchNormV3/ReadVariableOp�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02#
!FusedBatchNormV3/ReadVariableOp_1�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:2
FusedBatchNormV3S
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *�p}?2
Const�
AssignMovingAvg/sub/xConst*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
AssignMovingAvg/sub/x�
AssignMovingAvg/subSubAssignMovingAvg/sub/x:output:0Const:output:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: 2
AssignMovingAvg/sub�
AssignMovingAvg/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/sub_1Sub&AssignMovingAvg/ReadVariableOp:value:0FusedBatchNormV3:batch_mean:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
AssignMovingAvg/sub_1�
AssignMovingAvg/mulMulAssignMovingAvg/sub_1:z:0AssignMovingAvg/sub:z:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
AssignMovingAvg/mul�
#AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp(fusedbatchnormv3_readvariableop_resourceAssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp ^FusedBatchNormV3/ReadVariableOp*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
 *
dtype02%
#AssignMovingAvg/AssignSubVariableOp�
AssignMovingAvg_1/sub/xConst*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
AssignMovingAvg_1/sub/x�
AssignMovingAvg_1/subSub AssignMovingAvg_1/sub/x:output:0Const:output:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: 2
AssignMovingAvg_1/sub�
 AssignMovingAvg_1/ReadVariableOpReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/sub_1Sub(AssignMovingAvg_1/ReadVariableOp:value:0!FusedBatchNormV3:batch_variance:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
AssignMovingAvg_1/sub_1�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub_1:z:0AssignMovingAvg_1/sub:z:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
AssignMovingAvg_1/mul�
%AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp*fusedbatchnormv3_readvariableop_1_resourceAssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
 *
dtype02'
%AssignMovingAvg_1/AssignSubVariableOp�
IdentityIdentityFusedBatchNormV3:y:0$^AssignMovingAvg/AssignSubVariableOp&^AssignMovingAvg_1/AssignSubVariableOp*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@::::2J
#AssignMovingAvg/AssignSubVariableOp#AssignMovingAvg/AssignSubVariableOp2N
%AssignMovingAvg_1/AssignSubVariableOp%AssignMovingAvg_1/AssignSubVariableOp:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�$
�
?__inference_bn_2_layer_call_and_return_conditional_losses_46021

inputs
readvariableop_resource
readvariableop_1_resource,
(fusedbatchnormv3_readvariableop_resource.
*fusedbatchnormv3_readvariableop_1_resource
identity��#AssignMovingAvg/AssignSubVariableOp�%AssignMovingAvg_1/AssignSubVariableOpt
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype02
ReadVariableOpz
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype02
ReadVariableOp_1�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02!
FusedBatchNormV3/ReadVariableOp�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02#
!FusedBatchNormV3/ReadVariableOp_1�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:2
FusedBatchNormV3S
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *�p}?2
Const�
AssignMovingAvg/sub/xConst*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
AssignMovingAvg/sub/x�
AssignMovingAvg/subSubAssignMovingAvg/sub/x:output:0Const:output:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
: 2
AssignMovingAvg/sub�
AssignMovingAvg/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/sub_1Sub&AssignMovingAvg/ReadVariableOp:value:0FusedBatchNormV3:batch_mean:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
AssignMovingAvg/sub_1�
AssignMovingAvg/mulMulAssignMovingAvg/sub_1:z:0AssignMovingAvg/sub:z:0*
T0*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
:@2
AssignMovingAvg/mul�
#AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp(fusedbatchnormv3_readvariableop_resourceAssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp ^FusedBatchNormV3/ReadVariableOp*;
_class1
/-loc:@FusedBatchNormV3/ReadVariableOp/resource*
_output_shapes
 *
dtype02%
#AssignMovingAvg/AssignSubVariableOp�
AssignMovingAvg_1/sub/xConst*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: *
dtype0*
valueB
 *  �?2
AssignMovingAvg_1/sub/x�
AssignMovingAvg_1/subSub AssignMovingAvg_1/sub/x:output:0Const:output:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
: 2
AssignMovingAvg_1/sub�
 AssignMovingAvg_1/ReadVariableOpReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/sub_1Sub(AssignMovingAvg_1/ReadVariableOp:value:0!FusedBatchNormV3:batch_variance:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
AssignMovingAvg_1/sub_1�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub_1:z:0AssignMovingAvg_1/sub:z:0*
T0*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
:@2
AssignMovingAvg_1/mul�
%AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp*fusedbatchnormv3_readvariableop_1_resourceAssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1*=
_class3
1/loc:@FusedBatchNormV3/ReadVariableOp_1/resource*
_output_shapes
 *
dtype02'
%AssignMovingAvg_1/AssignSubVariableOp�
IdentityIdentityFusedBatchNormV3:y:0$^AssignMovingAvg/AssignSubVariableOp&^AssignMovingAvg_1/AssignSubVariableOp*
T0*A
_output_shapes/
-:+���������������������������@2

Identity"
identityIdentity:output:0*P
_input_shapes?
=:+���������������������������@::::2J
#AssignMovingAvg/AssignSubVariableOp#AssignMovingAvg/AssignSubVariableOp2N
%AssignMovingAvg_1/AssignSubVariableOp%AssignMovingAvg_1/AssignSubVariableOp:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: "�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
G
decoder_input6
serving_default_decoder_input:0���������L
conv_transpose_48
StatefulPartitionedCall:0���������tensorflow/serving/predict:�
�n
layer-0
layer_with_weights-0
layer-1
layer-2
layer_with_weights-1
layer-3
layer_with_weights-2
layer-4
layer-5
layer_with_weights-3
layer-6
layer_with_weights-4
layer-7
	layer-8

layer_with_weights-5

layer-9
layer_with_weights-6
layer-10
layer-11
layer_with_weights-7
layer-12
trainable_variables
	variables
regularization_losses
	keras_api

signatures
+�&call_and_return_all_conditional_losses
�__call__
�_default_save_signature"�j
_tf_keras_model�i{"class_name": "Model", "name": "Decoder", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "config": {"name": "Decoder", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 5]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "decoder_input"}, "name": "decoder_input", "inbound_nodes": []}, {"class_name": "Dense", "config": {"name": "dense_1", "trainable": true, "dtype": "float32", "units": 768, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_1", "inbound_nodes": [[["decoder_input", 0, 0, {}]]]}, {"class_name": "Reshape", "config": {"name": "Reshape_Layer", "trainable": true, "dtype": "float32", "target_shape": {"class_name": "__tuple__", "items": [3, 4, 64]}}, "name": "Reshape_Layer", "inbound_nodes": [[["dense_1", 0, 0, {}]]]}, {"class_name": "Conv2DTranspose", "config": {"name": "conv_transpose_1", "trainable": true, "dtype": "float32", "filters": 64, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [1, 1]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "output_padding": null}, "name": "conv_transpose_1", "inbound_nodes": [[["Reshape_Layer", 0, 0, {}]]]}, {"class_name": "BatchNormalization", "config": {"name": "bn_1", "trainable": true, "dtype": "float32", "axis": [3], "momentum": 0.99, "epsilon": 0.001, "center": true, "scale": true, "beta_initializer": {"class_name": "Zeros", "config": {}}, "gamma_initializer": {"class_name": "Ones", "config": {}}, "moving_mean_initializer": {"class_name": "Zeros", "config": {}}, "moving_variance_initializer": {"class_name": "Ones", "config": {}}, "beta_regularizer": null, "gamma_regularizer": null, "beta_constraint": null, "gamma_constraint": null}, "name": "bn_1", "inbound_nodes": [[["conv_transpose_1", 0, 0, {}]]]}, {"class_name": "LeakyReLU", "config": {"name": "lrelu_1", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}, "name": "lrelu_1", "inbound_nodes": [[["bn_1", 0, 0, {}]]]}, {"class_name": "Conv2DTranspose", "config": {"name": "conv_transpose_2", "trainable": true, "dtype": "float32", "filters": 64, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "output_padding": null}, "name": "conv_transpose_2", "inbound_nodes": [[["lrelu_1", 0, 0, {}]]]}, {"class_name": "BatchNormalization", "config": {"name": "bn_2", "trainable": true, "dtype": "float32", "axis": [3], "momentum": 0.99, "epsilon": 0.001, "center": true, "scale": true, "beta_initializer": {"class_name": "Zeros", "config": {}}, "gamma_initializer": {"class_name": "Ones", "config": {}}, "moving_mean_initializer": {"class_name": "Zeros", "config": {}}, "moving_variance_initializer": {"class_name": "Ones", "config": {}}, "beta_regularizer": null, "gamma_regularizer": null, "beta_constraint": null, "gamma_constraint": null}, "name": "bn_2", "inbound_nodes": [[["conv_transpose_2", 0, 0, {}]]]}, {"class_name": "LeakyReLU", "config": {"name": "lrelu_2", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}, "name": "lrelu_2", "inbound_nodes": [[["bn_2", 0, 0, {}]]]}, {"class_name": "Conv2DTranspose", "config": {"name": "conv_transpose_3", "trainable": true, "dtype": "float32", "filters": 64, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "output_padding": null}, "name": "conv_transpose_3", "inbound_nodes": [[["lrelu_2", 0, 0, {}]]]}, {"class_name": "BatchNormalization", "config": {"name": "bn_3", "trainable": true, "dtype": "float32", "axis": [3], "momentum": 0.99, "epsilon": 0.001, "center": true, "scale": true, "beta_initializer": {"class_name": "Zeros", "config": {}}, "gamma_initializer": {"class_name": "Ones", "config": {}}, "moving_mean_initializer": {"class_name": "Zeros", "config": {}}, "moving_variance_initializer": {"class_name": "Ones", "config": {}}, "beta_regularizer": null, "gamma_regularizer": null, "beta_constraint": null, "gamma_constraint": null}, "name": "bn_3", "inbound_nodes": [[["conv_transpose_3", 0, 0, {}]]]}, {"class_name": "LeakyReLU", "config": {"name": "lrelu_3", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}, "name": "lrelu_3", "inbound_nodes": [[["bn_3", 0, 0, {}]]]}, {"class_name": "Conv2DTranspose", "config": {"name": "conv_transpose_4", "trainable": true, "dtype": "float32", "filters": 1, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [1, 1]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "activation": "softplus", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "output_padding": null}, "name": "conv_transpose_4", "inbound_nodes": [[["lrelu_3", 0, 0, {}]]]}], "input_layers": [["decoder_input", 0, 0]], "output_layers": [["conv_transpose_4", 0, 0]]}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 5]}, "is_graph_network": true, "keras_version": "2.3.0-tf", "backend": "tensorflow", "model_config": {"class_name": "Model", "config": {"name": "Decoder", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 5]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "decoder_input"}, "name": "decoder_input", "inbound_nodes": []}, {"class_name": "Dense", "config": {"name": "dense_1", "trainable": true, "dtype": "float32", "units": 768, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "dense_1", "inbound_nodes": [[["decoder_input", 0, 0, {}]]]}, {"class_name": "Reshape", "config": {"name": "Reshape_Layer", "trainable": true, "dtype": "float32", "target_shape": {"class_name": "__tuple__", "items": [3, 4, 64]}}, "name": "Reshape_Layer", "inbound_nodes": [[["dense_1", 0, 0, {}]]]}, {"class_name": "Conv2DTranspose", "config": {"name": "conv_transpose_1", "trainable": true, "dtype": "float32", "filters": 64, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [1, 1]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "output_padding": null}, "name": "conv_transpose_1", "inbound_nodes": [[["Reshape_Layer", 0, 0, {}]]]}, {"class_name": "BatchNormalization", "config": {"name": "bn_1", "trainable": true, "dtype": "float32", "axis": [3], "momentum": 0.99, "epsilon": 0.001, "center": true, "scale": true, "beta_initializer": {"class_name": "Zeros", "config": {}}, "gamma_initializer": {"class_name": "Ones", "config": {}}, "moving_mean_initializer": {"class_name": "Zeros", "config": {}}, "moving_variance_initializer": {"class_name": "Ones", "config": {}}, "beta_regularizer": null, "gamma_regularizer": null, "beta_constraint": null, "gamma_constraint": null}, "name": "bn_1", "inbound_nodes": [[["conv_transpose_1", 0, 0, {}]]]}, {"class_name": "LeakyReLU", "config": {"name": "lrelu_1", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}, "name": "lrelu_1", "inbound_nodes": [[["bn_1", 0, 0, {}]]]}, {"class_name": "Conv2DTranspose", "config": {"name": "conv_transpose_2", "trainable": true, "dtype": "float32", "filters": 64, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "output_padding": null}, "name": "conv_transpose_2", "inbound_nodes": [[["lrelu_1", 0, 0, {}]]]}, {"class_name": "BatchNormalization", "config": {"name": "bn_2", "trainable": true, "dtype": "float32", "axis": [3], "momentum": 0.99, "epsilon": 0.001, "center": true, "scale": true, "beta_initializer": {"class_name": "Zeros", "config": {}}, "gamma_initializer": {"class_name": "Ones", "config": {}}, "moving_mean_initializer": {"class_name": "Zeros", "config": {}}, "moving_variance_initializer": {"class_name": "Ones", "config": {}}, "beta_regularizer": null, "gamma_regularizer": null, "beta_constraint": null, "gamma_constraint": null}, "name": "bn_2", "inbound_nodes": [[["conv_transpose_2", 0, 0, {}]]]}, {"class_name": "LeakyReLU", "config": {"name": "lrelu_2", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}, "name": "lrelu_2", "inbound_nodes": [[["bn_2", 0, 0, {}]]]}, {"class_name": "Conv2DTranspose", "config": {"name": "conv_transpose_3", "trainable": true, "dtype": "float32", "filters": 64, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "output_padding": null}, "name": "conv_transpose_3", "inbound_nodes": [[["lrelu_2", 0, 0, {}]]]}, {"class_name": "BatchNormalization", "config": {"name": "bn_3", "trainable": true, "dtype": "float32", "axis": [3], "momentum": 0.99, "epsilon": 0.001, "center": true, "scale": true, "beta_initializer": {"class_name": "Zeros", "config": {}}, "gamma_initializer": {"class_name": "Ones", "config": {}}, "moving_mean_initializer": {"class_name": "Zeros", "config": {}}, "moving_variance_initializer": {"class_name": "Ones", "config": {}}, "beta_regularizer": null, "gamma_regularizer": null, "beta_constraint": null, "gamma_constraint": null}, "name": "bn_3", "inbound_nodes": [[["conv_transpose_3", 0, 0, {}]]]}, {"class_name": "LeakyReLU", "config": {"name": "lrelu_3", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}, "name": "lrelu_3", "inbound_nodes": [[["bn_3", 0, 0, {}]]]}, {"class_name": "Conv2DTranspose", "config": {"name": "conv_transpose_4", "trainable": true, "dtype": "float32", "filters": 1, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [1, 1]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "activation": "softplus", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "output_padding": null}, "name": "conv_transpose_4", "inbound_nodes": [[["lrelu_3", 0, 0, {}]]]}], "input_layers": [["decoder_input", 0, 0]], "output_layers": [["conv_transpose_4", 0, 0]]}}}
�"�
_tf_keras_input_layer�{"class_name": "InputLayer", "name": "decoder_input", "dtype": "float32", "sparse": false, "ragged": false, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 5]}, "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 5]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "decoder_input"}}
�

kernel
bias
trainable_variables
	variables
regularization_losses
	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Dense", "name": "dense_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "dense_1", "trainable": true, "dtype": "float32", "units": 768, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 5}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 5]}}
�
trainable_variables
	variables
regularization_losses
	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Reshape", "name": "Reshape_Layer", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "Reshape_Layer", "trainable": true, "dtype": "float32", "target_shape": {"class_name": "__tuple__", "items": [3, 4, 64]}}}
�	

kernel
bias
trainable_variables
 	variables
!regularization_losses
"	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Conv2DTranspose", "name": "conv_transpose_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "conv_transpose_1", "trainable": true, "dtype": "float32", "filters": 64, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [1, 1]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "output_padding": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 3, 4, 64]}}
�
#axis
	$gamma
%beta
&moving_mean
'moving_variance
(trainable_variables
)	variables
*regularization_losses
+	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "BatchNormalization", "name": "bn_1", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "bn_1", "trainable": true, "dtype": "float32", "axis": [3], "momentum": 0.99, "epsilon": 0.001, "center": true, "scale": true, "beta_initializer": {"class_name": "Zeros", "config": {}}, "gamma_initializer": {"class_name": "Ones", "config": {}}, "moving_mean_initializer": {"class_name": "Zeros", "config": {}}, "moving_variance_initializer": {"class_name": "Ones", "config": {}}, "beta_regularizer": null, "gamma_regularizer": null, "beta_constraint": null, "gamma_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"3": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 3, 4, 64]}}
�
,trainable_variables
-	variables
.regularization_losses
/	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "LeakyReLU", "name": "lrelu_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "lrelu_1", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}}
�	

0kernel
1bias
2trainable_variables
3	variables
4regularization_losses
5	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Conv2DTranspose", "name": "conv_transpose_2", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "conv_transpose_2", "trainable": true, "dtype": "float32", "filters": 64, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "output_padding": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 3, 4, 64]}}
�
6axis
	7gamma
8beta
9moving_mean
:moving_variance
;trainable_variables
<	variables
=regularization_losses
>	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "BatchNormalization", "name": "bn_2", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "bn_2", "trainable": true, "dtype": "float32", "axis": [3], "momentum": 0.99, "epsilon": 0.001, "center": true, "scale": true, "beta_initializer": {"class_name": "Zeros", "config": {}}, "gamma_initializer": {"class_name": "Ones", "config": {}}, "moving_mean_initializer": {"class_name": "Zeros", "config": {}}, "moving_variance_initializer": {"class_name": "Ones", "config": {}}, "beta_regularizer": null, "gamma_regularizer": null, "beta_constraint": null, "gamma_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"3": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 6, 8, 64]}}
�
?trainable_variables
@	variables
Aregularization_losses
B	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "LeakyReLU", "name": "lrelu_2", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "lrelu_2", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}}
�	

Ckernel
Dbias
Etrainable_variables
F	variables
Gregularization_losses
H	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Conv2DTranspose", "name": "conv_transpose_3", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "conv_transpose_3", "trainable": true, "dtype": "float32", "filters": 64, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "output_padding": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 6, 8, 64]}}
�
Iaxis
	Jgamma
Kbeta
Lmoving_mean
Mmoving_variance
Ntrainable_variables
O	variables
Pregularization_losses
Q	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "BatchNormalization", "name": "bn_3", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "bn_3", "trainable": true, "dtype": "float32", "axis": [3], "momentum": 0.99, "epsilon": 0.001, "center": true, "scale": true, "beta_initializer": {"class_name": "Zeros", "config": {}}, "gamma_initializer": {"class_name": "Ones", "config": {}}, "moving_mean_initializer": {"class_name": "Zeros", "config": {}}, "moving_variance_initializer": {"class_name": "Ones", "config": {}}, "beta_regularizer": null, "gamma_regularizer": null, "beta_constraint": null, "gamma_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"3": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 12, 16, 64]}}
�
Rtrainable_variables
S	variables
Tregularization_losses
U	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "LeakyReLU", "name": "lrelu_3", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "lrelu_3", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}}
�	

Vkernel
Wbias
Xtrainable_variables
Y	variables
Zregularization_losses
[	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Conv2DTranspose", "name": "conv_transpose_4", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "conv_transpose_4", "trainable": true, "dtype": "float32", "filters": 1, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [1, 1]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "activation": "softplus", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "output_padding": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"-1": 64}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 12, 16, 64]}}
�
0
1
2
3
$4
%5
06
17
78
89
C10
D11
J12
K13
V14
W15"
trackable_list_wrapper
�
0
1
2
3
$4
%5
&6
'7
08
19
710
811
912
:13
C14
D15
J16
K17
L18
M19
V20
W21"
trackable_list_wrapper
 "
trackable_list_wrapper
�
trainable_variables
\layer_metrics
]non_trainable_variables

^layers
	variables
_layer_regularization_losses
regularization_losses
`metrics
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
-
�serving_default"
signature_map
#:!	�2dense_1_3/kernel
:�2dense_1_3/bias
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
alayer_metrics
trainable_variables
bnon_trainable_variables

clayers
	variables
dlayer_regularization_losses
regularization_losses
emetrics
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
flayer_metrics
trainable_variables
gnon_trainable_variables

hlayers
	variables
ilayer_regularization_losses
regularization_losses
jmetrics
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
3:1@@2conv_transpose_1_2/kernel
%:#@2conv_transpose_1_2/bias
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
klayer_metrics
trainable_variables
lnon_trainable_variables

mlayers
 	variables
nlayer_regularization_losses
!regularization_losses
ometrics
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
:@2bn_1_5/gamma
:@2bn_1_5/beta
": @ (2bn_1_5/moving_mean
&:$@ (2bn_1_5/moving_variance
.
$0
%1"
trackable_list_wrapper
<
$0
%1
&2
'3"
trackable_list_wrapper
 "
trackable_list_wrapper
�
player_metrics
(trainable_variables
qnon_trainable_variables

rlayers
)	variables
slayer_regularization_losses
*regularization_losses
tmetrics
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
ulayer_metrics
,trainable_variables
vnon_trainable_variables

wlayers
-	variables
xlayer_regularization_losses
.regularization_losses
ymetrics
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
3:1@@2conv_transpose_2_2/kernel
%:#@2conv_transpose_2_2/bias
.
00
11"
trackable_list_wrapper
.
00
11"
trackable_list_wrapper
 "
trackable_list_wrapper
�
zlayer_metrics
2trainable_variables
{non_trainable_variables

|layers
3	variables
}layer_regularization_losses
4regularization_losses
~metrics
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
:@2bn_2_5/gamma
:@2bn_2_5/beta
": @ (2bn_2_5/moving_mean
&:$@ (2bn_2_5/moving_variance
.
70
81"
trackable_list_wrapper
<
70
81
92
:3"
trackable_list_wrapper
 "
trackable_list_wrapper
�
layer_metrics
;trainable_variables
�non_trainable_variables
�layers
<	variables
 �layer_regularization_losses
=regularization_losses
�metrics
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�layer_metrics
?trainable_variables
�non_trainable_variables
�layers
@	variables
 �layer_regularization_losses
Aregularization_losses
�metrics
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
3:1@@2conv_transpose_3_2/kernel
%:#@2conv_transpose_3_2/bias
.
C0
D1"
trackable_list_wrapper
.
C0
D1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�layer_metrics
Etrainable_variables
�non_trainable_variables
�layers
F	variables
 �layer_regularization_losses
Gregularization_losses
�metrics
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
:@2bn_3_5/gamma
:@2bn_3_5/beta
": @ (2bn_3_5/moving_mean
&:$@ (2bn_3_5/moving_variance
.
J0
K1"
trackable_list_wrapper
<
J0
K1
L2
M3"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�layer_metrics
Ntrainable_variables
�non_trainable_variables
�layers
O	variables
 �layer_regularization_losses
Pregularization_losses
�metrics
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�layer_metrics
Rtrainable_variables
�non_trainable_variables
�layers
S	variables
 �layer_regularization_losses
Tregularization_losses
�metrics
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
3:1@2conv_transpose_4_2/kernel
%:#2conv_transpose_4_2/bias
.
V0
W1"
trackable_list_wrapper
.
V0
W1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�layer_metrics
Xtrainable_variables
�non_trainable_variables
�layers
Y	variables
 �layer_regularization_losses
Zregularization_losses
�metrics
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_dict_wrapper
J
&0
'1
92
:3
L4
M5"
trackable_list_wrapper
~
0
1
2
3
4
5
6
7
	8

9
10
11
12"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
.
&0
'1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
.
90
:1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
.
L0
M1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�2�
B__inference_Decoder_layer_call_and_return_conditional_losses_44976
B__inference_Decoder_layer_call_and_return_conditional_losses_45745
B__inference_Decoder_layer_call_and_return_conditional_losses_45036
B__inference_Decoder_layer_call_and_return_conditional_losses_45563�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
'__inference_Decoder_layer_call_fn_45146
'__inference_Decoder_layer_call_fn_45794
'__inference_Decoder_layer_call_fn_45255
'__inference_Decoder_layer_call_fn_45843�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
 __inference__wrapped_model_44204�
���
FullArgSpec
args� 
varargsjargs
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *,�)
'�$
decoder_input���������
�2�
B__inference_dense_1_layer_call_and_return_conditional_losses_45853�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_dense_1_layer_call_fn_45862�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
H__inference_Reshape_Layer_layer_call_and_return_conditional_losses_45876�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
-__inference_Reshape_Layer_layer_call_fn_45881�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
K__inference_conv_transpose_1_layer_call_and_return_conditional_losses_44238�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *7�4
2�/+���������������������������@
�2�
0__inference_conv_transpose_1_layer_call_fn_44248�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *7�4
2�/+���������������������������@
�2�
?__inference_bn_1_layer_call_and_return_conditional_losses_45942
?__inference_bn_1_layer_call_and_return_conditional_losses_45924�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
$__inference_bn_1_layer_call_fn_45955
$__inference_bn_1_layer_call_fn_45968�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
B__inference_lrelu_1_layer_call_and_return_conditional_losses_45973�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_lrelu_1_layer_call_fn_45978�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
K__inference_conv_transpose_2_layer_call_and_return_conditional_losses_44408�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *7�4
2�/+���������������������������@
�2�
0__inference_conv_transpose_2_layer_call_fn_44418�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *7�4
2�/+���������������������������@
�2�
?__inference_bn_2_layer_call_and_return_conditional_losses_46039
?__inference_bn_2_layer_call_and_return_conditional_losses_46021�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
$__inference_bn_2_layer_call_fn_46052
$__inference_bn_2_layer_call_fn_46065�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
B__inference_lrelu_2_layer_call_and_return_conditional_losses_46070�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_lrelu_2_layer_call_fn_46075�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
K__inference_conv_transpose_3_layer_call_and_return_conditional_losses_44578�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *7�4
2�/+���������������������������@
�2�
0__inference_conv_transpose_3_layer_call_fn_44588�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *7�4
2�/+���������������������������@
�2�
?__inference_bn_3_layer_call_and_return_conditional_losses_46136
?__inference_bn_3_layer_call_and_return_conditional_losses_46118�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
$__inference_bn_3_layer_call_fn_46149
$__inference_bn_3_layer_call_fn_46162�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
B__inference_lrelu_3_layer_call_and_return_conditional_losses_46167�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_lrelu_3_layer_call_fn_46172�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
K__inference_conv_transpose_4_layer_call_and_return_conditional_losses_44749�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *7�4
2�/+���������������������������@
�2�
0__inference_conv_transpose_4_layer_call_fn_44759�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *7�4
2�/+���������������������������@
8B6
#__inference_signature_wrapper_45342decoder_input�
B__inference_Decoder_layer_call_and_return_conditional_losses_44976�$%&'01789:CDJKLMVW>�;
4�1
'�$
decoder_input���������
p

 
� "?�<
5�2
0+���������������������������
� �
B__inference_Decoder_layer_call_and_return_conditional_losses_45036�$%&'01789:CDJKLMVW>�;
4�1
'�$
decoder_input���������
p 

 
� "?�<
5�2
0+���������������������������
� �
B__inference_Decoder_layer_call_and_return_conditional_losses_45563�$%&'01789:CDJKLMVW7�4
-�*
 �
inputs���������
p

 
� "-�*
#� 
0���������
� �
B__inference_Decoder_layer_call_and_return_conditional_losses_45745�$%&'01789:CDJKLMVW7�4
-�*
 �
inputs���������
p 

 
� "-�*
#� 
0���������
� �
'__inference_Decoder_layer_call_fn_45146�$%&'01789:CDJKLMVW>�;
4�1
'�$
decoder_input���������
p

 
� "2�/+����������������������������
'__inference_Decoder_layer_call_fn_45255�$%&'01789:CDJKLMVW>�;
4�1
'�$
decoder_input���������
p 

 
� "2�/+����������������������������
'__inference_Decoder_layer_call_fn_45794�$%&'01789:CDJKLMVW7�4
-�*
 �
inputs���������
p

 
� "2�/+����������������������������
'__inference_Decoder_layer_call_fn_45843�$%&'01789:CDJKLMVW7�4
-�*
 �
inputs���������
p 

 
� "2�/+����������������������������
H__inference_Reshape_Layer_layer_call_and_return_conditional_losses_45876a0�-
&�#
!�
inputs����������
� "-�*
#� 
0���������@
� �
-__inference_Reshape_Layer_layer_call_fn_45881T0�-
&�#
!�
inputs����������
� " ����������@�
 __inference__wrapped_model_44204�$%&'01789:CDJKLMVW6�3
,�)
'�$
decoder_input���������
� "K�H
F
conv_transpose_42�/
conv_transpose_4����������
?__inference_bn_1_layer_call_and_return_conditional_losses_45924�$%&'M�J
C�@
:�7
inputs+���������������������������@
p
� "?�<
5�2
0+���������������������������@
� �
?__inference_bn_1_layer_call_and_return_conditional_losses_45942�$%&'M�J
C�@
:�7
inputs+���������������������������@
p 
� "?�<
5�2
0+���������������������������@
� �
$__inference_bn_1_layer_call_fn_45955�$%&'M�J
C�@
:�7
inputs+���������������������������@
p
� "2�/+���������������������������@�
$__inference_bn_1_layer_call_fn_45968�$%&'M�J
C�@
:�7
inputs+���������������������������@
p 
� "2�/+���������������������������@�
?__inference_bn_2_layer_call_and_return_conditional_losses_46021�789:M�J
C�@
:�7
inputs+���������������������������@
p
� "?�<
5�2
0+���������������������������@
� �
?__inference_bn_2_layer_call_and_return_conditional_losses_46039�789:M�J
C�@
:�7
inputs+���������������������������@
p 
� "?�<
5�2
0+���������������������������@
� �
$__inference_bn_2_layer_call_fn_46052�789:M�J
C�@
:�7
inputs+���������������������������@
p
� "2�/+���������������������������@�
$__inference_bn_2_layer_call_fn_46065�789:M�J
C�@
:�7
inputs+���������������������������@
p 
� "2�/+���������������������������@�
?__inference_bn_3_layer_call_and_return_conditional_losses_46118�JKLMM�J
C�@
:�7
inputs+���������������������������@
p
� "?�<
5�2
0+���������������������������@
� �
?__inference_bn_3_layer_call_and_return_conditional_losses_46136�JKLMM�J
C�@
:�7
inputs+���������������������������@
p 
� "?�<
5�2
0+���������������������������@
� �
$__inference_bn_3_layer_call_fn_46149�JKLMM�J
C�@
:�7
inputs+���������������������������@
p
� "2�/+���������������������������@�
$__inference_bn_3_layer_call_fn_46162�JKLMM�J
C�@
:�7
inputs+���������������������������@
p 
� "2�/+���������������������������@�
K__inference_conv_transpose_1_layer_call_and_return_conditional_losses_44238�I�F
?�<
:�7
inputs+���������������������������@
� "?�<
5�2
0+���������������������������@
� �
0__inference_conv_transpose_1_layer_call_fn_44248�I�F
?�<
:�7
inputs+���������������������������@
� "2�/+���������������������������@�
K__inference_conv_transpose_2_layer_call_and_return_conditional_losses_44408�01I�F
?�<
:�7
inputs+���������������������������@
� "?�<
5�2
0+���������������������������@
� �
0__inference_conv_transpose_2_layer_call_fn_44418�01I�F
?�<
:�7
inputs+���������������������������@
� "2�/+���������������������������@�
K__inference_conv_transpose_3_layer_call_and_return_conditional_losses_44578�CDI�F
?�<
:�7
inputs+���������������������������@
� "?�<
5�2
0+���������������������������@
� �
0__inference_conv_transpose_3_layer_call_fn_44588�CDI�F
?�<
:�7
inputs+���������������������������@
� "2�/+���������������������������@�
K__inference_conv_transpose_4_layer_call_and_return_conditional_losses_44749�VWI�F
?�<
:�7
inputs+���������������������������@
� "?�<
5�2
0+���������������������������
� �
0__inference_conv_transpose_4_layer_call_fn_44759�VWI�F
?�<
:�7
inputs+���������������������������@
� "2�/+����������������������������
B__inference_dense_1_layer_call_and_return_conditional_losses_45853]/�,
%�"
 �
inputs���������
� "&�#
�
0����������
� {
'__inference_dense_1_layer_call_fn_45862P/�,
%�"
 �
inputs���������
� "������������
B__inference_lrelu_1_layer_call_and_return_conditional_losses_45973�I�F
?�<
:�7
inputs+���������������������������@
� "?�<
5�2
0+���������������������������@
� �
'__inference_lrelu_1_layer_call_fn_45978I�F
?�<
:�7
inputs+���������������������������@
� "2�/+���������������������������@�
B__inference_lrelu_2_layer_call_and_return_conditional_losses_46070�I�F
?�<
:�7
inputs+���������������������������@
� "?�<
5�2
0+���������������������������@
� �
'__inference_lrelu_2_layer_call_fn_46075I�F
?�<
:�7
inputs+���������������������������@
� "2�/+���������������������������@�
B__inference_lrelu_3_layer_call_and_return_conditional_losses_46167�I�F
?�<
:�7
inputs+���������������������������@
� "?�<
5�2
0+���������������������������@
� �
'__inference_lrelu_3_layer_call_fn_46172I�F
?�<
:�7
inputs+���������������������������@
� "2�/+���������������������������@�
#__inference_signature_wrapper_45342�$%&'01789:CDJKLMVWG�D
� 
=�:
8
decoder_input'�$
decoder_input���������"K�H
F
conv_transpose_42�/
conv_transpose_4���������