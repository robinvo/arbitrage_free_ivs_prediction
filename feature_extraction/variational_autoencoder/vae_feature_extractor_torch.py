
import tensorflow as tf
from tensorflow import keras
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils
import torch.distributions
import torchvision
import numpy as np



class Decoder(nn.Module):

    def __init__(self, latent_dims: int, out_dims: tuple) -> None:
        super(Decoder, self).__init__()
        self.out_dims = out_dims
        self.linear1a = nn.Linear(latent_dims, 512)
        self.linear1b = nn.Linear(512, 512)
        self.linear1c = nn.Linear(512, 512)
        self.linear2 = nn.Linear(512, out_dims[0]*out_dims[1])

        torch.nn.init.xavier_uniform_(self.linear1a.weight)
        torch.nn.init.xavier_uniform_(self.linear1b.weight)
        torch.nn.init.xavier_uniform_(self.linear1c.weight)
        torch.nn.init.xavier_uniform_(self.linear2.weight)

    def forward(self, z):
        #z = F.normalize(z)
        z = torch.tanh(self.linear1a(z))
        #z = F.normalize(z)
        z = torch.tanh(self.linear1b(z))
        #z = F.normalize(z)
        z = torch.tanh(self.linear1c(z))
        #z = F.normalize(z)
        z = torch.tanh(self.linear2(z))
        return z.reshape((-1, self.out_dims[0], self.out_dims[1], 1))


class Encoder(nn.Module):

    def __init__(self, latent_dims: int, in_dims: tuple) -> None:
        super(Encoder, self).__init__()
        self.linear1a = nn.Linear(in_dims[0]*in_dims[1], 512)
        self.linear1b = nn.Linear(512, 512)
        self.linear1c = nn.Linear(512, 512)
        self.linear2 = nn.Linear(512, latent_dims)
        self.linear3 = nn.Linear(512, latent_dims)

        torch.nn.init.xavier_uniform_(self.linear1a.weight)
        torch.nn.init.xavier_uniform_(self.linear1b.weight)
        torch.nn.init.xavier_uniform_(self.linear1c.weight)
        torch.nn.init.xavier_uniform_(self.linear2.weight)
        torch.nn.init.xavier_uniform_(self.linear3.weight)

        self.N = torch.distributions.Normal(0, 1)
        #self.N.loc = self.N.loc.cuda() # hack to get sampling on the GPU
        #self.N.scale = self.N.scale.cuda()
        self.kl = 0

    def forward(self, x):
        x = torch.flatten(x, start_dim=1)
        #x = F.normalize(x)
        x = torch.tanh(self.linear1a(x))
        #x = torch.normalize(x)
        x = torch.tanh(self.linear1b(x))
        #x = F.normalize(x)
        x = torch.tanh(self.linear1c(x))
        #x = F.normalize(x)
        mu =  self.linear2(x)
        sigma = torch.exp(self.linear3(x))
        z = mu + sigma*self.N.sample(mu.shape)
        self.kl = (sigma**2 + mu**2 - torch.log(sigma) - 1/2).sum()

        return z


class Autoencoder(nn.Module):

    def __init__(self, latent_dims, data_dims):
        super(Autoencoder, self).__init__()
        self.encoder = Encoder(latent_dims, data_dims)
        self.decoder = Decoder(latent_dims, data_dims)

    def forward(self, x):
        z = self.encoder(x)
        return self.decoder(z)



class Vae_Feature_Extractor:

    def __init__(self, file_name: str, shape: tuple, params: dict = {}) -> None:

        self.feature_dim = 5 if "feature_dim" not in params else params["feature_dim"]
        self.batch_size = 32 if "batch_size" not in params else params["batch_size"]
        self.beta = 0.1 if "beta" not in params else params["beta"]

        self.set_data_from_file(file_name, shape)

        self.autoencoder = Autoencoder(self.feature_dim, (self.num_ttms, self.num_strikes)).to("cpu")

        self.optimizer = torch.optim.Adagrad(self.autoencoder.parameters()) if "optimizer" not in params else params["optimizer"]
        
        self.model_is_trained = False
        self.features_produced = False
        
   
    def set_data_from_file(self, file_name: str, shape: tuple) -> None:

        self.num_datapoints = shape[0]
        self.num_ttms = shape[1]
        self.num_strikes = shape[2]
        
        data_ = np.genfromtxt(file_name, delimiter=',').reshape(shape).astype('float32')

        self.data = torch.utils.data.DataLoader(data_, batch_size=self.batch_size, shuffle=True)
        
        self.is_data_set = True


    def train(self, epochs):
        opt = self.optimizer
        history = []
        for epoch in range(epochs):
            running_loss = 0.0
            running_kl_loss = 0.0
            running_rec_loss = 0.0

            for x in self.data:
                x = x.to("cpu") 
                opt.zero_grad()
                x_hat = self.autoencoder(x)
                rec_loss = ((x - x_hat)**2).sum()
                kl_loss = self.autoencoder.encoder.kl

                loss = rec_loss + self.beta * kl_loss

                loss.backward()
                opt.step()

                running_loss += loss.item()
                running_kl_loss += kl_loss.item()
                running_rec_loss += rec_loss.item()

            running_loss /= self.num_datapoints
            running_kl_loss /= self.num_datapoints
            running_rec_loss /= self.num_datapoints
            
            history.append([running_rec_loss, running_kl_loss, running_rec_loss+running_kl_loss])
            print("Epoch:", epoch, "Reconstruction Loss:", running_rec_loss,  "KL Loss:",running_kl_loss, "Total Loss:", running_rec_loss+running_kl_loss)

        self.model_is_trained = True
        
        return history


    def save_model(self, filename: str) -> None:
        if self.model_is_trained:
            torch.save(self.autoencoder, "models/" + filename)
        else: 
            print("Model needs to be trained first!")

        return 

    
    def load_model(self, filename: str) -> None:
        self.autoencoder = torch.load("models/" + filename)
        self.autoencoder.eval()

        return


    def produce_features(self, data: np.array) -> np.array:
        """data should have dimension (,self.enc_input_shape)."""
        num_data_points = data.shape[0]

        self.produced_features = np.zeros((num_data_points,self.feature_dim))

        z = self.autoencoder.encoder(torch.from_numpy(data))

        self.produced_features = z.detach().numpy()
        self.features_produced = True

        return self.produced_features


    def save_produced_features(self, filename_and_path: str) -> None:

        if not self.features_produced:
            print("Produce features first by calling produce_features.")
            return

        filename_and_path += "_vae_model"

        f = open(filename_and_path,'ab')

        np.savetxt(f, self.produced_features, delimiter=',')

        f.close() 

        return 


    def create_iv_grids_from_features(self, feature_data: np.array) -> np.array:
        
        iv_grids = self.autoencoder.decoder(torch.from_numpy(feature_data).float())

        return iv_grids.detach().numpy()

