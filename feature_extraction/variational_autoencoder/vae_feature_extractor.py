import numpy as	np
import tensorflow as tf
from tensorflow import keras


class Vae_Feature_Extractor:

    def __init__(self, params: dict = {}) -> None:

        self.optimizer = keras.optimizers.Adam() if "optimizer" not in params else params["optimizer"]
        self.feature_dim = 5 if "feature_dim" not in params else params["feature_dim"]
        self.batch_size = 10 if "batch_size" not in params else params["batch_size"]

        self.model_is_trained = False
        self.is_data_set = False
        self.features_produced = False
        
   
    def set_data_from_file(self, file_name: str, shape: tuple) -> None:

        self.num_datapoints = shape[0]
        self.num_ttms = shape[1]
        self.num_strikes = shape[2]
        

        self.data = np.genfromtxt(file_name, delimiter=',').reshape(shape).astype('float32')
        self.enc_input_shape = self.data.shape[1:]

        self.data = tf.data.Dataset.from_tensor_slices(self.data).batch(self.batch_size).shuffle(True)

        self.enc = self.encoder(self.enc_input_shape)
        self.dec = self.decoder(self.feature_dim)

        self.vae = VAE(self.enc, self.dec)

        self.vae.compile(optimizer=self.optimizer)
        
        self.is_data_set = True


    """
    def loss_func(self, encoder_mu, encoder_log_variance):

        def vae_reconstruction_loss(y_true, y_predict):
            reconstruction_loss_factor = 1000
            reconstruction_loss = keras.backend.mean(keras.backend.square(y_true-y_predict), axis=[1, 2, 3])
            
            return reconstruction_loss_factor * reconstruction_loss

        def vae_kl_loss(y_true, y_predict):
            kl_loss = -0.5 * keras.backend.sum(1.0 + encoder_log_variance - keras.backend.square(encoder_mu) - keras.backend.exp(encoder_log_variance), axis=1)
            
            return kl_loss

        def vae_loss(y_true, y_predict):
            reconstruction_loss = vae_reconstruction_loss(y_true, y_predict)
            kl_loss = vae_kl_loss(encoder_mu, encoder_log_variance)

            loss = reconstruction_loss + kl_loss

            return loss

        return vae_loss
    
    
    def mse_loss(self, y_true: np.ndarray, y_pred: np.ndarray) -> float:

        r_loss = keras.backend.mean(keras.square(y_true - y_pred), axis = 1)

        return r_loss


    def kl_loss(self, mean: np.ndarray, log_var: np.ndarray) -> float:

        kl_loss =  -0.5 * keras.backend.sum(1 + log_var - keras.backend.square(mean) - keras.backend.exp(log_var), axis = 1)

        return kl_loss 


    def vae_loss(self, y_true: np.ndarray, y_pred: np.ndarray, mean: np.ndarray, log_var: np.ndarray) -> float:

        r_loss = self.mse_loss(y_true, y_pred)

        kl_loss = self.kl_loss(mean, log_var)

        return  r_loss + kl_loss
    """

    def encoder(self, input_encoder) -> keras.Model:

        self.input_encoder = input_encoder 
        enc_inputs = keras.Input(shape=self.input_encoder, name='encoder_input')
        #enc_inputs = input_encoder
        #enc_inputs = keras.layers.BatchNormalization(name='bn_0')(enc_inputs)

        # Block-1
        x = keras.layers.Conv2D(64, kernel_size=3, strides= 1, padding='same', name='conv_1')(enc_inputs)
        x = keras.layers.BatchNormalization(name='bn_1')(x)
        x = keras.layers.LeakyReLU(name='lrelu_1')(x)
        #x = keras.layers.ELU(name="elu_1")(x)
        
        # Block-2
        x = keras.layers.Conv2D(64, kernel_size=3, strides= 2, padding='same', name='conv_2')(x)
        x = keras.layers.BatchNormalization(name='bn_2')(x)
        x = keras.layers.LeakyReLU(name='lrelu_2')(x)
        #x = keras.layers.ELU(name="elu_2")(x)
        
        # Block-3
        x = keras.layers.Conv2D(64, 3, 2, padding='same', name='conv_3')(x)
        x = keras.layers.BatchNormalization(name='bn_3')(x)
        x = keras.layers.LeakyReLU(name='lrelu_3')(x)
        #x = keras.layers.ELU(name="elu_3")(x)

    
        # Block-4
        x = keras.layers.Conv2D(64, 3, 1, padding='same', name='conv_4')(x)
        x = keras.layers.BatchNormalization(name='bn_4')(x)
        x = keras.layers.LeakyReLU(name='lrelu_4')(x)
        #x = keras.layers.ELU(name="elu_4")(x)

        # Final Block
        x = keras.layers.Flatten()(x)

        flatten = keras.layers.Dense(32, activation="softplus")(x)

        z_mean = keras.layers.Dense(self.feature_dim, name="z_mean")(flatten)
        z_log_var = keras.layers.Dense(self.feature_dim, name="z_log_var")(flatten)
        z = Sampling()([z_mean, z_log_var]) 
        
        model = keras.Model(enc_inputs, [z_mean, z_log_var, z], name="Encoder")

        #return [enc_inputs, z_mean, z_log_var, z, model]
        return model


    def decoder(self, input_decoder):
    
        dim1 = (((self.num_ttms + 1) // 2) + 1) // 2
        dim2 = (((self.num_strikes + 1) // 2) + 1) // 2

        self.input_encoder = input_decoder 
        dec_inputs = keras.Input(shape=self.input_encoder, name='decoder_input')
        #dec_inputs = input_decoder
        #dec_inputs = keras.layers.BatchNormalization(name='bn_0')(dec_inputs)

        y = keras.layers.Dense(dim1*dim2*64, name='dense_1')(dec_inputs)
        y = keras.layers.Reshape((dim1, dim2, 64), name='Reshape_Layer')(y)
    
        # Block-1
        y = keras.layers.Conv2DTranspose(64, 3, strides= 1, padding='same',name='conv_transpose_1')(y)
        y = keras.layers.BatchNormalization(name='bn_1')(y)
        y = keras.layers.LeakyReLU(name='lrelu_1')(y)
        #y = keras.layers.ELU(name="elu_1")(y)
    
        # Block-2
        y = keras.layers.Conv2DTranspose(64, 3, strides= 2, padding='same', name='conv_transpose_2')(y)
        y = keras.layers.BatchNormalization(name='bn_2')(y)
        y = keras.layers.LeakyReLU(name='lrelu_2')(y)
        #y = keras.layers.ELU(name="elu_2")(y)

        
        # Block-3
        y = keras.layers.Conv2DTranspose(64, 3, 2, padding='same', name='conv_transpose_3')(y)
        y = keras.layers.BatchNormalization(name='bn_3')(y)
        y = keras.layers.LeakyReLU(name='lrelu_3')(y)
        #y = keras.layers.ELU(name="elu_3")(y)
        
        # Block-4
        outputs = keras.layers.Conv2DTranspose(1, 3, 1, padding='same', activation='softplus', name='conv_transpose_4')(y)

        model = tf.keras.Model(dec_inputs, outputs, name="Decoder")

        return model

    """
    def sampling(self, input_1, input_2):

        mean = keras.Input(shape=input_1, name='input_layer1')
        log_var = keras.Input(shape=input_2, name='input_layer2')
        out = keras.layers.Lambda(self.sampling_reparameterization, name='encoder_output')([mean, log_var])
        enc_2 = tf.keras.Model([mean,log_var], out,  name="Encoder_2")

        return enc_2


    def sampling_reparameterization(self, distribution_params: tuple) -> np.ndarray:

        mean, log_var = distribution_params
        epsilon = keras.random_normal(shape = keras.shape(mean), mean = 0., stddev = 1.)
        z = mean + keras.exp(log_var / 2) * epsilon

        return z


    @tf.function
    def train_step(self, data_batch):

        with tf.GradientTape() as encoder, tf.GradientTape() as decoder:
        
            mean, log_var = self.enc(data_batch, training=True)
            latent = self.sampling(mean, log_var)
            generated_ivs = self.dec(latent, training=True)
            loss = self.vae_loss(data_batch, generated_ivs, mean, log_var)
 
        gradients_of_enc = encoder.gradient(loss, self.enc.trainable_variables)
        gradients_of_dec = decoder.gradient(loss, self.dec.trainable_variables)
        
        self.optimizer.apply_gradients(zip(gradients_of_enc, self.enc.trainable_variables))
        self.optimizer.apply_gradients(zip(gradients_of_dec, self.dec.trainable_variables))

        return loss
    """

    def train(self, epochs: int):

        if not self.is_data_set:
            print("Set training data first")

            return 

        self.history = self.vae.fit(self.data, epochs=epochs)

        self.model_is_trained = True

        return self.history


    def save_model(self, filename: str) -> None:
        if self.model_is_trained:
            self.enc.save("models/" + filename + "_enc")
            self.dec.save("models/" + filename + "_dec")
        else: 
            print("Model needs to be trained first!")

    
    def load_model(self, filename: str) -> None:
        self.enc = keras.models.load_model("models/" + filename + "_enc")
        self.dec = keras.models.load_model("models/" + filename + "_dec")


    def produce_features(self, data: np.array) -> np.array:
        """data should have dimension (,self.enc_input_shape)."""
        num_data_points = data.shape[0]

        self.produced_features = np.zeros((num_data_points,self.feature_dim))

        if data.shape[1:] != self.enc_input_shape:
            print("data has wrong shape. Should have shape", self.feature_dim, "but has", data.shape[1:])
            return self.produced_features

        z_mean, _, z = self.enc.predict(data)

        self.produced_features = np.array(z)
        self.features_produced = True

        return self.produced_features


    def save_produced_features(self, filename_and_path: str) -> None:

        if not self.features_produced:
            print("Produce features first by calling produce_features.")
            return

        filename_and_path += "_vae_model"

        f = open(filename_and_path,'ab')

        np.savetxt(f, self.produced_features, delimiter=',')

        f.close() 

        return 


    def create_iv_grids_from_features(self, feature_data: np.array) -> np.array:
        
        iv_grids = self.dec.predict(feature_data)

        return np.array(iv_grids)



class VAE(keras.Model):

    def __init__(self, encoder, decoder, **kwargs):
        super(VAE, self).__init__(**kwargs)
        self.encoder = encoder
        self.decoder = decoder

        self.encoder.summary()
        self.decoder.summary()

        self.total_loss_tracker = keras.metrics.Mean(name="total_loss")
        self.reconstruction_loss_tracker = keras.metrics.Mean(
            name="reconstruction_loss"
        )
        self.kl_loss_tracker = keras.metrics.Mean(name="kl_loss")

    @property
    def metrics(self):
        return [
            self.total_loss_tracker,
            self.reconstruction_loss_tracker,
            self.kl_loss_tracker,
        ]


    def train_step(self, data):

        with tf.GradientTape() as tape:
            z_mean, z_log_var, z = self.encoder(data)
            reconstruction = self.decoder(z)
            #reconstruction_loss = tf.reduce_mean(tf.reduce_sum(tf.reduce_sum(keras.losses.mean_squared_error(data, reconstruction), axis=0), axis=0))
            reconstruction_loss = tf.reduce_mean(
                tf.reduce_sum(
                    keras.losses.mean_squared_error(data, reconstruction), axis=(1, 2)
                )
            )
            kl_loss = -0.5 * (1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var))
            kl_loss = tf.reduce_mean(tf.reduce_sum(kl_loss, axis=1))

            total_loss =  reconstruction_loss + kl_loss

        grads = tape.gradient(total_loss, self.trainable_weights)

        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))

        self.total_loss_tracker.update_state(total_loss)
        self.reconstruction_loss_tracker.update_state(reconstruction_loss)
        self.kl_loss_tracker.update_state(kl_loss)

        return {
            "loss": self.total_loss_tracker.result(),
            "reconstruction_loss": self.reconstruction_loss_tracker.result(),
            "kl_loss": self.kl_loss_tracker.result(),
        }



class Sampling(tf.keras.layers.Layer):
    """Uses (z_mean, z_log_var) to sample z."""

    def call(self, inputs):

        z_mean, z_log_var = inputs
        batch = tf.shape(z_mean)[0]
        dim = tf.shape(z_mean)[1]
        epsilon = tf.keras.backend.random_normal(shape=(batch, dim))

        return z_mean + tf.exp(0.5 * z_log_var) * epsilon














