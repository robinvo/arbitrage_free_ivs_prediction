import numpy as np
from scipy.stats import norm


def black_scholes_call(S, K, tau, r, sigma):
    '''
    :param S: Asset price
    :param K: Strike price
    :param tau: Time to maturity
    :param r: risk-free rate 
    :param sigma: volatility

    :return: call price
    '''

    d1 = (np.log(S / K) + (r + sigma ** 2 / 2) * tau) / (sigma * tau**0.5)
    d2 = d1 - sigma * tau**0.5


    call = S * norm.cdf(d1) -  norm.cdf(d2)* K * np.exp(-r * tau)

    return call


def iv_grids_to_call_grids(iv_data, ttms, strikes):
    '''
    :param iv_data: iv data with shape (num_data_points, num_ttms, num_stirkes)

    :return: call price grids with shape (num_data_points, num_ttms, num_stirkes)
    '''
    call_data = np.zeros(iv_data.shape)

    num_strikes = strikes.shape[0]
    num_ttms = ttms.shape[0]

    strikes = np.tile(np.array([strikes]), (num_ttms,1))
    ttms = np.tile(np.array([ttms]), (num_strikes,1)).T

    for i in range(iv_data.shape[0]):
        call_data[i,:,:] = black_scholes_call(S=1., K=strikes, tau=ttms, r=0., sigma=iv_data[i,:,:])
    return call_data