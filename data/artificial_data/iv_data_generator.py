from typing import Callable
import numpy as np

from scipy.stats import norm, uniform

class ImpliedVolatilityDataGenerator:

    sabr_parameters = np.array([0.6, 0.7, -0.6, 1., 0.1]) 
    noise = 0.005

    def __init__(self, strike_grid: np.array , ttm_grid: np.array) -> None:

        self.ttm_grid = ttm_grid
        self.strike_grid = strike_grid

        self.num_ttms = ttm_grid.size
        self.num_strikes = strike_grid.size

        self.data = []

        
    def generate(self, num_points: int, method: str= "SABR") -> np.ndarray:

        self.method = method

        if method == "SABR":
            params = self.sabr_parameters
        elif method == "Heston":
            pass

        data = np.zeros((num_points,self.num_strikes*self.num_ttms))

        for i in range(num_points):
            theta = params + norm.rvs(size=params.shape , scale=self.noise)

            data[i,:] = self.implied_vol_grid(theta, method).flatten()

        self.data = data

        return self.data


    def generate_with_moving_params(self, num_points: int, method: str= "SABR") -> np.ndarray:

        self.method = method

        if method == "SABR":
            params = self.sabr_parameters

            data = np.zeros((num_points,self.num_strikes*self.num_ttms))
            #theta = params
            for i in range(num_points):

                theta = params + norm.rvs(size=params.shape , scale=self.noise)
                #theta = params

                #theta[0:2] += uniform.rvs(size=2, scale=0.1)

                #theta[0] += 0.2 * i/num_points # change alpha from 0.06+noise to 0.1+noise in the end
                #theta[1] += 0.2 * i/num_points # change beta from 0.4+noise to 0.6+noise in the end

                theta[0] -= 0.1 * np.cos(i/200) + 0.05 * np.cos(i/400) #+ norm.rvs(size=1 , scale=self.noise)[0]
                theta[1] -= 0.1 * np.cos(i/400) #+ norm.rvs(size=1 , scale=self.noise)[0]

                #theta[0:3] += norm.rvs(loc=[0.00002*np.sin(i/60), 0.00015*np.sin(i/60), 0], scale=self.noise)

                data[i,:] = self.implied_vol_grid(theta, method).flatten()

        elif method == "Heston":
            pass

        self.data = data

        return self.data


    def save_to_csv(self, filename_and_path: str) -> None:

        f = open(filename_and_path,'ab')

        np.savetxt(f, self.data, delimiter=',')

        f.close() 

        return


    def corr_brownian_motion(self, n: int, T: float, dim: int, rho: float):

        dt = T/n

        dW1 = norm.rvs(size=(dim,n+1) , scale=np.sqrt(dt))
        dW2 = rho * dW1 + np.sqrt(1 - np.power(rho ,2)) * norm.rvs(size=(dim,n+1) , scale=np.sqrt(dt))
            
        W1 = np.cumsum(dW1, axis=-1)
        W2 = np.cumsum(dW2, axis=-1)
    
        return W1, W2


    def euler_maruyama(self, mu: Callable, sigma: Callable, T: float, x0: float, W: np.ndarray) -> np.ndarray:

        dim = W.shape[0]
        n = W.shape[1]-1
        Y = np.zeros((dim,n+1))
        dt = T/n
        sqrt_dt = np.sqrt(dt)
        Y[:,0] = x0
        
        for i in range(n):
            Y[:,i+1] = Y[:,i] + np.multiply(mu(Y[:,i]),dt) + sigma(Y[:,i],i)*sqrt_dt*(W[:,i+1]-W[:,i])
        
        return Y


    def sabr_iv(self, alpha: float, beta: float, rho: float, tau: float, K: float, S0: float, V0: float) -> float:
        """
        Args:
        alpha: Float greater zero.
        beta: Float between zero and one.
        rho: Float in [-1, 1].
        tau: Time to maturity.
        K: Strike.
        S0 and V0: Initial stock price and initial volatility.

        Returns:
        Implied volatility for given parameters calculated using SABR closed form formula. 
        """

        #zeta = alpha/(V0*(1-beta))*(np.power(S0,1-beta)-np.power(K,1-beta))
        #S_mid = (S0 + K)/2
        #gamma1 = beta/S_mid
        #gamma2 = -beta*(1-beta)/S_mid/S_mid
        #D = np.log((np.sqrt(1-2*rho*zeta+zeta*zeta)+zeta-rho)/(1-rho))
        #eps = tau*alpha*alpha
        
        #t1 = (2*gamma2-gamma1*gamma1+1/S_mid/S_mid)/24*np.power((V0*S_mid**beta/alpha),2)
        #t2 = rho*gamma1/4*V0*np.power(S_mid,beta)/alpha
        #t3 = (2-3*rho*rho)/24
        #tmp = t1+t2+t3

        #return alpha*(S0-K)/D*(1+eps*tmp)

        z = alpha/V0 * (S0*K)**((1-beta)/2) * np.log(S0/K)

        x = np.log((np.sqrt(1-2*rho*z+z**2)+z-rho)/(1-rho))

        u1 = (1-beta)**2 * V0**2 / (24 * (S0*K)**(1-beta)) 
        u2 = rho * beta * V0 * alpha / (4 * (S0*K)**((1-beta)/2))
        u3 = (2 - 3 * rho**2) * alpha**2 / 24

        u = 1 + tau * (u1 + u2 + u3)

        d1 = (1-beta)**2 * np.log(S0/K)**2 / 24
        d2 = (1-beta)**4 * np.log(S0/K)**4 / 1920

        d = (S0*K)**((1-beta)/2) * (1 + d1 + d2)

        iv = V0 * z / x * u / d

        return iv


    def heston_iv() -> float:
        pass


    def implied_vol_grid(self, theta: np.array, method: str) -> np.ndarray:
        """
        Args:
        theta: 
        Numpy array giving parameters for the relevant method to generate implied volatility. 
        For "SABR": [alpha, beta, rho, S0, V0]

        Returns:
        Implied volatility grid for given parameters theta, ttms, and strikes.      
        """

        iv = np.zeros((self.num_ttms, self.num_strikes))

        if method == "SABR":
            for j in range(self.num_ttms):
                for k in range(self.num_strikes):
                    iv[j,k] = self.sabr_iv(theta[0],theta[1],theta[2],self.ttm_grid[j],self.strike_grid[k],theta[3],theta[4])

        elif method == "Heston":
            pass

        return iv

