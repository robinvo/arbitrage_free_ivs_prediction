import numpy as np
from tensorflow import keras
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense


class Lstm_prediction_model:

    def __init__(self, data: np.ndarray, params: dict = {}) -> None:

        self.data = data

        self.scaler = MinMaxScaler(feature_range=(0, 1))
        self.scaled_data = self.scaler.fit_transform(data)
        
        self.feature_dim = 5 if "feature_dim" not in params else params["feature_dim"]
        self.look_back = 5 if "look_back" not in params else params["look_back"]

        self.lstm_input_shape = (self.look_back, self.feature_dim)
        

    def initialize_model(self) -> None:
        self.lstm = Sequential()
        self.lstm.add(LSTM(self.feature_dim, input_shape=self.lstm_input_shape))
        self.lstm.add(Dense(self.feature_dim))

        self.lstm.compile(loss='mean_squared_error', optimizer='adam')
        
        self.lstm.summary()

        return


    def create_dataset(self) -> None:
        dataX, dataY = [], []

        for i in range(len(self.scaled_data)-self.look_back):
            a = self.scaled_data[i:i+self.look_back, :]
            dataX.append(a)
            dataY.append(self.scaled_data[i + self.look_back, :])
        self.trainX = np.array(dataX)
        self.trainY = np.array(dataY)

        print(self.trainX.shape, self.trainY.shape)

        return

    
    def create_advanced_dataset(self) -> None:
        dataX, dataY = [], []

        for i in range(len(self.scaled_data)-self.look_back-1):
            Z_star_1 = np.mean(self.scaled_data[i:i+self.look_back, :], axis=0)
            Z_star_2 = np.mean(self.scaled_data[i:i+int(self.look_back), :], axis=0)
            Z_star_3 = self.scaled_data[i+self.look_back-1, :]

            dataX.append([Z_star_1,Z_star_2,Z_star_3])
            dataY.append(self.scaled_data[i+self.look_back, :])

        self.trainX = np.array(dataX)
        self.trainY = np.array(dataY)

        print(self.trainX.shape, self.trainY.shape)

        return


    def train_model(self, epochs: int, batch_size: int = 2) -> None:

        self.lstm.fit(self.trainX, self.trainY, epochs=epochs, batch_size=batch_size, verbose=2)

        return

    
    def predict(self, data: np.ndarray) -> np.ndarray:
        for i in range(data.shape[0]):
            data[i] = self.scaler.transform(data[i])

        return self.scaler.inverse_transform(self.lstm.predict(data))


    def save_lstm_model(self, name: str = "lstm_model") -> None:
        model_json = self.lstm.to_json()
        with open(name+".json", "w") as json_file:
            json_file.write(model_json)
        self.lstm.save_weights(name+".h5")

        return

    def load_lstm_model(self, name: str) -> None:
        json_file = open(name+".json", 'r')
        loaded_model_json = json_file.read()
        json_file.close()

        self.lstm = keras.models.model_from_json(loaded_model_json)
        self.lstm.load_weights(name+".h5")
        self.lstm.compile(loss='mean_squared_error', optimizer='adam')

        return




