import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def plot_features_with_prediction(
    data: np.ndarray, 
    prediction: np.ndarray, 
    true_predictions: np.ndarray = None,
    use_true_predictions: bool = False,
    save_path_and_name: str = "",
    params: dict = {}
) -> plt.figure:
    """
    data.shape = (num_time_points, num_features) 
    datpredictiona.shape = (num_prediction_time_points, num_features) 
    true_predictions.shape = (num_prediction_time_points, num_features) or None
    """
    num_time_points = data.shape[0]
    num_features = data.shape[1]
    num_prediction_time_points = prediction.shape[0]

    fig_title = "Prediction with Transformer" if "fig_title" not in params else params["fig_title"]
    share_y = True if "share_y" not in params else params["share_y"]

    #x_1 = [i for i in range(num_time_points)]
    #x_2 = [i+num_time_points for i in range(num_prediction_time_points)]

    factor = 3 if use_true_predictions else 2

    data_total = np.zeros((num_time_points+num_prediction_time_points, factor*num_features))
    data_total[:,:] = np.nan

    for i in range(num_features):
        data_total[:num_time_points,factor*i] = data[:,i]
        #data_total[:num_time_points,factor*i+1] = data[:,i]
        data_total[num_time_points:,factor*i+1] = prediction[:,i]
        if use_true_predictions:
            data_total[num_time_points:,factor*i+1] = true_predictions[:,i]
            data_total[num_time_points:,factor*i+2] = prediction[:,i]

    col_names = ["Train","True Predict","Predict"]*num_features if use_true_predictions else ["Train","Predict"]*num_features

    df = pd.DataFrame(data_total, columns=col_names)
    
    fig, axs = plt.subplots(1, num_features, figsize=(20,5), sharey=share_y)

    fig.suptitle(fig_title, fontsize=14)
    
    #curr = 0
    #for ax in axs.flat:
    for i in range(num_features):
        df.iloc[:, int(factor*i):int(factor*(i+1))].plot(ax=axs[i])
        #ax.plot(x_1, data[:,curr], 'tab:blue')  
        #ax.plot(x_2, prediction[:,curr], 'tab:red')
        #ax.plot(x_2, true_predictions[:,curr], 'tab:green')

        axs[i].set_title('Feature {}'.format(i+1))
        axs[i].set(xlabel='Timestep')
        #axs[i].label_outer()
        #axs[i].set_ylim(np.nanmin(data_total[:,i])-0.01, np.nanmax(data_total[:,i])+0.01)
        axs[i].grid()
        #curr += 1

    if save_path_and_name != "":
        fig.savefig(save_path_and_name, bbox_inches='tight')

    return fig


def plot_rmse(mat1: np.array, mat2: np.array) -> plt.figure:
    """
    mat1.shape=mat2.shape= (num_points, num_features)
    """

    rmse = np.sqrt(np.mean((mat1-mat2)**2, axis=0))
    fig = plt.figure()
    
    ax = fig.add_subplot(111)

    rmse = np.zeros()

    for i in range(mat1.shape[0]):
        pass

    return