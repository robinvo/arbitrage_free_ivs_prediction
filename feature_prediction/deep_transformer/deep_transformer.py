import torch.nn as nn
import torch
from torch.utils.data import Dataset
import numpy as np
import pandas as pd
import os
from sklearn.preprocessing import MinMaxScaler
from tensorflow import keras
from scipy.stats import norm


"""
The architecture is based on the paper “Attention Is All You Need”. 
Ashish Vaswani, Noam Shazeer, Niki Parmar, Jakob Uszkoreit, Llion Jones, Aidan N Gomez, Lukasz Kaiser, and Illia Polosukhin. 2017.
"""

class Transformer(nn.Module):
    # d_model : number of features
    def __init__(self, feature_size: int = 6, extra_feature_size: int = 6, num_layers: int = 3, dropout: float = 0.0) -> None:
        super(Transformer, self).__init__()

        self.out_dim = feature_size
        self.d_model = feature_size + extra_feature_size

        self.encoder_layer = nn.TransformerEncoderLayer(d_model=self.d_model, nhead=int(self.d_model), dropout=dropout)
        self.transformer_encoder = nn.TransformerEncoder(self.encoder_layer, num_layers=num_layers)     

        self.decoder = nn.Linear(in_features=self.d_model, out_features=self.out_dim)

        self.init_weights()


    def init_weights(self) -> None:
        initrange = 0.1    
        self.decoder.bias.data.zero_()
        self.decoder.weight.data.uniform_(-initrange, initrange)

        return


    def _generate_square_subsequent_mask(self, sz: int):
        mask = (torch.triu(torch.ones(sz, sz)) == 1).transpose(0, 1)
        mask = mask.float().masked_fill(mask == 0, float('-inf')).masked_fill(mask == 1, float(0.0))

        return mask


    def forward(self, src, device):
        
        mask = self._generate_square_subsequent_mask(len(src)).to(device)
        output = self.transformer_encoder(src, mask)
        output = self.decoder(output)

        return output



class Transformer_prediction_model:

    def __init__(
        self, 
        data: np.ndarray, 
        params: dict = {}, 
        decoder: keras.Model = None, 
        call_prices: np.ndarray = None, 
        strikes: np.ndarray = None, 
        ttms: np.ndarray = None
    ) -> None:

        self.num_data_points = data.shape[0]
        self.num_features = data.shape[1]

        self.look_back = 10 if "look_back" not in params else params["look_back"]
        self.forecast_window = 5 if "forecast_window" not in params else params["forecast_window"]
        self.beta_NDA = 0.02 if "beta_NDA" not in params else params["beta_NDA"]

        #self.scaler = MinMaxScaler()

        self.data = data

        if decoder is not None:
            self.vae_dec = decoder
            self.call_prices = call_prices
            self.strikes = strikes
            self.ttms = ttms


        self.training_data = self.create_data_set_with_extra_features(data)


    def create_data_set_with_extra_features(self, data: np.ndarray, is_train: bool = True) -> np.ndarray:
        """
        Adds sin, cos features to the dataset
        
        Args: 
            data: array of shape (num_data_points, num_features)

        Returns:
            data_: scaled array of shape (num_data_points, num_features + num_extra_features)
        
        """
        sin_cos_features = np.array([[np.cos(i/100),np.cos(i/100),np.sin(i/200),np.sin(i/200)] for i in range(data.shape[0])])
        self.num_extra_features = sin_cos_features.shape[1]

        data_ = np.zeros((data.shape[0], self.num_features+self.num_extra_features))

        if is_train:
            #data_[:,:self.num_features] = self.scaler.fit_transform(data) 
            data_[:,:self.num_features] = data 
        else:
            #data_[:,:self.num_features] = self.scaler.transform(data) 
            data_[:,:self.num_features] = data 

        data_[:,self.num_features:] = sin_cos_features

        print("Data shape with extra features: ", data_.shape)

        return data_


    def transformer(self, epochs: int = 10, path_to_save_model: str = "./", path_to_save_loss: str = "./", device: str = "cpu"):
        """Returns: path to the best model"""

        device = torch.device(device)

        model = Transformer(feature_size=self.num_features, extra_feature_size=self.num_extra_features).double().to(device)

        optimizer = torch.optim.Adam(model.parameters())
        #scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=10)
        criterion = torch.nn.MSELoss()
        best_model = ""
        min_train_loss = float('inf')

        for epoch in range(epochs + 1):

            train_loss = 0.0

            model.train()
            for i in range(self.num_data_points-self.look_back-1):
            
                optimizer.zero_grad()

                # Shape of _input : [batch, input_length, feature]
                # Desired input for model: [input_length, batch, feature]

                #src = _input.permute(1,0,2).double().to(device)[:-1,:,:] # torch.Size([24, 1, 7])
                #target = _input.permute(1,0,2).double().to(device)[1:,:,:] # src shifted by 1.
                src = torch.tensor([self.training_data[i:i+self.look_back]]).permute(1,0,2).to(device)
                target = torch.tensor([self.training_data[i+1:i+self.look_back+1]]).permute(1,0,2).to(device)

                #print(src.size(), target.size())
                prediction = model(src, device) # torch.Size([24, 1, 7])
                #print(prediction.size())
                #print(target[:,:,:self.num_features].size())
                loss = criterion(prediction, target[:,:,:self.num_features])
                loss.backward()
                optimizer.step()
                #scheduler.step(loss.detach().item())
                train_loss += loss.detach().item()

            if train_loss < min_train_loss:
                torch.save(model.state_dict(), path_to_save_model + f"best_train_{epoch}.pth")
                torch.save(optimizer.state_dict(), path_to_save_model + f"optimizer_{epoch}.pth")
                min_train_loss = train_loss
                best_model = f"best_train_{epoch}.pth"

            #if epoch % 1 == 0: # Plot 1-Step Predictions
            #
            #    #logger.info(f"Epoch: {epoch}, Training loss: {train_loss}")
            #   src_humidity = self.scaler.inverse_transform(src[:,:,0].cpu()) #torch.Size([35, 1, 7])
            #    target_humidity = self.scaler.inverse_transform(target[:,:,0].cpu()) #torch.Size([35, 1, 7])
            #    prediction_humidity = self.scaler.inverse_transform(prediction[:,:,0].detach().cpu().numpy()) #torch.Size([35, 1, 7])
            #    #plot_training(epoch, path_to_save_predictions, src_humidity, prediction_humidity, sensor_number, index_in, index_tar)
            
            train_loss /= self.num_data_points
            print("Epoch:", epoch, ", Training Loss:", train_loss)

            #self.log_loss(train_loss, path_to_save_loss, train=True)
            
        #plot_loss(path_to_save_loss, train=True)
        return best_model


    def transformer_no_dynamic_arbitrage_2(self, epochs: int = 10, path_to_save_model: str = "./", path_to_save_loss: str = "./", device: str = "cpu") -> str:
        """Returns: path to the best model"""

        device = torch.device(device)

        model = Transformer(feature_size=self.num_features, extra_feature_size=self.num_extra_features).double().to(device)

        optimizer = torch.optim.Adam(model.parameters(), lr=1e-2)
        criterion = torch.nn.MSELoss()
        best_model = ""
        min_train_loss = float('inf')

        #self.loss_history = np.array([])

        for epoch in range(epochs + 1):

            train_loss = 0.0
            loss1_ = 0.0
            loss2_ = 0.0

            model.train()
            for i in range(self.num_data_points-self.look_back-1):
            
                optimizer.zero_grad()

                src = torch.tensor([self.training_data[i:i+self.look_back]]).permute(1,0,2).to(device)
                target = torch.tensor([self.training_data[i+1:i+self.look_back+1]]).permute(1,0,2).to(device)

                prediction = model(src, device) 

                # create predicted IV grid using decoder
                predicted_IV_grid = self.vae_dec(prediction[-1,:,:].float()) # shape (1, 12, 16, 1)
 
                # create call price grid 
                predicted_call_grid = iv_grids_to_call_grids(iv_data=predicted_IV_grid[:,:,:,0], ttms=self.ttms, strikes=self.strikes)
                true_call_grid = self.call_prices[i+self.look_back+1]


                loss1 = criterion(prediction, target[:,:,:self.num_features])
                loss2 = criterion(predicted_call_grid, torch.from_numpy(np.array([true_call_grid])).type(torch.DoubleTensor))
                loss = loss1 + self.beta_NDA * loss2
                loss.backward()
                optimizer.step()
            
                train_loss += loss.detach().item()
                loss1_ += loss1.detach().item()
                loss2_ += loss2.detach().item()


            if train_loss < min_train_loss:
                torch.save(model.state_dict(), path_to_save_model + f"best_train_NDA_{epoch}.pth")
                torch.save(optimizer.state_dict(), path_to_save_model + f"optimizer_NDA_{epoch}.pth")
                min_train_loss = train_loss
                best_model = f"best_train_NDA_{epoch}.pth"

            
            train_loss /= self.num_data_points
            loss1_ /= self.num_data_points
            loss2_ /= self.num_data_points

            print("Epoch:", epoch, ", Total Loss:", train_loss, ", Call Grid MSE Loss:", loss2_, ", Feature MSE Loss:", loss1_)

            #self.loss_history = np.append(self.loss_history, [train_loss, loss1_, loss2_])
            self.log_loss([loss1_+loss2_, loss1_, loss2_], path_to_save_loss, train=True)
            
        return best_model


    def transformer_no_dynamic_arbitrage(
        self, 
        epochs: int = 10, 
        path_to_save_model: str = "./", 
        path_to_save_loss: str = "./", 
        device: str = "cpu", 
        beta_feature_mse: float = 0.0
    ) -> str:
        """Returns: path to the best model"""

        device = torch.device(device)

        model = Transformer(feature_size=self.num_features, extra_feature_size=self.num_extra_features).double().to(device)

        optimizer = torch.optim.Adagrad(model.parameters())
        criterion = torch.nn.MSELoss()
        best_model = ""
        min_train_loss = float('inf')

        #self.loss_history = np.array([])

        for epoch in range(epochs + 1):

            train_loss = 0.0
            loss_featrue_mse = 0.0
            loss_call_mse = 0.0

            model.train()
            for i in range(self.num_data_points-self.look_back-1):
            
                optimizer.zero_grad()

                src = torch.tensor([self.training_data[i:i+self.look_back]]).permute(1,0,2).to(device)
                target = torch.tensor([self.training_data[i+1:i+self.look_back+1]]).permute(1,0,2).to(device)

                prediction = model(src, device) 

                # create predicted IV grid using decoder
                predicted_IV_grid = self.vae_dec(prediction.float()) # shape (150, 12, 16, 1)
                # create call price grid 
                predicted_call_grids = iv_grids_to_call_grids(iv_data=predicted_IV_grid[:,:,:,0], ttms=self.ttms, strikes=self.strikes)
                true_call_grids = self.call_prices[i+1:i+self.look_back+1]#.reshape(self.look_back, self.ttms.shape[0], self.strikes.shape[0])


                loss_ = criterion(predicted_call_grids[0], torch.from_numpy(np.array(true_call_grids[0])).type(torch.DoubleTensor))
                for i in range(1,self.look_back):
                    loss_ += criterion(predicted_call_grids[i], torch.from_numpy(np.array(true_call_grids[i])).type(torch.DoubleTensor))
            
                loss_featrue_mse_ = beta_feature_mse * criterion(prediction, target[:,:,:self.num_features])
                loss = loss_ + loss_featrue_mse
                loss.backward()
                optimizer.step()

                train_loss += loss.detach().item()
                loss_featrue_mse += loss_featrue_mse_.detach().item()
                loss_call_mse += loss_.detach().item()
          

            torch.save(model.state_dict(), path_to_save_model + f"best_train_NDA_{epoch}.pth")
            torch.save(optimizer.state_dict(), path_to_save_model + f"optimizer_NDA_{epoch}.pth")
            if train_loss < min_train_loss:
                min_train_loss = train_loss
                best_model = f"best_train_NDA_{epoch}.pth"

            
            train_loss /= self.num_data_points
            loss_featrue_mse /= self.num_data_points
            loss_call_mse /= self.num_data_points

            if beta_feature_mse==0:
                print("Epoch:", epoch, ", Call Grids MSE Loss:", train_loss)
            else: 
                print("Epoch:", epoch, ", Total Loss:", loss_call_mse+loss_featrue_mse/beta_feature_mse, ", Feature MSE Loss:", loss_featrue_mse/beta_feature_mse, ",  Call Price MSE Loss:", loss_call_mse)
            
        return best_model


    def transformer_no_dynamic_arbitrage_retrain_with_new_data(
        self, 
        new_data: np.array,
        trained_model: str, 
        epochs: int = 10, 
        path_to_save_model: str = "./", 
        path_to_save_loss: str = "./", 
        device: str = "cpu",
        beta_feature_mse: float = 0.0
    ):
        """Returns: path to the best model"""

        device = torch.device(device)

        model = Transformer(feature_size=self.num_features, extra_feature_size=self.num_extra_features).double().to(device)
        model.load_state_dict(torch.load(path_to_save_model+trained_model))

        optimizer = torch.optim.Adam(model.parameters())
        criterion = torch.nn.MSELoss()
        best_model = ""
        min_train_loss = float('inf')

        # extra features for new data
        data = self.create_data_set_with_extra_features(new_data, is_train=False)

        in_sample_errors = np.array([])

        for epoch in range(epochs + 1):

            train_loss = 0.0
            loss_featrue_mse = 0.0

            model.train()
            for i in range(new_data.shape[0]-self.look_back-1):
            
                optimizer.zero_grad()

                src = torch.tensor([data[i:i+self.look_back]]).permute(1,0,2).to(device)
                target = torch.tensor([data[i+1:i+self.look_back+1]]).permute(1,0,2).to(device)

                prediction = model(src, device) 

                # create predicted IV grid using decoder
                predicted_IV_grid = self.vae_dec(prediction.float()) # shape (150, 12, 16, 1)
 
                # create call price grid 
                predicted_call_grids = iv_grids_to_call_grids(iv_data=predicted_IV_grid[:,:,:,0], ttms=self.ttms, strikes=self.strikes)
                true_call_grids = self.call_prices[i:i+self.look_back]#.reshape(self.look_back, self.ttms.shape[0], self.strikes.shape[0])

                loss = criterion(predicted_call_grids[0], torch.from_numpy(np.array(true_call_grids[0])).type(torch.DoubleTensor))
                for i in range(1,self.look_back):
                    loss += criterion(predicted_call_grids[i], torch.from_numpy(np.array(true_call_grids[i])).type(torch.DoubleTensor))

                loss_featrue_mse_ = beta_feature_mse * criterion(prediction,  target[:,:,:self.num_features])
                loss += loss_featrue_mse
                loss.backward()
                optimizer.step()

                train_loss += loss.detach().item()
                loss_featrue_mse += loss_featrue_mse_.detach().item()

                if epoch==epochs:
                    in_sample_errors = data[i+self.look_back+1,:self.num_features] - prediction.detach().numpy()[-1]


            if train_loss < min_train_loss:
                torch.save(model.state_dict(), path_to_save_model + f"best_train_NDA_trained_twice_{epoch}.pth")
                torch.save(optimizer.state_dict(), path_to_save_model + f"optimizer_NDA_trained_twice_{epoch}.pth")
                min_train_loss = train_loss
                best_model = f"best_train_NDA_trained_twice_{epoch}.pth"

            
            train_loss /= new_data.shape[0]
            if beta_feature_mse==0:
                print("Epoch:", epoch, ", Call Grids MSE Loss:", train_loss)
            else: 
                print("Epoch:", epoch, ", Total Loss:", train_loss, ", Feature MSE Loss:", loss_featrue_mse)

        return best_model, in_sample_errors


    def in_sample_errors(self, path_to_save_model: str, best_model: str, device: str = "cpu") -> np.array:

        in_sample_predictions = np.zeros((self.num_data_points, self.num_features))
        in_sample_errors = np.zeros((self.num_data_points, self.num_features))

        model = Transformer(feature_size=self.num_features, extra_feature_size=self.num_extra_features).double().to(device)
        model.load_state_dict(torch.load(path_to_save_model+best_model))

        for i in range(self.num_data_points):
            next_input = self.training_data[i:self.look_back+i,:]
            # next_input_model shape: (self.look_back, 1, self.num_features+4)
            next_input_model = torch.tensor([next_input]).permute(1,0,2).to(device)

            # prediction shape: (self.look_back, 1, self.num_features)
            prediction = model(next_input_model, device) 
            in_sample_predictions[i,:] = prediction[-1,0,:].detach().cpu().numpy()

        #in_sample_errors = self.scaler.inverse_transform(self.training_data[:,:self.num_features])-self.scaler.inverse_transform(in_sample_predictions)
        in_sample_errors = self.training_data[:,:self.num_features]-in_sample_predictions

        return in_sample_errors


    def inference(self, path_to_save_model: str, best_model: str, device: str = "cpu") -> np.ndarray:
        """
        Args:
           
        Returns: 
            prediction_features: array of shape (self.forecast_window, self.num_features)
        """

        device = torch.device(device)
        
        model = Transformer(feature_size=self.num_features, extra_feature_size=self.num_extra_features).double().to(device)
        model.load_state_dict(torch.load(path_to_save_model+best_model))
        criterion = torch.nn.MSELoss()

        with torch.no_grad():

            model.eval()

            # Calculate the errors 
            in_sample_predictions = np.zeros((self.num_data_points-self.forecast_window-self.look_back, self.num_features))

            for i in range(self.num_data_points-self.look_back-self.forecast_window):
                next_input = self.training_data[i:self.look_back+i,:]
                # next_input_model shape: (self.look_back, 1, self.num_features+4)
                next_input_model = torch.tensor([next_input]).permute(1,0,2).to(device)

                # prediction shape: (self.look_back, 1, self.num_features)
                prediction = model(next_input_model, device) 
                in_sample_predictions[i,:] = prediction[-1,0,:].detach().cpu().numpy()

            self.num_in_sample_predictions = self.num_data_points-self.forecast_window-self.look_back

            in_sample_errors = self.training_data[self.look_back:self.num_data_points-self.forecast_window,:self.num_features]-in_sample_predictions
            #in_sample_errors = self.scaler.inverse_transform(self.training_data[self.look_back:self.num_data_points-self.forecast_window,:self.num_features])-self.scaler.inverse_transform(in_sample_predictions)

            #print(in_sample_errors.shape)
            #src = _input.permute(1,0,2).double().to(device)[1:, :, :] # 47, 1, 7: t1 -- t47
            #target = target.permute(1,0,2).double().to(device) # t48 - t59

            idx1 = self.num_data_points-self.forecast_window-self.look_back
            idx2 = idx1 + self.look_back
            # next_input.shape: (self.look_back, self.num_features+4)
            next_input = self.training_data[idx1:idx2,:]

            all_predictions = np.zeros((self.forecast_window, self.num_features))

            for i in range(self.forecast_window):
                # desired input for model: [input_length, batch, feature]

                # next_input_model shape: (self.look_back, 1, self.num_features+4)
                next_input_model = torch.tensor([next_input]).permute(1,0,2).to(device)
                
                # prediction shape: (self.look_back, 1, self.num_features)
                prediction = model(next_input_model, device) 

                # the last value of prediction is the new predicted value
                #print(prediction.detach().cpu().numpy().shape)
                #rand_int = np.random.randint(low=0, high=self.num_in_sample_predictions, size=1)
        
                all_predictions[i,:] = prediction[-1,0,:].detach().cpu().numpy()

                #all_predictions[i,:] = prediction[-1,0,:].detach().cpu().numpy()+in_sample_errors[rand_int,:]

                next_input = self.training_data[idx1+i:idx2+i,:]
                if i < self.look_back:
                    #print(all_predictions[:i+1,:].shape, all_predictions.shape)
                    #print(next_input[self.look_back-i-1:,:self.num_features].shape, next_input.shape)
                    next_input[self.look_back-i-1:,:self.num_features] = all_predictions[:i+1,:]
                else:
                    next_input[:,:self.num_features] = all_predictions[i-self.look_back:i,:]

                #pos_encoding_old_vals = src[i+1:, :, 1:] # 46, 1, 6, pop positional encoding first value: t2 -- t47
                #pos_encoding_new_val = target[i + 1, :, 1:].unsqueeze(1) # 1, 1, 6, append positional encoding of last predicted value: t48
                #pos_encodings = torch.cat((pos_encoding_old_vals, pos_encoding_new_val)) # 47, 1, 6 positional encodings matched with prediction: t2 -- t48
                
                #next_input_model = torch.cat((src[i+1:, :, 0].unsqueeze(-1), prediction[-1,:,:].unsqueeze(0))) #t2 -- t47, t48'
                #next_input_model = torch.cat((next_input_model, pos_encodings), dim = 2) # 47, 1, 7 input for next round

            #true = torch.cat((src[1:,:,0],target[:-1,:,0]))
            #loss = criterion(true, all_predictions[:,:,0])
            
            #src_features = self.scaler.inverse_transform(src[:,:,0].cpu())
            #target_features = self.scaler.inverse_transform(target[:,:,0].cpu())


            # all_predictions.shape: (self.forecast_window, self.num_features)
            #print(all_predictions.shape)

            prediction_features = all_predictions
            #prediction_features = self.scaler.inverse_transform(all_predictions)

            #in_sample_errors = self.scaler.inverse_transform(in_sample_errors)

        return prediction_features, in_sample_errors


    def inference_with_new_data(self, data: np.array, path_to_save_model: str, best_model: str, device: str = "cpu") -> np.ndarray:
               
        device = torch.device(device)
        
        model = Transformer(feature_size=self.num_features, extra_feature_size=self.num_extra_features).double().to(device)
        model.load_state_dict(torch.load(path_to_save_model+best_model))

        new_data_points = data.shape[0]

        # extra features for new data
        data = self.create_data_set_with_extra_features(data, is_train=False)

        with torch.no_grad():

            model.eval()

            idx1 = new_data_points-self.forecast_window-self.look_back
            idx2 = idx1 + self.look_back
            # next_input.shape: (self.look_back, self.num_features+4)
            next_input = data[idx1:idx2,:]

            all_predictions = np.zeros((self.forecast_window, self.num_features))

            for i in range(self.forecast_window):
                # desired input for model: [input_length, batch, feature]

                # next_input_model shape: (self.look_back, 1, self.num_features+4)
                next_input_model = torch.tensor([next_input]).permute(1,0,2).to(device)
                
                # prediction shape: (self.look_back, 1, self.num_features)
                prediction = model(next_input_model, device) 

                all_predictions[i,:] = prediction[-1,0,:].detach().cpu().numpy()

                # create next_input
                next_input = data[idx1+i:idx2+i,:]

                if i < self.look_back:
                    next_input[self.look_back-i-1:,:self.num_features] = all_predictions[:i+1,:]
                else:
                    next_input[:,:self.num_features] = all_predictions[i+1-self.look_back:i+1,:]

            #prediction_features = self.scaler.inverse_transform(all_predictions)
            prediction_features = all_predictions


        return prediction_features


    def log_loss(self, loss_vals: list, path_to_save_loss: str, train: bool = True) -> None:
        if train:
            file_name = "train_loss.txt"
        else:
            file_name = "val_loss.txt"

        path_to_file = path_to_save_loss+file_name
        os.makedirs(os.path.dirname(path_to_file), exist_ok=True)
        with open(path_to_file, "a") as f:
            f.write(str(loss_vals[0])+","+str(loss_vals[1])+","+str(loss_vals[2])+"\n")
            f.close()

        return

"""
class IVDataset(Dataset):

    def __init__(self, csv_name, root_dir, training_length, forecast_window):

        # load raw data file
        csv_file = os.path.join(root_dir, csv_name)
        self.df = pd.read_csv(csv_file)
        self.root_dir = root_dir

        self.transform = MinMaxScaler()
        self.T = training_length
        self.S = forecast_window

    def __len__(self):
        # return number of sensors
        return len(self.df.groupby(by=["reindexed_id"]))

    # Will pull an index between 0 and __len__. 
    def __getitem__(self, idx):
        
        # Sensors are indexed from 1
        idx = idx+1

        # np.random.seed(0)

        start = np.random.randint(0, len(self.df[self.df["reindexed_id"]==idx]) - self.T - self.S) 
        sensor_number = str(self.df[self.df["reindexed_id"]==idx][["sensor_id"]][start:start+1].values.item())
        index_in = torch.tensor([i for i in range(start, start+self.T)])
        index_tar = torch.tensor([i for i in range(start + self.T, start + self.T + self.S)])
        _input = torch.tensor(self.df[self.df["reindexed_id"]==idx][["humidity", "sin_hour", "cos_hour", "sin_day", "cos_day", "sin_month", "cos_month"]][start : start + self.T].values)
        target = torch.tensor(self.df[self.df["reindexed_id"]==idx][["humidity", "sin_hour", "cos_hour", "sin_day", "cos_day", "sin_month", "cos_month"]][start + self.T : start + self.T + self.S].values)

        # scalar is fit only to the input, to avoid the scaled values "leaking" information about the target range.
        # scalar is fit only for humidity, as the timestamps are already scaled
        # scalar input/output of shape: [n_samples, n_features].
        scaler = self.transform

        scaler.fit(_input[:,0].unsqueeze(-1))
        _input[:,0] = torch.tensor(scaler.transform(_input[:,0].unsqueeze(-1)).squeeze(-1))
        target[:,0] = torch.tensor(scaler.transform(target[:,0].unsqueeze(-1)).squeeze(-1))

        # save the scalar to be used later when inverse translating the data for plotting.

        return index_in, index_tar, _input, target, sensor_number
"""


def black_scholes_call(S, K, tau, r, sigma):
    '''
    :param S: Asset price
    :param K: Strike price
    :param tau: Time to maturity
    :param r: risk-free rate 
    :param sigma: volatility

    :return: call price
    '''

    d1 = (torch.log(S / K) + (r + sigma ** 2 / 2) * tau) / (sigma * tau**0.5)
    d2 = d1 - sigma * tau**0.5

    N = torch.distributions.Normal(0, 1)

    call = S * N.cdf(d1) -  N.cdf(d2)* K * torch.exp(-r * tau)

    return call


def iv_grids_to_call_grids(iv_data, ttms, strikes):
    '''
    :param iv_data: iv data with shape (num_data_points, num_ttms, num_stirkes)

    :return: call price grids with shape (num_data_points, num_ttms, num_stirkes)
    '''
    call_data = torch.zeros(iv_data.size(), dtype=torch.float64)

    num_strikes = strikes.shape[0]
    num_ttms = ttms.shape[0]

    strikes = np.tile(np.array([strikes]), (num_ttms,1))
    ttms = np.tile(np.array([ttms]), (num_strikes,1)).T

    ttms = torch.from_numpy(ttms)
    strikes = torch.from_numpy(strikes)

    for i in range(iv_data.shape[0]):
        call_data[i,:,:] = black_scholes_call(S=torch.tensor([1.]), K=strikes, tau=ttms, r=torch.tensor([0.]), sigma=iv_data[i,:,:])
    return call_data